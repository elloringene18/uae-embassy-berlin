<?php

use Illuminate\Database\Seeder;

class UserUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = \App\User::find(1);

        $item->update(['password'=> \Illuminate\Support\Facades\Hash::make('Hatch1809@2020!!!')]);
    }
}
