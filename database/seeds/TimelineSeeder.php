<?php

use Illuminate\Database\Seeder;

class TimelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // //'date','content_en','content_ar','content','photo'

        $data = [
            [
                'date' => '1971',
                'content_en' => 'Establishment of
									the United Arab
									Emirates with
									Sheikh Zayed as
									President',
                'content_ar' => 'تأسيس دولة الإمارات العربية المتحدة برئاسة سمو الشيخ زايد بن سلطان آل نهيان',
                'content' => 'Gründung der Vereinigten Arabischen Emirate mit Scheikh Zayed als Präsident',
                'photo' => 'img/timeline/1971.jpg',
            ],
            [
                'date' => '1972',
                'content_en' => 'The UAE establishes
									diplomatic relations
									with the Federal
									Republic of Germany',
                'content_ar' => 'دولة الإمارات العربية المتحدة تنشئ علاقات دبلوماسية مع جمهورية المانيا الاتحادية ',
                'content' => 'Die VAE nehmen diplomatische Beziehungen mit der Bundesrepublik Deutschland auf',
                'photo' => 'img/timeline/1972.jpg',
            ],
            [
                'date' => '1976',
                'content_en' => 'First UAE Embassy
									founded in Bonn,
									West Germany',
                'content_ar' => 'تأسيس أول سفارة لدولة الإمارات العربية المتحدة في بون، المانيا الغربية',
                'content' => 'Mit der Botschaft in Bonn wird die erste diplomatische Vertretung der VAE in der BRD eingerichtet',
                'photo' => 'img/timeline/1976.jpg',
            ],
            [
                'date' => '2002',
                'content_en' => 'UAE mission moves to Berlin;
									inauguration of new
									Embassy building in 2004',
                'content_ar' => 'انتقال سفارة دولة الإمارات العربية المتحدة إلى برلين، وافتتاح مقر السفارة الجديد عام ٢٠٠٤',
                'content' => 'Umzug der VAE-Botschaft nach Berlin, Einweihung des neuen Botschaftsgebäudes 2004',
                'photo' => 'img/timeline/2002.jpg',
            ],
            [
                'date' => '2004',
                'content_en' => 'The UAE and Germany
									sign agreement upon
									strategic partnership',
                'content_ar' => 'توقيع اتفاقية شراكة استراتيجية بين دولة الإمارات العربية المتحدة وجمهورية المانيا الاتحادية',
                'content' => 'VAE und BRD unterzeichnen Vereinbarung über strategische Partnerschaft',
                'photo' => 'img/timeline/2004.jpg',
            ],
            [
                'date' => '2006',
                'content_en' => 'Goethe-Institut Gulf
									Region in Abu Dhabi
									2007
									Branch office in
									Dubai',
                'content_ar' => 'افتتاح معهد جوته لمنطقة الخليج في ابوظبي، وافتتاح فرع في دبي عام ٢٠٠٧',
                'content' => 'Eröffnung des Goethe-Institut Golf-Region in Abu Dhabi, 2007 Standort Dubai',
                'photo' => 'img/timeline/2006.jpg',
            ],
            [
                'date' => '2009',
                'content_en' => 'German Emirati
									Joint Council for
									Industry &
									Commerce founded',
                'content_ar' => 'تأسيس المجلس الألماني الإماراتي المشترك للصناعة والتجارة',
                'content' => 'Gründung der Deutsch-Emiratischen Industrie- und Handelskammer (AHK)',
                'photo' => 'img/timeline/2009.jpg',
            ],
            [
                'date' => '2011/2012',
                'content_en' => 'Pupils can earn
									the German
									International
									Abitur in Abu
									Dhabi & Dubai',
                'content_ar' => 'إمكانية حصول التلاميذ على شهادة أبيتور الألمانية الدولية في أبوظبي ودبي',
                'content' => 'Schüler können an den Deutschen Schulen Abu Dhabi und Dubai das Deutsche Abitur ablegen',
                'photo' => 'img/timeline/2010.jpg',
            ],
            [
                'date' => '2013',
                'content_en' => 'Start of the
									bilateral SAWA
									Museum
									Academy',
                'content_ar' => 'إنشاء أكاديمية علم المتاحف "سوا"',
                'content' => 'Start der bilateralen SAWA Museum Academy',
                'photo' => 'img/timeline/2013.jpg',
            ],
            [
                'date' => '2017',
                'content_en' => 'UAE and German
									governments
									agree to an
									energy
									partnership',
                'content_ar' => 'اتفاق حكومتا دولة الإمارات العربية المتحدة وجمهورية ألمانيا الاتحادية على شراكة في مجال الطاقة المتجددة',
                'content' => 'Regierung der VAE und Bundesregierung vereinbaren Energiepartnerschaft',
                'photo' => 'img/timeline/2017.jpg',
            ],
            [
                'date' => '2019',
                'content_en' => 'Both countries
									sign joint
									declaration of
									intent in Berlin',
                'content_ar' => 'توقيع "إعلان نوايا مشترك" بين دولة الإمارات العربية المتحدة وجمهورية المانيا الاتحادية في برلين',
                'content' => 'Beide Länder unterzeichnen gemeinsame Absichtserklärung in Berlin',
                'photo' => 'img/timeline/2019.jpg',
            ],
            [
                'date' => '2021',
                'content_en' => 'Expo 2020 is
									taking place in
									Dubai with
									strong German
									representation',
                'content_ar' => 'يقام معرض اكسبو ٢٠٢٠ في دولة الإمارات العربية المتحدة بتمثيل قوي من جمهورية المانيا الاتحادية',
                'content' => 'Die Expo 2020 Dubai findet mit starker deutscher Beteiligung statt',
                'photo' => 'img/timeline/2020.jpg',
            ],
        ];

        foreach($data as $item)
            \App\Models\Timeline::create($item);
    }
}
