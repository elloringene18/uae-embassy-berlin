<?php

use Illuminate\Database\Seeder;

class PageSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'Ambassador Bio',
                'title_ar' => '',
                'title' => '',
                'content_en' => '
										<h2>H.E. Hafsa Al Ulama</h2>
										<span>UAE Ambassador to Germany</span>
										<p>Welcome to the official website of the Embassy of the United Arab Emirates (UAE) in Berlin. We hope that you will be able to find helpful information and resources on our website.</p>',
                'content_ar' => '<h2>،سعادة حفصة العلماء</h2>
                                <span>سفيرة دولة الإمارات العربية المتحدة</span>
                                <p>يسرني أن أرحب بكم في الموقع الرسمي لسفارة دولة الإمارات العربية المتحدة لدى جمهورية ألمانيا الاتحادية، وآمل أن يقدم لكم الموقع معلومات مفيدة عن خدمات السفارة وأنشطتها الهادفة لدعم التعاون الوثيق وعلاقات الصداقة التي تجمع دولة الإمارات وألمانيا. كما يسعد السفارة تلقي استفساراتكم ومقترحاتكم عبر خدمة البريد الإلكتروني، ويمكنكم متابعتنا أيضاً على حساباتنا على وسائل التواصل الاجتماعي الخاصة بالسفارة عبر UAEinBerlin@.</p>
                                <p>حفصة العلماء</p><br/>
                                        ',
                'content' => '<h2>H.E. Hafsa Al Ulama</h2>
                                <span>Botschafterin der Vereinigten Arabischen Emirate</span>
                                <p>Willkommen auf der offiziellen Website der Botschaft der Vereinigten Arabischen Emirate (VAE) in Berlin. Wir hoffen, dass Sie auf unseren Seiten hilfreiche Informationen und anschauliche...</p>
                                        ',
                'page_id' => 1,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Ambassador Photo',
                'title_ar' => '',
                'title' => '',
                'content_en' => 'public/img/hafsa-g.jpg',
                'content_ar' => 'public/img/hafsa-g.jpg',
                'content' => 'public/img/hafsa-g.jpg',
                'page_id' => 1,
                'parent_section_id' => '',
                'type' => 'image'
            ],
            [
                'title_en' => 'Ambassador Bio Link',
                'title_ar' => '',
                'title' => '',
                'content_en' => '//uae-embassy.de/en/p/ambassador-bio',
                'content_ar' => '//uae-embassy.de/ar/p/ambassador-bio',
                'content' => '//uae-embassy.de/p/ambassador-bio',
                'page_id' => 1,
                'type' => 'text',
                'parent_section_id' => '',
            ],
        ];

        foreach($data as $item){
            $section['title_en'] = $item['title_en'];
            $section['title_ar'] = $item['title_ar'];
            $section['title'] = $item['title'];
            $section['content_en'] = $item['content_en'];
            $section['content_ar'] = $item['content_ar'];
            $section['content'] = $item['content'];
            $section['page_id'] = $item['page_id'];
            $section['type'] = isset($item['type']) ? $item['type'] : 'html';
            $section['parent_section_id'] = $item['parent_section_id'];
            $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
            $new = \App\Models\PageSection::create($section);
        }
    }
}
