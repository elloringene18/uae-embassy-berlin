<?php

use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Academic Facilities',
                'name_ar' => 'المرافق الأكاديمية',
                'slug' => 'academic-facilities',
                'content' => '<p><strong>Auditorium</strong><br>Our uniquely shaped and designed school auditorium has a centralised stage and can seat up to 400 people.</p>
                <p><strong>Science Labs</strong><br>Our school proudly offers eight fully-equipped science laboratories, two for each of the four sciences – chemistry, physics, biology, and general science.</p>
                <p><strong>Smaller Class Sizes</strong><br>At Dar Al Marefa, every class is limited to 24 students. This helps each child receive individual attention, increases their participation and creates a better flow of communication between the teacher, the student and their peers. </p>
                <p><strong>Library</strong><br>Our centralised library building is home to an extensive selection of resources, available across books
                    and digital formats. Here, we support the development of students’ literacy skills and encourage them to take up
                    reading as a hobby. The library subscribes to a range of online general and specialised resources and periodicals.
                    The library also features a designated study space with computers and internet access to facilitate research work.</p>
                <p><strong>Art &amp; Design Technology</strong><br>Our purpose-built art and design studios allow students to explore a wide variety of art and design techniques, including printmaking, painting, sculpture and drawing.</p>
                <p><strong>ICT</strong><br>Each of our computer labs uses the latest software as well as a range of 3D printers, giving students access to some of the newest industry technology and tools.</p>
                <p><strong>Music Studios</strong><br>Our custom-designed studio spaces support the teaching of music for the curriculum. A wide variety of additional musical extracurricular activities are offered through lunch time and after-school clubs.</p>
                ',
                'content_ar' => '
                <p><strong>قاعة المحاضرات
                    </strong><br>تتسع قاعة المحاضرات المدرسية ذات التصميم الفريد لحوالي 400 شخص، وهي تضم منصة مركزية.</p>
                <p><strong>مختبرات العلوم</strong><br>
                    توجد ثمانية مختبرات علوم مُجهَزة بالكامل، اثنان لكل فرع من فروع العلوم: الكيمياء والفيزياء والأحياء والعلوم العامة.
                </p>
                <p><strong>فصول أصغر حجمًا</strong><br>
                    .في مدرسة دار المعرفة، يضم كل فصل 24 طالبًا فقط؛ مما يساعد كل طفل في أن يتلقَّى اهتمامًا فرديًّا وزيادة مشاركتهم وإيجاد أسلوب تواصل أفضل بين المُعلم والطلاب وأقرانهم
                </p>
                <p><strong>المكتبة</strong><br>
                </p><p>
                    تضم المكتبة المركزية مجموعة كبيرة ومتنوعة من المراجع والوسائط التي تستهدف تطوير مهارات اكتساب المعلومات لدى الطلاب. وتشمل المكتبة مساحة للمذاكرة وأجهزة كمبيوتر مع إمكانية الدخول على الإنترنت، وهو الأمر الذي يسهّل العمل البحثي.
                    تشترك المكتبة في مجموعة من المراجع والدوريات، سواء العامة أو المتخصصة، عبر الإنترنت. ويتم العمل باستمرار على إضافة المزيد من الكتب الروائية والأدبية الجديدة للتشجيع على القراءة من أجل المتعة.
                </p>
                <p><strong>تقنيات الفنون والتصميم</strong><br>
                    تتيح الأستوديوهات المُصمَمة خصيصاً للفنون والتصميم فرصة رائعة للطلاب لاكتشاف مجموعة كبيرة ومتنوعة من تقنيات وأساليب الفن والتصميم، من بينها الطباعة والرسم والنحت والتلوين.
                </p>
                <p><strong>تقنية المعلومات والاتصالات</strong><br>
                    توجد مختبرات كمبيوتر متخصصة تستخدم أحدث البرمجيات المطابقة للمعايير، بالإضافة إلى مجموعة من الطابعات التي تستخدم تقنية 3D من أجل إتاحة الفرصة للطلاب للتعامل مع أحدث التقنيات.
                </p>
                <p><strong>أستوديوهات موسيقى</strong><br>
                    توجد مجموعة من الأستوديوهات المُصمَمة خصيصاً لتعزيز عملية تدريس الموسيقي في المناهج. كما يمكن استخدامها أيضاً لتقديم أنشطة إضافية مرتبطة بالمنهج خلال وقت الغداء ونوادي ما بعد المدرسة.
                </p>
                ',
            ],
            [
                'name' => 'Sports Facilities',
                'name_ar' => 'المرافق الرياضية',
                'slug' => 'sports-facilities',
                'content' => '
                <p>
                    As one of the top IB schools in Dubai, Dar Al Marefa gives students the opportunity to enjoy sports and exercise all-year through, even in the summer. Our indoor air-conditioned sports facilities, include a fully equipped gymnasium and a large double volume sports hall that can be used for a variety of activities, such as:
                </p>
                <ul>
                    <li>Netball</li>
                    <li>Basketball</li>
                    <li>Badminton</li>
                    <li>Volleyball</li>
                    <li>Wall climbing</li>
                    <li>Gymnastics</li>
                    <li>Table tennis</li>
                    <li>Tennis</li>
                    <li>Football</li>
                    <li>Swimming Pool</li>
                </ul>
                ',
                'content_ar' => '
                <p> نوفر في دار المعرفة للطلاب فرصة للاستمتاع بالرياضة وممارستها طوال العالم حتى في فصل الصيف. تضم المرافق الرياضية المغطاة والمكيفة الهواء صالة ألعاب رياضية مجهزة بالكامل، وقاعة ألعاب رياضية ذات مساحة كبيرة يمكن استخدامها لممارسة عدد كبير من الأنشطة المتنوعة، مثل:</p>
                <ul>
                    <li>الجمباز</li>
                    <li>تنس الطاولة</li>
                    <li>التنس</li>
                    <li>كرة القدم</li>
                    <li>المسبح</li>
                    <li>كرة الشبكة</li>
                    <li>كرة السلة</li>
                    <li>كرة الريشة</li>
                    <li>الكرة الطائرة</li>
                    <li>الكرة الطائرة</li>
                    <li>تسلق الجدران</li>
                </ul>
                ',
            ],
            [
                'name' => 'Cafeteria',
                'name_ar' => 'الكافتيريا',
                'slug' => 'cafeteria',
                'content' => '
                 <p>A healthy lifestyle starts with basics like making the right food choices. Dar Al Marefa cafeteria offers the students a healthy menu with local, sustainable ingredients and dishes that are tasty and affordable.</p>
                <p>The nutrition teachings students learn about in class are put into practice in our cafeteria, helping to positively influence their food choices at school, and home, and when out in the wider community. </p>
                <br>
                <ul>
                    <li>Provide an enjoyable, nutritious and attractively presented selection of food and drinks at reasonable prices</li>
                    <li>Help reduce health risk factors by encouraging the development of good eating habits</li>
                    <li>Provide students with practical experience of making healthy food choices that reinforce classroom lessons</li>
                    <li>Demonstrate high standards of hygiene in relation to the preparation, storage and serving of food</li>
                </ul>
                ',
                'content_ar' => '
                <p>
                    تقدم الكافتيريا للطلاب خيارات غذائية صحية من خلال قائمة طعام تعتمد على المكونات المحلية المُستدامة، وتضم أطباقاً شهية وبأسعار في متناول الجميع.
                </p>
                <p>
                    تساعد الكافتيريا الطلاب على الالتزام بالممارسات الغذائية السليمة التي يتعلمونها في قاعات الدرس. وهذا الأمر من شأنه إحداث تأثير إيجابي في اختياراتهم الغذائية في المدرسة وفي المنزل، وأيضاً عندما يخرجون إلى مجتمعاتهم خارج المنزل والمدرسة.
                </p>
                <p>
                    تهدف الكافتيريا إلى:
                </p>
                <br>
                <ul>
                    <li>تقديم مجموعة متنوعة من المأكولات والمشروبات الشهية والمغذية بأسلوب جذاب وبأسعار معقولة</li>
                    <li>تزويد الطلاب بخبرة عملية في اتخاذ خيارات غذائية صحية تعزّز التعلم داخل قاعات الدرس</li>
                    <li>المساعدة في الحد من المخاطر الصحية من خلال العمل على غرس عادات غذائية جيدة</li>
                    <li>تطبيق أعلى المعايير الصحية، وذلك فيما يتعلق بإعداد الطعام وتخزينه وتقديمه.</li>
                </ul>
                ',
            ],
            [
                'name' => 'School Bus Transport',
                'name_ar' => 'استخدام الحافلات المدرسية',
                'slug' => 'school-bus-transport',
                'content' => '
                    <p>Dar Al Marefa offers safe bus transport to and from school, for students who require it. Please speak to our RTA representative to arrange your child’s pick up and drop off.  </p>
                    <p><strong>Meeting the Bus</strong><br>When meeting the bus, students must:</p>
                    <ul>
                        <li>Be accompanied by a parent/guardian to and from designated waiting area</li>
                        <li>Arrive at least five minutes before the regular pick up time</li>
                        <li>Not engage in play or other activities that will endanger themselves or their companions while waiting for the bus</li>
                        <li>Board the bus in an orderly manner</li>
                    </ul>
                    <p><strong>Riding the Bus</strong><br>During the journey, students must:</p>
                    <ul>
                        <li>Follow the instructions of the bus driver and bus assistant</li>
                        <li>Sit in their assigned seat, remain seated and wear seatbelts (where fitted) throughout the journey</li>
                        <li>Keep the aisles and walkway clear at all times</li>
                        <li>Be part of and create a positive behaviour environment</li>
                    </ul>
                    <p><strong>Leaving the Bus</strong><br>When disembarking, students must:</p>
                    <ul>
                        <li>Remain seated until the bus comes to a full stop</li>
                        <li>Get off the bus in an orderly manner</li>
                        <li>Only cross the road at the front of the bus</li>
                    </ul>
                    <p>Please note that no change will be made to the bus route without the approval of the RTA and School management.</p>
                ',
                'content_ar' => '
                    <p>
                        توفر دار المعرفة حافلات آمنة لنقل الطلاب الراغبين في ذلك من المدرسة وإليها. يُرجى التحدث مع ممثل هيئة الطرق والمواصلات لدينا لتنسيق أماكن الركوب والنزول من الحافلة.
                    </p>
                    <p><strong>انتظار الحافلة</strong><br>عند انتظار الحافلة، يجب اتباع ما يلي:</p>
                    <ul>
                        <li>اصطحاب أحد الوالدين أو ولي الأمر الطفل من منطقة الانتظار المخصصة وإليها</li>
                        <li>الوصول قبل الموعد المعتاد لركوب الحافلة بخمس دقائق على الأقل</li>
                        <li>عدم الانشغال باللعب أو أي أنشطة أخرى قد تعرض الطفل أو زملائه للخطر أثناء انتظار الحافلة</li>
                        <li>الالتزام بالنظام عند صعود الحافلة</li>
                    </ul>
                    <p><strong>ركوب الحافلة</strong><br>يجب على الطلاب أثناء وجودهم في الحافلة:</p>
                    <ul>
                        <li>اتباع تعليمات سائق الحافلة ومساعده</li>
                        <li>الجلوس في المقاعد المخصصة لهم وعدم مغادرتها وارتداء أحزمة الأمان (في المقاعد التي تحتوي على أحزمة أمان) طوال الرحلة.</li>
                        <li>عدم وضع شيء في الممرات الموجودة بين المقاعد</li>
                        <li>المساعدة في توفير بيئة سلوكية آمنة</li>
                    </ul>
                    <p><strong>مغادرة الحافلة</strong><br>يجب على الطلاب عند النزول من الحافلة اتباع ما يلي:</p>
                    <ul>
                        <li>البقاء في مقاعدهم حتى تتوقف الحافلة تمامًا</li>
                        <li>الالتزام بالنظام عند النزول من الحافلة</li>
                        <li>عبور الطريق أمام الحافلة فقط</li>
                    </ul>
                    <p>يُرجى ملاحظة أنه لن يكون هناك تغيير في مسار الحافلة المدرسية دون الحصول على موافقة من هيئة الطرق والمواصلات وإدارة المدرسة.</p>
                ',
            ],
        ];


        $imgs = [
            [
                'academic-1.jpg',
                'academic-2.jpg',
                'academic-3.jpg',
                'academic-4.jpg',
                'academic-5.jpg',
                'academic-6.jpg',
                'academic-7.jpg',
                'academic-8.jpg',
                'academic-9.jpg',
                'academic-10.jpg',
            ],
            [
                'sports-1.jpg',
                'sports-2.jpg',
                'sports-3.jpg',
                'sports-4.jpg',
                'sports-5.jpg',
                'sports-6.jpg',
                'sports-7.jpg',
                'sports-8.jpg',
                'sports-9.jpg',
            ],
            [
                'cafeteria-1.jpg',
                'cafeteria-2.jpg',
            ],
            [
                'bus-1.jpg',
            ],
        ];

        foreach ($data as $index => $item){
            $facility = \App\Models\Facility::create($item);

            foreach ($imgs[$index] as $img){
                $upload['path'] = 'uploads/facilities';
                $upload['original_name'] = $img;
                $upload['file_name'] = $img;
                $upload['mime_type'] = 'image/jpeg';
                $upload['template'] = 'slide';

                $facility->uploads()->create($upload);
            }
        }
    }
}
