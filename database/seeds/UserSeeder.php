<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Admin', 'email' => 'gene@thisishatch.com', 'password' => \Illuminate\Support\Facades\Hash::make('secret') ],
            [ 'name' => 'Admin', 'email' => 'admin@uae-embassy.de', 'password' => \Illuminate\Support\Facades\Hash::make('UAE@Berlin#2020!') ],
        ];

        foreach ($data as $index => $item){
            \App\User::create($item);
        }
    }
}
