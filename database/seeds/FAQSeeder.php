<?php

use Illuminate\Database\Seeder;

class FAQSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'page_section_id' => 7,
                'title' => 'Visa FAQs',
                'data' => [
                    [
                        'question' => 'Missing passport for foreigner with residence permission in the UAEMissing passport for foreigner with residence permission in the UAE',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
                            <ul>
                            <li>A copy of the old passport and a copy of the certificate of residence</li>
                            <li>A copy of the new passport/ Ordinary passport</li>
                            <li>Minutes of the German police documenting theft of passport with a translation of the ratification of our consulate into Arabic</li>
                            <li>A letter from the employer</li>
                            <li>2 color photographs</li>
                            <li>Pay (2 X 150 AED) per person, according to the following steps</li>
                            <li>Please click on the link below:</li>
                            </ul>
                            <p>&nbsp;</p>
                            <p>The UAE Ministry of Foreign Affairs offers a range of services, view them <a href="http://mofa.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx&nbsp;" target="_blank">here</a>.</p>
                            <p><a href="https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/Services/Documents/Loss.pdf" target="_blank">Click here to download the form for a lost or missing passport</a>.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am a German citizen holding an ordinary passport and planning to travel to the UAE. Do I need a visa to enter the UAE?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>No, the visa will be obtained upon arrival.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'Is there any fee for a holder of a German ordinary passport?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>No</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'What is the duration for a visit visa to the UAE?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>Visit visa bearers may stay for a maximum period of 90 days in any 180 day period.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am a German Diplomatic /Service passport holder and planning to travel to the UAE. Do I require a visa to enter the UAE?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>German diplomats OR service passport holders: No visa required.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am travelling with an ordinary passport and am not exempted from the visa requirements, but am planning to visit the UAE as a tourist. How do I apply for a visa?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>You can arrange the tourist visa in the in the following ways:</p>
										<ol>
										<li>Hotels &amp; tourist companies can apply on your behalf</li>
										<li>UAE national carriers can arrange a visa on your behalf.</li>
										<li>Organisations based in the UAE can apply for visit visas and service visas.</li>
										</ol>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am a holding a Diplomatic / Service passport and planning to visit the UAE for tourism purposes. How do I apply for the visa?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>No visa required.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am planning to apply for a visa to visit the UAE. Can I submit the application to the UAE Embassy?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>Only a Diplomatic / Official / Service passport holder can apply through the Embassy for official visits. </p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am holder of an ordinary passport, not exempted from the visa requirement and planning to apply for a visit visa to the UAE. Can I submit the application to the UAE Embassy?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>No, you can apply for a visit visa in the following ways:</p>
										<ol>
										<li>Only organisations based in the UAE can apply for visit visas and service visas.</li>
										<li>Individuals (relatives or friends), already residing in the UAE, may apply for a visit visa on your behalf, which is of course subject to guidelines.</li>
										</ol>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'What are the visa requirements and visa types for visits to the UAE?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>Please visit the website of the UAE Ministry of Interior for more details.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'What are the regulations for the transit visa?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<ul>
										<li>Only airlines can arrange the transit visa prior to departure.</li>
										<li>Passengers should have a confirmed booking for the third destination.</li>
										<li>The passport must be valid for at least six (6) months.</li>
										</ul>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'How long does the visa process take, if I submit the application to the Embassy?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>(Diplomatic/ Official/ Service passports only):</p>
										<p>The visa issuance process usually takes 14 days. Therefore, we recommend to all applicants to submit their applications earlier to avoid any delays. The entry permits, whether for a single entry or multiple entries, depend on the decision of the authority</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'For how long should my passport at least be valid to be able to travel to the UAE (all passport types)?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>The passport should be valid for at least another six (6) months and signed by the bearer. </p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am planning to work or to reside in the UAE. Whom should I contact?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>Resident visas and work permits are issued by the relevant authorities. In the UAE, please contact your sponsor (employer) for further details.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'How long can I stay in the UAE after my first entry?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>This depends on the type of visa you have been granted. Please visit the website of the UAE Ministry of Interior for more details.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'How do I submit my visa application to the Embassy? (Diplomatic, Official and Service passport holders only)',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<ol>
										<li>Please download the application form:&nbsp;<a target="_blank" href="https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/Services/Documents/Visa%20Application.pdf">Visa Application.pdf</a>&nbsp;&nbsp;<a target="_blank" href="https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/Services/Documents/Additional%20appendix.docx">Additional appendix.docx</a></li>
										<li>Submit two passport photographs in color and with a white background (size 4 cm &ndash; 6 cm).</li>
										<li>A verbal note from your Embassy or organisation is necessary.</li>
										<li>Mention your sponsor (employer) in the application.</li>
										<li>Clarify the purpose of your visit.</li>
										<li>Tourist and transit visa can be obtained through travel agencies or any operating hotel. The Embassy only issues two types of visas (short-term visa, long-term visa).</li>
										<li>Scan your visa application, passport (cover and first page of your passport in color), verbal note and all other documents and send them by email to the UAE Embassy in Berlin for a first screening: <a target="_blank" href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a></li>
										<li>As soon as we receive the approval from the home government, we will notify you by email to the email address provided in your application form.</li>
										<li>After receiving the approval notification, please send us all required original documents during the admission hours of the consular section for submitting applications from 9:30 a.m. until 12:30 p.m. The collection of your passport will be possible during the period from 2:00 p.m. until 3:30 p.m.</li>
										<li>The Embassy issues only official visit visa.</li>
										</ol>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am travelling to the UAE and hold a travel document. Do the UAE Immigration Departments issue a visa on travel documents?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => 'No, the Embassy and the UAE Immigration Departments do not issue a visa on travel documents.',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'We are a film authority and planning to film during our visit to the UAE. What are the proper ways to acquire permission?',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>The UAE National Media Council (NMC) can guide you in this matter. Please visit their website to get detailed instructions for obtaining a proper permission to film.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
                    [
                        'question' => 'I am traveling / on business in the UAE and would like to know the customs regulations and legislation before entering.',
                        'question_ar' => '',
                        'question_de' => '',
                        'answer' => '
										<p>Please visit the official website of the UAE Federal Customs Authority to get a clear overview of the customs regulations and related issues.</p>',
                        'answer_ar' => '',
                        'answer_de' => '',
                    ],
//                    [
//                        'question' => 'I am planning to visit the UAE and have to take my personal medication with me, since I am receiving medical treatment. What am I supposed to do before my departure to the UAE?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//                            <p>Please do the following: </p>
//                            <ol>
//                            <li>Medication prescribed by your doctor: Provide a certified letter by your treating physician or hospital stating that you are under medication treatment course.</li>
//                            <li>Medication purchased directly from the pharmacy: If you are taking medicine without prescription with you, which you purchased from any pharmacy, please keep the medication package for reference in case the custom offices should ask questions about it.</li>
//                            </ol>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'Can I enter the UAE with my stepchildren without their biological parent?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//										<p>Yes, with written proof of parental responsibility.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'Can I enter the UAE with my grandchildren?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//										<p>Yes, with written approval from a parent or guardian.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'My children have a different surname to mine; do I need a proof of parental responsibility in order to enter the UAE?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//										<p>It would be advisable.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'What is the minimum validity period that I need to have in my passport?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//										<p>Six months, beyond the end of your intended stay. If you hold a residence permit, your passport must be valid for at least six months in order to travel in and out of the country.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
                ]
            ],
            [
                'page_section_id' => 12,
                'title' => 'Legalization FAQs',
                'data' => [
//                    [
//                        'question' => 'I graduated from an International School in Germany and would like to legalise my school certificate before departing to the UAE, as the Ministry of Foreign Affairs in Germany will not legalise it. What am I supposed to do?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//										<p>The Embassy cannot legalise any certificate of an international school, if the school is not accredited or officially registered with the Ministry of Education and Cultural Affairs in any of the German Federal States.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'What is mean by original signature on the certificate of origin and the commercial documents?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//											<p>Required is the original signature of the authorised person on the document. A rubber stamp will not suffice.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'I am planning to visit the UAE and would like to take my pet with me for the time of my holiday/ stay in the UAE. Which precautions do I have to take?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//											<p>Please visit the website of the <a target="_blank" href="http://www.moew.gov.ae/Portal/en/our-services/animal-wealth/permits/importing-permit-for-pet-animals.aspx">UAE Ministry of Environment and Water</a> for more details.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'I am a physician / nurse / member of the medical staff and about to assume my new job at one of the hospitals in the UAE. How do I certify my professional certificate before leaving for the UAE?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//                            <p>Physicians - Please legalise the following certificates before submitting them to the Embassy:</p>
//                            <ol>
//                            <li>Diploma legalised by the University and the Ministry of Education and Cultural Affairs</li>
//                            <li>Certificate of Medical Specialty legalised by the issuing authority</li>
//                            <li>Approbation issued by the issuing authority</li>
//                            </ol>
//                            <br/>
//                            <p>Nurses and medical staff - please legalise the following certificates before submitting them to the Embassy:</p>
//                            <p>Diploma legalised by issuing authority</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'I am an engineer / lawyer / or any other profession and about to assume my new job in the UAE. How do I certify my professional certificate before leaving for the UAE?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//                            <p>Please legalise the following documents before submitting them to the Embassy:</p>
//                            <ol>
//                            <li>Diploma, legalised by the issuing authority and the Ministry of Education and Cultural Affairs.</li>
//                            <li>Certificate of your professional experience from your company, legalised by the relevant association.</li>
//                            <li>Certificate of registry from your syndicate stating that you are registered and eligible to practice your profession.</li>
//                            </ol>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'I have a document in German language and would like to use it in the UAE. What am I supposed to do?',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//                            <p>Make sure that the original document is legalised according to the legalisation&nbsp; process mentioned above.</p>
//                            <ul>
//                            <li>The document must be translated by an officially authorised translator in Germany.</li>
//                            <li>The translation must be attached with the original document and legalised by the competent court before submitting it to the UAE Embassy in Berlin.</li>
//                            </ul>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
//                    [
//                        'question' => 'I would like to obtain more elaboration on the express procedure of the UAE Embassy for the legalisation of documents.',
//                        'question_ar' => '',
//                        'question_de' => '',
//                        'answer' => '
//                            <p>There are two ways of submitting your documents for legalisation:</p>
//                            <p>1.)&nbsp; In person at the consular section:</p>
//                            <ol>
//                            <li>Ordinary procedure: In case you pay the legalisation fees with your Credit card at the Embassy, you can collect the legalised document the day after, during the official working hours from 2:30 p.m. to 3:30 p.m.</li>
//                            </ol>
//                            <p>2.)&nbsp; By mail:</p>
//                            <ol>
//                            <li>Ordinary procedure: As soon as the Embassy receives your payment of the legalisation fees, the legalisation process will be started, and will then be finished the following day.&nbsp;</li>
//                            </ol>
//                            <p>&nbsp;</p>
//                            <p>Note: If you are to send the documents for legalising by mail, kindly include a return envelope so that the Embassy can return the documents after completing the legalisation process. The Embassy is not responsible for the returning fees or a loss of the document in the returning process.</p>',
//                        'answer_ar' => '',
//                        'answer_de' => '',
//                    ],
                ]
            ],
        ];

        foreach($data as $item){
            $d['title'] = $item['title'];
            $d['page_section_id'] = $item['page_section_id'];
            $faq = \App\Models\Faq::create($d);

            foreach ($item['data'] as $fq){
                $fq['faq_id'] = $faq->id;
                \App\Models\FaqItem::create($fq);
            }
        }


        $data = [
            [
                'page_section_id' => 7,
                'title' => 'Visa FAQs DE',
                'data' => [
                    [
                        'question' => 'Was tue ich bei Verlust eines Passes (für Residenten)?',
                        'answer' => '<p>Bitte reichen Sie folgenden Unterlagen ein:</p>
                        <ul>
                        <li>Polizeiliches Bericht enth&auml;lt die Nummer des Verlorenen Passes</li>
                        <li>&Uuml;bersetzung des Polizeiberichts ins Arabische durch einen vereidigten &Uuml;bersetzer</li>
                        <li>Schreiben des Sponsors in den VAE</li>
                        <li>Kopie des verlorenen Reiseasses</li>
                        <li>Kopie der Aufenthalt auf dem verlorenen Reisepass</li>
                        <li>Kopie des VAE-Personalausweises</li>
                        <li>Farbige Kopie des neuen Reisepasses</li>
                        <li>Bezahlen Sie (2 x 150 AED) beim Klicken auf den folgenden <a href="https://www.mofaic.gov.ae/" target="_blank">Link</a>.</li>
                        <li>Individual Services</li>
                        </ul>',
                    ],
                    [
                        'question' => 'Ich bin deutscher Staatsbürger, der einen normalen Reisepass besitzt und in die VAE reisen möchte. Benötige ich ein Visum für die Einreise in die VAE?',
                        'answer' => '
									<p>Nein, deutsche Staatsbürger sind auf Grundlage des Gegenseitigkeitsprinzips von den Einreisevisumspflichten der VAE befreit</p>',
                    ],
                    [
                        'question' => 'Müssen Inhaber eines normalen deutschen Passes eine Gebühr zahlen?',
                        'answer' => '
									<p>Nein.</p>',
                    ],
                    [
                        'question' => 'Welche Gültigkeitsdauer hat ein Visum für die VAE?',
                        'answer' => '
									<p>Sie dürfen sich höchstens 90 Tage in einem Zeitraum von 180 Tagen aufhalten. </p>',
                    ],
                    [
                        'question' => 'Ich bin deutscher Diplomaten-/ Dienstpassinhaber und beabsichtige, in die VAE zu reisen. Benötige ich ein Visum für die Einreise in die VAE?',
                        'answer' => '
									<p>Deutsche Diplomaten- oder Dienstpassinhaber: <br/>Kein Visum erforderlich</p>',
                    ],
                    [
                        'question' => 'Ich reise mit einem normalen Reisepass und bin nicht von der Visumpflicht befreit. Ich beabsichtige jedoch, die Vereinigten Arabischen Emirate als Tourist zu besuchen. Wie beantrage ich ein Visum?',
                        'answer' => '<p>Sie k&ouml;nnen das Touristenvisum auf folgende Weise beantragen::</p>
<ol>
<li>Hotels und touristische Unternehmen k&ouml;nnen in Ihrem Namen ein Visum beantragen.</li>
<li>VAE-Angeh&ouml;rige k&ouml;nnen f&uuml;r Sie ein Visum beantragen.</li>
<li>Eine in den Vereinigten Arabischen Emiraten ans&auml;ssige Organisation kann Visa und Dienstleistungsvisa f&uuml;r Sie beantragen.</li>
</ol>',
                    ],
                    [
                        'question' => 'Ich beabsichtige, ein Visum für die VAE zu beantragen. Kann ich den Antrag bei der VAE-Botschaft einreichen',
                        'answer' => '
									<p>Nein. Nur Diplomaten-/ und Dienstpassinhaber können über die Botschaft offizielle Besuche beantragen.</p>',
                    ],
                    [
                        'question' => 'Ich bin Inhaber eines normalen Reisepasses, der nicht von der Visumpflicht befreit ist und beabsichtige die Beantragung eines Besuchsvisums für die Vereinigten Arabischen Emirate. Kann ich den Antrag bei der VAE-Botschaft einreichen?',
                        'answer' => '<p>Nein, Sie k&ouml;nnen auf folgende Weise ein Visum beantragen:</p>
<ul>
<li>Nur in den Vereinigten Arabischen Emiraten ans&auml;ssige Organisationen k&ouml;nnen ein Visum beantragen.</li>
<li>Einzelpersonen (Verwandte oder Freunde), die bereits in den Vereinigten Arabischen Emiraten ans&auml;ssig sind, k&ouml;nnen in Ihrem Namen ein Visum beantragen, das selbstverst&auml;ndlich Richtlinien unterliegt.</li>
</ul>',
                    ],
                    [
                        'question' => 'Was sind die Visabestimmungen und Visatypen für einen Besuch in den Vereinigten Arabischen Emiraten?',
                        'answer' => '
									<p>Weitere Informationen finden Sie auf der Webseite der Allgemeinen Verwaltung für Aufenthalts- und Ausländerangelegenheiten des betreffenden Emirats.</p>',
                    ],
                    [
                        'question' => 'Wie lauten die Bestimmungen für das Transitvisum?',
                        'answer' => '<ul>
<li>Nur Fluggesellschaften k&ouml;nnen das Transitvisum vor dem Abflug arrangieren.</li>
<li>Passagiere sollten die Buchung f&uuml;r das dritte Reiseziel best&auml;tigt haben.</li>
<li>Der Reisepass muss mindestens sechs (6) Monate g&uuml;ltig sein.</li>
</ul>',
                    ],
                    [
                        'question' => 'Wie lange dauert das Visumausstellungsverfahren, wenn ich den Antrag bei der Botschaft einreiche (Nur diplomatische / amtliche / Dienstpässe)?',
                        'answer' => '
									<p>Das Visumausstellungsverfahren dauert in der Regel 14 Tage. Wir empfehle daher, Ihren Antrag frühzeitig einzureichen, um Verzögerungen zu vermeiden. Die Eintragung, ob für einen einzelnen Eintrag oder für mehrere Einträge, hängt von der Entscheidung der Behörde ab.</p>',
                    ],
                    [
                        'question' => 'Wie lange muss mein Pass mindestens gültig sein, um in die VAE reisen zu können (alle Passarten)?',
                        'answer' => '
									<p>Der Pass sollte mindestens sechs (6) Monate gültig sein und vom Inhaber unterzeichnet sein.</p>',
                    ],
                    [
                        'question' => 'Ich beabsichtige, in den Vereinigten Arabischen Emiraten zu arbeiten oder in die VAE auszuwandern. Wen sollte ich kontaktieren?',
                        'answer' => '
									<p>Arbeitsvisa und Arbeitsgenehmigungen werden von den zuständigen Behörden ausgestellt.<br/>
Wenden Sie sich an Ihren Sponsor in den Vereinigten Arabischen Emiraten, um weitere Informationen zu erhalten.Weitere Informationen finden Sie auf der Allgemeinen Verwaltung für Aufenthalts- und Ausländerangelegenheiten im betreffenden Emirats.
</p>',
                    ],
                    [
                        'question' => 'Wie lange kann ich nach meinem Ersteintrag in den Vereinigten Arabischen Emiraten bleiben?',
                        'answer' => '
									<p>Dies hängt von der Art des Visums ab, das Ihnen erteilt wurde. Weitere Informationen finden Sie auf der Webseite der Allgemeinen Verwaltung für Aufenthalts- und Ausländerangelegenheiten des betreffenden Emirats.</p>',
                    ],
                    [
                        'question' => 'Wie reiche ich meinen Visumantrag bei der Botschaft ein (Nur für Diplomaten-, Dienst- und Dienstpassinhaber)?',
                        'answer' => '<ul>
<li><em>Laden Sie das Formular herunter.</em></li>
<li><em>Reichen Sie 2 Passfotos in Farbe und mit wei&szlig;em Hintergrund (Gr&ouml;&szlig;e 4 cm - 6 cm) ein.</em></li>
<li><em>Es ist eine Verbalnote Ihrer Botschaft oder Organisation erforderlich.</em></li>
<li><em>Erw&auml;hnen Sie Ihren Sponsor in der Anwendung.</em></li>
<li><em>Kl&auml;ren Sie Ihren Besuchszweck.</em></li>
<li><em>Touristen- und Transitvisa k&ouml;nnen Sie &uuml;ber Reiseb&uuml;ros oder jedes andere Hotel erwerben. Die Botschaft stellt nur drei Arten von Visa aus (Kurzzeitvisum, Langzeitvisum und Mehrfachvisum).</em></li>
<li><em>Scannen Sie Ihren Visumantrag, Ihren Pass (Deckblatt und erste Seite Ihres Passes in Farbe), die Verbalnote und alle anderen Dokumente und senden Sie diese per E-Mail an die Botschaft der VAE in Berlin f&uuml;r eine erste Vorf&uuml;hrung: </em><a href="mailto:berlinEMB.CONS@mofaic.gov.ae">CONS@mofaic.gov.ae</a></li>
<li><em>Sobald wir eine Genehmigung erhalten haben, werden wir Sie per E-Mail an die in Ihrem Antragsformular genannte Email-Adresse informieren.</em></li>
<li><em>Schicken Sie uns bitte nach Erhalt der Zustimmungsbenachrichtigung alle Dokumente im Original zu oder reichen Sie sie w&auml;hrend der &Ouml;ffnungszeiten der konsularischen Abteilung f&uuml;r die Einreichung der Antr&auml;ge von 9:30 - 12:30 Uhr vor Ort ein. Die Abholung Ihres Passes erfolgt in der Zeit von 14:00 bis 15:30 Uhr.</em></li>
</ul>',
                    ],
                ]
            ],
            [
                'page_section_id' => 12,
                'title' => 'Legalization FAQs DE',
                'data' => [
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                    [
                        'question' => '',
                        'answer' => '',
                    ],
                ]
            ],
        ];

        foreach($data as $item){
            $d['title'] = $item['title'];
            $d['page_section_id'] = $item['page_section_id'];
            $faq = \App\Models\Faq::create($d);

            foreach ($item['data'] as $fq){
                $fq['faq_id'] = $faq->id;
                \App\Models\FaqItem::create($fq);
            }
        }

        $data = [
            [
                'page_section_id' => 7,
                'title' => 'Visa FAQs AR',
                'data' => [
                    [
                        'question' => 'أنا مواطن ألماني أحمل جوازاً عادياً وأريد السفر إلى دولة الإمارات. هل أحتاج تأشيرة لدخول الإمارات؟',
                        'answer' => '<p>لا، المواطنون الألمان معفون من تأشيرة الدخول إعمالاً لمبدأ المعاملة بالمثل. </p>',
                    ],
                    [
                        'question' => 'هل هناك رسوم يدفعها حامل جواز السفر الألماني؟',
                        'answer' => '<p>لا.</p>',
                    ],
                    [
                        'question' => 'ما هي فترة صلاحية تأشيرة دولة الإمارات العربية المتحدة؟',
                        'answer' => '</p>يحق لصاحب الفيزا الإقامة 90 يوماً بحد أقصى خلال فترة 180 يوماً. <p>',
                    ],
                    [
                        'question' => 'أنا أحمل جواز سفر ألماني دبلوماسي/رسمي، وأريد السفر إلى دولة الإمارات. هل أحتاج إلى تأشيرة دخول؟',
                        'answer' => '<p>حاملو جوازات السفر الدبلوماسية/الرسمية الألمان ليسوا بحاجة لتأشيرة دخول الإمارات العربية المتحدة.   </p>',
                    ],
                    [
                        'question' => 'أنا أسافر باستخدام جواز سفر عادي ولست معفى من تأشيرة الدخول. وأريد زيارة دولة الإمارات كسائح، كيف يمكنني طلب تأشيرة الدخول؟',
                        'answer' => '<p>يمكن طلب تأشيرة سياحية كالتالي:</p>
<ul>
<li>يمكن طلب التأشيرة عن طريق الفنادق والمؤسسات السياحية.</li>
<li>يمكن لمواطني دولة الإمارات طلب تأشيرة لك</li>
<li>يمكن للمؤسسات الموجودة في دولة الإمارات طلب تأشيرة عادية أو تأشيرة خدمات لك.</li>
</ul>',
                    ],
                    [
                        'question' => 'أحتاج تأشيرة لدخول دولة الإمارات. هل يمكنني تقديم طلب التأشيرة لدى سفارة دولة الإمارات؟',
                        'answer' => ' <p> لا. فقط حاملو جوازات السفر الدبلوماسية والرسمية يمكنهم طلب الزيارات الرسمية عن طريق السفارة.</p>',
                    ],
                    [
                        'question' => 'أحمل جواز سفر عادي غير معفي من الحصول على تأشيرة دخول دولة الإمارات، وأحتاج تأشيرة زيارة لدولة الإمارات العربية المتحدة. هل يمكني تقديم طلب الحصول على التأشيرة لدى السفارة؟',
                        'answer' => '<p>لا يمكن تقديم الطلب للسفارة. ويمكن طلب الفيزا بالوسائل التالية:</p>
<p>من خلال إحدى المؤسسات الموجودة بالإمارات العربية المتحدة</p>
<p>من خلال شخص (من الأقارب أو الأصدقاء) مقيم بدولة الإمارات ويمكنه تقديم الطلب نيابة عنك بما يراعي اللوائح والقوانين.</p>',
                    ],
                    [
                        'question' => 'ما هي أنواع تأشيرة زيارة دولة الإمارات وشروطها؟',
                        'answer' => '<p>يمكن التعرف على أنواع التأشيرات وشروط الحصول عليها من خلال موقع الإدارة العامة للإقامة وشؤون الأجانب بالإمارة المعنية. </p>',
                    ],
                    [
                        'question' => ' ما هي شروط الحصول على تأشيرة العبور (الترانزيت)؟',
                        'answer' => '<p>لا يمكن الحصول على تأشيرة العبور إلا عن طريق شركات الطيران</p>
<p>يجب أن يكون المسافر قد أكد تذكرة السفر للدولة الهدف</p>
<p>يجب أن يكون جواز السفر سارياً لمدة 6 أشهر على الأقل.</p>',
                    ],
                    [
                        'question' => 'ما هو الحد الأدنى لفترة سريان جواز السفر حتى يحق لي السفر إلى الإمارات العربية المتحدة (جميع الجوازات)؟',
                        'answer' => '<p>يجب أن يكون الجواز صالحاً لمدة ستة أشهر  على الأقل من تاريخ طلب التأشيرة، ويجب أن يكون موقّعاً من طرف مالكه. </p>',
                    ],
                    [
                        'question' => 'أريد العمل والإقامة في الإمارات العربية المتحدة. ما هي الجهات التي أتواصل معها؟',
                        'answer' => '<p>يتم إصدار تأشيرات وتصاريح العمل من قبل الإدارات الرسمية المختصة. لمزيد من المعلومات عليك الاتصال بالكفيل في الإمارات العربية المتحدة.</p>
<p>كما يرجى زيارة موقع الإدارة العامة للإقامة وشؤون الأجانب بالإمارة المعنية للحصول على مزيد من التفاصيل.</p>',
                    ],
                    [
                        'question' => 'كم يحق لي البقاء في دولة الإمارات العربية المتحدة بعد الدخول؟',
                        'answer' => '<p>تتوقف فترة الإقامة المسموح بها على نوع التأشيرة التي حصلت عليها. لمزيد من المعلومات يرجى زيارة موقع الإدارة العامة للإقامة وشؤون الأجانب بالإمارة المعنية </p>',
                    ],
                ]
            ],
        ];

        foreach($data as $item){
            $d['title'] = $item['title'];
            $d['page_section_id'] = $item['page_section_id'];
            $faq = \App\Models\Faq::create($d);

            foreach ($item['data'] as $fq){
                $fq['faq_id'] = $faq->id;
                \App\Models\FaqItem::create($fq);
            }
        }
    }
}
