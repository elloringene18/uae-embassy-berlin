<?php

use Illuminate\Database\Seeder;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Expo 2020 Updates',
                'title_en' => 'Expo 2020 Updates',
                'title_ar' => 'المستجدات الخاصة بمعرض
                                        EXPO 2020 ',
                'content' => 'Alle Informationen über die Weltausstellung Expo 2020 aus erster Hand',
                'content_en' => 'Be the first to get all information on the world’s greatest show, the Expo 2020.',
                'content_ar' => 'كن أول من يحصل EXPO 2020 على كافة المستجدات الخاصة بأكبر حدث في العالم،',
                'link' => 'https://www.expo2020dubai.com/de',
                'link_ar' => 'https://www.expo2020dubai.com/ar',
                'link_en' => 'https://www.expo2020dubai.com/en',
                'icon' => 'public/img/home-icon-expo.png',
            ],
            [
                'title' => 'AKTUELLE NACHRICHTEN AUS DEN VAE',
                'title_en' => 'The Latest UAE News',
                'title_ar' => 'آخر الأخبار الخاصة بدولة الإمارات العربية المتحدة',
                'content' => 'Informieren Sie sich über die aktuellsten Geschehnisse in den VAE über die offizielle Nachrichtenagentur WAM.',
                'content_en' => 'Stay up-to-date with what is happening in the UAE by visiting our official news and communications partner, WAM-The Emirates News Agency.',
                'content_ar' => 'ابق على اطلاع دائم بما يحدث في دولة الإمارات العربية المتحدة من خلال زيارة الصفحة الالكترونية الخاصة بشريكنا الإعلامي، وكالة أنباء الإمارات (وام)',
                'link' => 'https://www.wam.ae/de',
                'link_ar' => 'https://www.wam.ae/ar',
                'link_en' => 'https://www.wam.ae/en',
                'icon' => 'public/img/home-icon-news.png',
            ],
            [
                'title' => 'CORONAVIRUS (COVID-19) INFORMATIONEN & HILFE',
                'title_en' => 'Coronavirus (COVID-19) Guidance & Support',
                'title_ar' => 'الدعم الخاص بفيروس كورونا المستجد<br/>(COVID-19)',
                'content' => 'Erhalten Sie die aktuellsten Coronavirus- (COVID-19) Updates und informieren Sie sich über neueste Entwicklungen über unsere offiziellen Kanäle',
                'content_en' => 'Find out the latest Coronavirus (COVID-19) updates and developments by visiting our official sources below.',
                'content_ar' => 'أحدث إجراءات التعامل مع فيروس كورونا المستجد',
                'link' => 'https://www.mofaic.gov.ae/en/corona-virus-latest-updates',
                'link_ar' => 'https://www.mofaic.gov.ae/ar-ae/corona-virus-latest-updates',
                'link_en' => 'https://www.mofaic.gov.ae/en/corona-virus-latest-updates',
                'icon' => 'public/img/home-icon-alert.png',
            ],
            [
                'title' => 'Aktuelles zur Mars-Mission der Vereinigten Arabischen Emirate',
                'title_en' => 'Emirates Mars Mission updates',
                'title_ar' => 'تعرف على مشروع الإمارات لاستكشاف كوكب المريخ',
                'content' => '',
                'content_en' => '',
                'content_ar' => '',
                'link' => 'https://www.mbrsc.ae/emirates-mars-mission',
                'link_ar' => 'https://www.mbrsc.ae/ar/emirates-mars-mission',
                'link_en' => 'https://www.mbrsc.ae/emirates-mars-mission',
                'icon' => 'public/img/home-icon-alert.png',
            ],
        ];

        foreach($data as $item){
            \App\Models\Announcement::create($item);
        }
    }
}
