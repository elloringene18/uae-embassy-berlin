<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Veranstaltungen',
                'title_ar' => 'فعاليات',
                'title_en' => 'Events',
                'thumbnail' => 'img/gallery/events/4.jpg',
                'photos' => [
                    [ 'thumbnail' => 'img/gallery/events/1.jpg', 'photo' => 'img/gallery/events/1-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/2.jpg', 'photo' => 'img/gallery/events/2-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/3.jpg', 'photo' => 'img/gallery/events/3-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/4.jpg', 'photo' => 'img/gallery/events/4-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/5.jpg', 'photo' => 'img/gallery/events/5-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/6.jpg', 'photo' => 'img/gallery/events/6-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/events/7.jpg', 'photo' => 'img/gallery/events/7-full.jpg' ],
                ]
            ],
            [
                'title' => 'Offizielle Treffen',
                'title_ar' => 'اجتماعات رسمية',
                'title_en' => 'Official Meetings',
                'thumbnail' => 'img/gallery/meetings/1.jpg',
                'photos' => [
                    [ 'thumbnail' => 'img/gallery/meetings/1.jpg', 'photo' => 'img/gallery/meetings/1-full.jpg' ],
                ]
            ],
            [
                'title' => 'Besuche',
                'title_ar' => 'زيارات',
                'title_en' => 'Visits',
                'thumbnail' => 'img/gallery/visits/1.jpg',
                'photos' => [
                    [ 'thumbnail' => 'img/gallery/visits/1.jpg', 'photo' => 'img/gallery/visits/1-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/visits/2.jpg', 'photo' => 'img/gallery/visits/2-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/visits/3.jpg', 'photo' => 'img/gallery/visits/3-full.jpg' ],
                ]
            ],
            [
                'title' => 'Zum Herunterladen',
                'title_ar' => 'مواد للتنزيل',
                'title_en' => 'Media Download',
                'thumbnail' => 'img/gallery/media/2.jpg',
                'photos' => [
                    [ 'thumbnail' => 'img/gallery/media/1.jpg', 'photo' => 'img/gallery/media/1-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/media/2.jpg', 'photo' => 'img/gallery/media/2-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/media/3.jpg', 'photo' => 'img/gallery/media/3-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/media/4.jpg', 'photo' => 'img/gallery/media/4-full.jpg' ],
                    [ 'thumbnail' => 'img/gallery/media/5.jpg', 'photo' => 'img/gallery/media/5-full.jpg' ],
                ]
            ],
        ];

        foreach($data as $item){
            $gallery['title'] = $item['title'];
            $gallery['title_ar'] = $item['title_ar'];
            $gallery['title_en'] = $item['title_en'];
            $gallery['thumbnail'] = $item['thumbnail'];

            $newgallery = \App\Models\Gallery::create($gallery);

            foreach ($item['photos'] as $photo){
                $newgallery->photos()->create($photo);
            }
        }
    }
}
