<?php

use Illuminate\Database\Seeder;

class BulletinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //'title','title_ar','title_en','edition','edition_ar','edition_de','date','file'


        $data = [
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'title_ar' => 'UAE ECONOMIC BULLETIN',
                'title_en' => 'UAE ECONOMIC BULLETIN',
                'edition' => '1st Edition',
                'edition_ar' => '1st Edition',
                'edition_en' => '1st Edition',
                'date' => 'January 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/Open-Data/UAE_Newsletter_1_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'title_ar' => 'UAE ECONOMIC BULLETIN',
                'title_en' => 'UAE ECONOMIC BULLETIN',
                'edition' => '2nd Edition',
                'edition_ar' => '2nd Edition',
                'edition_en' => '2nd Edition',
                'date' => 'February 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/Open-Data/UAE_Newsletter_2_2018_s.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'title_ar' => 'UAE ECONOMIC BULLETIN',
                'title_en' => 'UAE ECONOMIC BULLETIN',
                'edition' => '3rd Edition',
                'edition_ar' => '3rd Edition',
                'edition_en' => '3rd Edition',
                'date' => 'March 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/Open-Data/ECONOMIC%20BULLETIN%20MARCH%202018_FINAL.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '4th Edition',
                'date' => 'April 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/Open-Data/UAE_Economic_Bulletin_4_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '5th Edition',
                'date' => 'May 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/Open-Data/UAE_Economic%20Bulletin_5_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '6th Edition',
                'date' => 'June 2018',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE-ECONOMIC-BULLETIN-6thEdition-June2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '7th Edition',
                'date' => 'July 2018',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Economic_Bulletin_7_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '8th Edition',
                'date' => 'August 2018',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Economic_Bulletin_August_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '9th Edition',
                'date' => 'September 2018',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Economic_Bulletin_9_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '10th Edition',
                'date' => 'October 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_Economic_Bulletin_10_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '11th Edition',
                'date' => 'November 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_Economic_Bulletin_11_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '12th Edition',
                'date' => 'December 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_EconomicBulletin_12_2018.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '13th Edition',
                'date' => 'January 2018',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_Newsletter_1_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '14th Edition',
                'date' => 'February 2019',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_EconomicBulletin_14_Feb19.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '15th Edition',
                'date' => 'March 2019',
                'file' => 'https://sp.mofaic.gov.ae/SiteCollectionDocuments/EconomicAffairs/UAE_EconomicBulletin_15_Mar19.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '16th Edition',
                'date' => 'April 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_EconomicBulletin_16_April19_FINAL.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '17th Edition',
                'date' => 'May 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_05_2019_draft.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '18th Edition',
                'date' => 'June 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_06_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '19th Edition',
                'date' => 'July 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_07_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '20th Edition',
                'date' => 'August 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_EB_08_2019_new.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '21th Edition',
                'date' => 'September 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_09_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '22nd Edition',
                'date' => 'October 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_10_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '23rd Edition',
                'date' => 'November 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_11_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '24th Edition',
                'date' => 'December 2019',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_12_2019.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '25th Edition',
                'date' => 'January 2020',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/Economic%20Bulletin%20January%202020.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '26th Edition',
                'date' => 'February 2020',
                'file' => 'https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/Media/Documents/UAE_Economic_Bulletin_26_2020%20copy.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '27th Edition',
                'date' => 'March 2020',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/2020.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '28th Edition',
                'date' => 'April 2020',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_economic%20Newsletter_04_2020.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '29th Edition',
                'date' => 'May 2020',
                'file' => 'https://sp.mofaic.gov.ae/OpenData/Documents/UAE_Newsletter_05_2020.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '30th Edition',
                'date' => 'June 2020',
                'file' => 'https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/Media/Documents/UAE_Newsletter_06_2020.pdf'
            ],
            [
                'title' => 'UAE ECONOMIC BULLETIN',
                'edition' => '31st Edition',
                'date' => 'July 2020',
                'file' => '//uae-embassy.de/public/pdfs/Economic-Bulletin-July-2020.pdf'
            ],
        ];

        foreach($data as $item){
            $item['title_en'] = $item['title'];
            $item['title_ar'] = $item['title'];
            $item['edition_ar'] = $item['edition'];
            $item['edition_en'] = $item['edition'];
            \App\Models\Bulletin::create($item);
        }
    }
}
