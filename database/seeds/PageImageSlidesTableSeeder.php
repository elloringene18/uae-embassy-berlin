<?php

use Illuminate\Database\Seeder;

class PageImageSlidesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('page_image_slides')->delete();

        $data = [
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/1.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/2.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/3.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/4-1.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/5.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/6.jpg' ],
            [ 'page_id' => 1, 'photo' => 'img/home-slider/final/7.jpg' ],
        ];

        foreach($data as $item)
            \App\Models\PageImageSlide::create($item);

//        <div><img src="img/home-slider/final/1.jpg"></div>
//					  <div><img src="img/home-slider/final/2.jpg"></div>
//					  <div><img src="img/home-slider/final/3.jpg"></div>
//					  <div><img src="img/home-slider/final/4-1.jpg"></div>
//					  <div><img src="img/home-slider/final/5.jpg"></div>
//					  <div><img src="img/home-slider/final/6.jpg"></div>
//					  <div><img src="img/home-slider/final/7.jpg"></div>
        
    }
}
