<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

class NewPageSeeder extends Seeder
{
    use \App\Services\CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','slug','content_en','content_ar','content','publish_date','thumbnail','featured_image'

        $data = [
            [
                'title_en' => 'Terms of Use',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Privacy Policy',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Disclaimer',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
        ];

        foreach($data as $item){
            $item['slug'] = $this->generateSlug($item['title_en']);
            \App\Models\Article::create($item);
        }

    }
}
