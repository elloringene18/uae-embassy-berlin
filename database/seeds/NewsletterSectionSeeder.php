<?php

use Illuminate\Database\Seeder;

class NewsletterSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'Newsletters',
                'title_ar' => 'Newsletters',
                'title' => 'Newsletters',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
        ];

        if(\App\Models\PageSection::where('title_en',$data[0]['title_en'])->count()==0){
            foreach($data as $item){
                $section['title_en'] = $item['title_en'];
                $section['title_ar'] = $item['title_ar'];
                $section['title'] = $item['title'];
                $section['content_en'] = $item['content_en'];
                $section['content_ar'] = $item['content_ar'];
                $section['content'] = $item['content'];
                $section['page_id'] = $item['page_id'];
                $section['parent_section_id'] = $item['parent_section_id'];
                $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
                $new = \App\Models\PageSection::create($section);

                if($new && isset($item['children'])){
                    foreach ($item['children'] as $child){
                        $section['title_en'] = $child['title_en'];
                        $section['title_ar'] = $child['title_ar'];
                        $section['title'] = $child['title'];
                        $section['content_en'] = $child['content_en'];
                        $section['content_ar'] = $child['content_ar'];
                        $section['content'] = $child['content'];
                        $section['page_id'] = $item['page_id'];
                        $section['slug'] = \Illuminate\Support\Str::slug($child['title_en']);
                        $section['parent_section_id'] = $new->id;

                        \App\Models\PageSection::create($section);
                    }
                }
            }
        }

    }
}
