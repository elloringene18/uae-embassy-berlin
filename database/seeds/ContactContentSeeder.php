<?php

use Illuminate\Database\Seeder;

class ContactContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'Embassy in Berlin',
                'title_ar' => 'السفارة في برلين',
                'title' => 'Botschaft in Berlin',
                'content_en' => '
							<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9713.63363775905!2d13.3577958!3d52.5079476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa52a6bde1e0bc32a!2sEmbassy%20of%20the%20United%20Arab%20Emirates!5e0!3m2!1sen!2sae!4v1592915479375!5m2!1sen!2sae" width="100%" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p>Address: Hiroshimastra&szlig;e 18, 10785 Berlin, Germany</p>
										<p>Hours: Opens Monday&ndash;Friday 9AM&ndash;4PM</p>
										<p>Phone: +49 (0) 30 516516</p>
										<p>Fax: +49 (0) 30 516 51-900</p>
										<p>E-mail: <a href="mailto:BerlinEMB@mofa.gov.ae">BerlinEMB@mofa.gov.ae</a></p>
										<p>&nbsp;</p>
										<p><strong>Consular Office</strong></p>
										<p>Telephone Information: 9:30AM - 12:30PM</p>
										<p>Document Submission: 9:30AM - 12:30PM</p>
										<p>Document Collection: 2:30PM - 3:30PM</p>
										<p>E-mail:&nbsp;<a href="mailto:BerlinEMB.CONS@mofaic.gov.ae" target="_blank">BerlinEMB.CONS@mofaic.gov.ae</a></p>
									</div>
								</div>
							</div>
                ',
                'content_ar' => '
							<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9713.63363775905!2d13.3577958!3d52.5079476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa52a6bde1e0bc32a!2sEmbassy%20of%20the%20United%20Arab%20Emirates!5e0!3m2!1sen!2sae!4v1592915479375!5m2!1sen!2sae" width="100%" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
                <p>العنوان: Hiroshimastrasse 18-20, 10785 Berlin, Germany</p>
<p>مواعيد العمل: الاثنين إلى الجمعة، من 9 صباحاً حتى 4 مساء</p>
<p>هاتف:<span style="direction: ltr">  516516 30 (0) 49+</span></p>
<p>فاكس: <span style="direction: ltr"> 900-51 516 30 (0) 49+</span></p>
<p>بريد إلكتروني: <a href="mailto:BerlinEMB@mofa.gov.ae">BerlinEMB@mofa.gov.ae</a></p>
<br/>
<p>المكتب القنصلي:</p>
<p>الاستفسار الهاتفي: من 9:30 صباحاً حتى 12:30 ظهراً</p>
<p>تسليم المستندات: من 9:30 صباحاً حتى 12:30 ظهراً</p>
<p>استلام المستندات:&nbsp; من 2:30 عصراً حتى 3:30 عصراً</p>
<p>بريد إلكتروني: <a href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a></p>
									</div>
								</div>
							</div>
',
                'content' => '<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9713.63363775905!2d13.3577958!3d52.5079476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa52a6bde1e0bc32a!2sEmbassy%20of%20the%20United%20Arab%20Emirates!5e0!3m2!1sen!2sae!4v1592915479375!5m2!1sen!2sae" width="100%" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p>Adresse: Hiroshimastraße 18-20, 10785 Berlin</p>
										<p>Öffnungszeiten: Montag – Freitag 09:00 – 16:00 Uhr</p>
										<p>Telefon: +49 (0) 30 516516</p>
										<p>Fax: +49 (0) 30 516 51-900</p>
										<p>E-mail: <a href="mailto:BerlinEMB@mofa.gov.ae">BerlinEMB@mofa.gov.ae</a></p>
										<p>&nbsp;</p>
										<p><strong>Konsularabteilung</strong></p>
										<p>Telefonische Auskunft: 9:30 - 12:30 Uhr</p>
										<p>Dokumentenabgabe: 9:30 - 12:30 Uhr</p>
										<p>Dokumentenabholung: 14:30 - 15:30 Uhr</p>
										<p>E-mail:&nbsp;<a href="mailto:BerlinEMB.CONS@mofaic.gov.ae" target="_blank">BerlinEMB.CONS@mofaic.gov.ae</a></p>
									</div>
								</div>
							</div>',
                'page_id' => 5,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Consulate General',
                'title_ar' => 'القنصليات',
                'title' => 'Generalkonsulate',
                'content_en' => '
							<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.339479488213!2d7.106470515717891!3d50.73219217951522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bee174a112db67%3A0xfd96356ea231fd55!2sUAE%20consulate!5e0!3m2!1sen!2sae!4v1592915543472!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>Consulate General in Bonn</strong></p>
										<p>Address: Erste F&auml;hrgasse 6, 53113 Bonn</p>
										<p>Work Hours: Monday – Friday  09:00 AM  - 04:00 PM</p>
										<p>Phone: +49 (0) 228 267070</p>
										<p>Email: <a href="mailto:Consular.bonn@mofaic.gov.ae">Consular.bonn@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2661.242882695083!2d11.622851315624928!3d48.16339997922551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e7504cbb89271%3A0x2a7b264fe5a529f7!2sConsulate%20General%20of%20the%20UAE!5e0!3m2!1sen!2sae!4v1592915587983!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>Consulate General in Munich</strong></p>
										<p>Address: Lohengrinstrasse 21, 81925 Munich, Germany</p>
										<p>Work Hours: Monday  &ndash; Friday 09:00 AM&nbsp; - 04:00 PM</p>
										<p>Phone: +49 (0) 89 4120010</p>
										<p>E-mail: <a href="mailto:munichcon@mofaic.gov.ae">munichcon@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>',
                'content_ar' => '
							<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.339479488213!2d7.106470515717891!3d50.73219217951522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bee174a112db67%3A0xfd96356ea231fd55!2sUAE%20consulate!5e0!3m2!1sen!2sae!4v1592915543472!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>القنصلية العامة في بون</strong></p>
<p>العنوان: Erste F&auml;hrgasse 6, 53113 Bonn, Germany</p>
<p>مواعيد العمل: الاثنين إلى الجمعة، من 9 صباحاً حتى 4 مساء</p>

<p>هاتف: <span style="direction: ltr">267070 228 (0) 49+</span></p>
<p>بريد إلكتروني: <a href="mailto:Consular.bonn@mofaic.gov.ae">Consular.bonn@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2661.242882695083!2d11.622851315624928!3d48.16339997922551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e7504cbb89271%3A0x2a7b264fe5a529f7!2sConsulate%20General%20of%20the%20UAE!5e0!3m2!1sen!2sae!4v1592915587983!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>القنصلية العامة في ميونيخ</strong></p>
<p>العنوان: Lohengrinstrasse 21, 81925 Munich, Germany</p>
<p>مواعيد العمل: الاثنين إلى الجمعة، من 9 صباحاً حتى 4 مساء</p>
<p>هاتف: <span style="direction: ltr"> 4120010 89 (0) 49+</span></p>
<p>بريد إلكتروني: <a href="mailto:munichcon@mofaic.gov.ae">munichcon@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>',
                'content' => '
							<img src="//uae-embassy.de/public/img/pages/contact.jpg" width="100%">
							<br/>
							<br/>
							<div class="map-list">
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.339479488213!2d7.106470515717891!3d50.73219217951522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bee174a112db67%3A0xfd96356ea231fd55!2sUAE%20consulate!5e0!3m2!1sen!2sae!4v1592915543472!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>Generalkonsulat in Bonn</strong></p>
										<p>Adresse: Erste Fährgasse 6, 53113 Bonn</p>
										<p>Öffnungszeiten: Montag – Freitag 09:00 – 16:00 Uhr</p>
										<p>Telefon: +49 (0) 228 267070</p>
                                        <p>E-Mail: <a href="mailto:Consular.bonn@mofaic.gov.ae">Consular.bonn@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2661.242882695083!2d11.622851315624928!3d48.16339997922551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e7504cbb89271%3A0x2a7b264fe5a529f7!2sConsulate%20General%20of%20the%20UAE!5e0!3m2!1sen!2sae!4v1592915587983!5m2!1sen!2sae" width="100%" height="160" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
									<div class="col-md-6">
										<p><strong>Generalkonsulat in München</strong></p>
										<p>Adresse: Lohengrinstrasse 21, 81925 Munich</p>
										<p>Öffnungszeiten: Montag – Freitag 09:00 – 16:00 Uhr</p>
										<p>Telefon: +49 (0) 89 4120010</p>
										<p>E-mail: <a href="mailto:munichcon@mofaic.gov.ae">munichcon@mofaic.gov.ae</a></p>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>',
                'page_id' => 5,
                'parent_section_id' => '',
            ],
        ];

        foreach($data as $item){
            $section['title_en'] = $item['title_en'];
            $section['title_ar'] = $item['title_ar'];
            $section['title'] = $item['title'];
            $section['content_en'] = $item['content_en'];
            $section['content_ar'] = $item['content_ar'];
            $section['content'] = $item['content'];
            $section['page_id'] = $item['page_id'];
            $section['parent_section_id'] = $item['parent_section_id'];
            $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
            $new = \App\Models\PageSection::create($section);

            if($new && isset($item['children'])){
                foreach ($item['children'] as $child){
                    $section['title_en'] = $child['title_en'];
                    $section['title_ar'] = $child['title_ar'];
                    $section['title'] = $child['title'];
                    $section['content_en'] = $child['content_en'];
                    $section['content_ar'] = $child['content_ar'];
                    $section['content'] = $child['content'];
                    $section['page_id'] = $item['page_id'];
                    $section['slug'] = \Illuminate\Support\Str::slug($child['title_en']);
                    $section['parent_section_id'] = $new->id;

                    \App\Models\PageSection::create($section);
                }
            }
        }
    }
}
