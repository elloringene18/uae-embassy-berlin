<?php

use Illuminate\Database\Seeder;

class ArticleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Press Release', 'slug' => 'press-release' ],
            [ 'name' => 'Page', 'slug' => 'page'],
        ];

        foreach($data as $item)
            \App\Models\ArticleType::create($item);
    }
}
