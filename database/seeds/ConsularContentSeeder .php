<?php

use Illuminate\Database\Seeder;

class ConsularContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'Visa',
                'title_ar' => 'التأشيرات',
                'title' => 'Visum',
                'content_en' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/>
                    <p>The Consular Department of the Embassy of the United Arab Emirates in Berlin provides legalization services for documents and certificates of various kinds, including diplomas, certificates, powers of attorney, etc. Legalization is the certification of the authenticity of a stamp and signature on a document.</p>
                    <br/>
                    <p>The <a target="_blank" href="https://www.ica.gov.ae/en/home.aspx">Federal Authority of Identity and Citizenship</a> in the United Arab Emirates has enacted numerous regulations for entry visas. The visa application procedure depends on the purpose of the trip and the type of visa applied for. Important information on how to apply for a visa can be found on our Embassy website and on the website of the <a target="_blank" href="https://www.moi.gov.ae/en/">Ministry of Interior of the UAE</a>.</p>
                ',
                'content_ar' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/><p>يقدم القسم القنصلي بسفارة دولة الإمارات العربية المتحدة في برلين لجميع المواطنين خدمة&nbsp;التصديق على الوثائق والمستندات بأنواعها ويعتبر التصديق اجراء يفيد بصحة الختم والتوقيع على الوثيقة أو المستند.</p>
<br/>
<p>وقامت الادارات العامة للاقامة وشؤون الأجانب بالإمارات العربية المتحدة بسن العديد من القواعد المتعلقة بتأشيرة دخول الدولة، وتتوقف إجراءات طلب التأشيرة على نوع التأشيرة المطلوب والغرض من السفر. ويمكن الاطلاع على هذه الإجراءات على صفحة سفارة الدولة وكذلك من صفحة&nbsp;<a target="_blank" href="https://www.moi.gov.ae/ar/default.aspx">وزارة الداخلية بدولة الإمارات العربية المتحدة</a>.&nbsp;</p>',
                'content' => '
							<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
                            <p>Die Konsularabteilung der Botschaft der Vereinigten Arabischen Emirate in Berlin bietet einen Legalisationsservice für Dokumente und Urkunden unterschiedlicher Art, darunter Zeugnisse, Zertifikate, Vollmachten, usw. Die Legalisation – oder Beglaubigung - ist die Bescheinigung der Echtheit von Stempel und Unterschrift auf einem Dokument. </p>
                            <br/>
                            <p>Die Migrationsbeh&ouml;rde in den Vereinigten Arabischen Emiraten (<a target="_blank" href="http://ica.gov.ae/">Federal Authority of Identity and Citizenship</a>) hat zahlreiche Bestimmungen f&uuml;r Einreisevisa in Kraft gesetzt. Das Prozedere des Visumantrags h&auml;ngt vom Zweck der Reise und der Art des beantragten Visums ab. Wichtige Informationen zur Antragstellung finden Sie auf unserer Webseite und auf der Seite des <a href="https://www.moi.gov.ae/en/"  target="_blank">Innenministeriums der VAE</a>.</p>
						',
                'page_id' => 4,
                'parent_section_id' => '',
                'children' => [
                    [
                        'title_en' => 'EU/EEA Citizens',
                        'title_ar' => 'مواطنو الاتحاد الأوروبي',
                        'title' => 'Bürger der EU und des EWR',
                        'content_en' => '
                        <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p>Citizens or holders of passports issued by EU states listed below (Diplomatic &ndash; Official &ndash;  Service &ndash; Ordinary) are exempted from UAE Entry Visa Requirements, according to reciprocity. They may stay for a maximum period of 90 days in any 180&ndash;day period.</p>
							<p>Maximum period of 90 days in any 180&ndash;day period.</p>
							<div class="row">
								<div class="col-md-6">
									<ul>
										<li>Germany</li>
										<li>Italia</li>
										<li>Netherlands</li>
										<li>France</li>
										<li>Sweden</li>
										<li>Luxembourg</li>
										<li>Switzerland</li>
										<li>Austria</li>
										<li>Belgium</li>
										<li>Norway</li>
										<li>Denmark</li>
										<li>Portugal</li>
										<li>Slovakia</li>
										<li>Greece</li>
										<li>Finland</li>
										<li>Spain</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>Latvia</li>
										<li>Liechtenstein</li>
										<li>Iceland</li>
										<li>Slovenia</li>
										<li>Croatia</li>
										<li>Czech Republic</li>
										<li>Poland</li>
										<li>Hungary</li>
										<li>Bulgaria</li>
										<li>Estonia</li>
										<li>Lithuania</li>
										<li>Cyprus</li>
										<li>Malta</li>
										<li>Romania</li>
									</ul>
								</div>
							</div>
							
<br/>
							<p>German citizens are exempted from UAE Entry Visa Requirements, according to reciprocity. They may stay for a maximum period of 90 days in any 180-day period.</p>
<br/>
<p>Only the German</p>
<p>Ordinary passport</p>
<p>Child passport (renewed child passports will no longer be accepted)</p>
<p>Diplomatic &ndash; Service and UN passport will be accepted</p>
<br/>
<p><strong>Passport Validity:</strong></p>
<p><strong>For entry and transit, the passport must be valid for at least 6 months from the date of entry.</strong></p>
<p><strong>Entry with provisional passports is no longer possible</strong>.</p>
<br/>
<p>Holder of a travel pass Travel document</p>
<p>Entry and transit with a &lsquo;<em>Travel Document</em>\' is not accepted.</p>
<p>A visa cannot be issued.</p>
<br/>
<p>Foreign spouses of German citizens are required to have a visa before departure and this depending on the nationality. Please check the table for Non-German citizens for more details.</p>
<p>The European Union and the Schengen region are exempted from the state\'s entry visa on the principle of reciprocity.</p>
                        ',
                        'content_ar' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/>
<p>يُعفى مواطني أو حاملي جوازات السفر مثل جواز السفر العادي أو جواز السفر الرسمي أو جواز السفر الدبلوماسي من الاتحاد الأوروبي ومنطقة شنغن من تأشيرة الدخول للبلدان التالية وفقًا لمبدأ المعاملة بالمثل. يمكنهم البقاء لمدة أقصاها 90 يومًا في فترة 180 يومًا.</p>
<br/>
<div class="row">
    <div class="col-md-6">
        <ul>
<li>ألمانيا</li>
<li>إيطاليا</li>
<li>هولندا</li>
<li>فرنسا</li>
<li>السويد</li>
<li>لكسمبورج</li>
<li>سويسرا</li>
<li>النمسا</li>
<li>بلجيكا</li>
<li>النرويج</li>
<li>١١- الدانمرك</li>
<li>البرتغال</li>
<li>سلوفاكيا</li>
<li>اليونان</li>
<li>فنلندا</li>
<li>إسبانيا</li>
<li>لاتفيا</li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
<li>ليختنشتاين</li>
<li>آيسلندا</li>
<li>سلوفينيا</li>
<li>كرواتيا</li>
<li>الجمهورية التشيكية</li>
<li>بولندا</li>
<li>هنغاريا</li>
<li>بلغاريا</li>
<li>إستونيا</li>
<li>ليتوانيا</li>
<li>قبرص</li>
<li>مالطة</li>
<li>رومانيا</li>
<li>الجبل الأسود</li>
        </ul>
    </div>
</div>
<br/>
<p>اتخذت سلطات الهجرة في الإمارات العربية المتحدة مؤخراً إجراءات رئيسية لإصدار تأشيرات السفر لتسهيل السفر إلى البلاد. في هذا السياق ، تم إصدار أنواع مختلفة من التأشيرات اعتمادًا على الغرض من الدخول. لمزيد من

المعلومات.</p>
<br/>
<p>الموقع الإلكتروني للإدارة العامة للإقامة وشؤون الأجانب في الإمارة المعنية</p>
<br/>
<p><strong>أولاً: لحاملي الجنسية الألمانية</strong></p>
<p>يعفي المواطنون الألمان من تأشيرات الدخول عند الوصول إلى الإمارات العربية المتحدة يختم جواز السفر الألماني العادي أو جواز السفر الرسمي أو جواز السفر الدبلوماسي بمجرد أن تمر عبر أحدى المطارات أو الحدود البرية أو البحرية في الإمارات العربية المتحدة. يمكنك البقاء في الدولة لمدة أقصاها 90 يومًا خلال فترة 180 يومًا.</p>
<br/>
<p>تقبل فقط جوازات السفر التالية:</p>
<p>جواز سفر عادي</p>
<p>جواز سفر للأطفال&nbsp;</p>
<p>جوازات السفر الدبلوماسية والرسمية &nbsp;</p>
<br/>
<p><strong>تعليمات هامة </strong></p>
<p><strong>صلاحية جواز السفر للدخول والعبور ، يجب أن يكون جواز السفر صالح لمدة 6 أشهر على الأقل من تاريخ الدخول.</strong></p>
<br/>
<p>حاملي وثائق السفر&nbsp;</p>
<p>&nbsp;لايسمح بدخول حاملي وثائق السفر</p>
<p>&nbsp;ولايصدر لهم تأشيرات دخول</p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
                            <p>B&uuml;rger oder Inhaber von Reisep&auml;ssen wie dem Normalen Reisepass, Dienstpass oder Diplomatenpass der Europ&auml;ischen Union und dem Schengen-Raum (siehe nachfolgende Tabelle) sind nach dem Grundsatz der Gegenseitigkeit vom Einreisevisum befreit.</p>
<p>Sie d&uuml;rfen sich maximal 90 Tage in einem Zeitraum von 180 Tagen in den VAE aufhalten.</p>
<p>&nbsp;</p>
<div class="row">
    <div class="col-md-6">
        <ul>
<li>Deutschland</li>
<li>Italien</li>
<li>Niederlande</li>
<li>Frankreich</li>
<li>Schweden</li>
<li>Luxemburg</li>
<li>Schweiz</li>
<li>&Ouml;sterreich</li>
<li>Belgien</li>
<li>Norwegen</li>
<li>D&auml;nemark</li>
<li>Portugal</li>
<li>Slowakei</li>
<li>Griechenland</li>
<li>Finnland</li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
<li>Spanien</li>
<li>Lettland</li>
<li>Liechtenstein</li>
<li>Island</li>
<li>Slovenien</li>
<li>Kroatien</li>
<li>Tschechische Republik</li>
<li>Polen</li>
<li>Ungarn</li>
<li>Bulgarien</li>
<li>Estland</li>
<li>Litauen</li>
<li>Zypern</li>
<li>Malta</li>
<li>Rum&auml;nien</li>
<li>Montenegro</li>
        </ul>
    </div>
</div>
                       
							<br/>
							<p>Deutsche Staatsb&uuml;rger sind auf der Grundlage des Gegenseitigkeitsprinzips von der Visumpflicht f&uuml;r die Einreise in die VAE befreit. Sie d&uuml;rfen sich maximal 90 Tage innerhalb eines 180-Tage-Zeitraums in den VAE aufhalten.</p>
<br/>
<p><strong>Es werden&nbsp; ausschlie&szlig;lich folgende Reisedokumente akzeptiert:</strong></p>
<p>Normaler Reisepass</p>
<p>Kinderreisepass (verl&auml;ngerter Kinderreisepass wird nicht akzeptiert)</p>
<p>Diplomaten &ndash; ,Dienst- und UN- Pass</p>
<br/>
<p><u>Wichtige&nbsp; Hinweise !</u></p>
<br/>
<p><strong>G&uuml;ltigkeit des Reisepasses :</strong></p>
<p><strong>F&uuml;r eine Einreise und im Transit muss der Reisepass ab dem Einreisedatum mindestens 6 Monate g&uuml;ltig sein.&nbsp;</strong></p>
<br/>
<p><strong>Die Einreise mit einem&nbsp; vorl&auml;ufigen Reisepass&nbsp; ist nicht mehr m&ouml;glich.</strong></p>
<br/>
<p>Inhaber eines Reiseausweises&nbsp;&nbsp;<em>Travel Document</em></p>
<p>Die Einreise und der Transit mit einem Reiseausweis&nbsp;<em>Travel-Dokument</em>&nbsp;werden nicht akzeptiert.</p>
<p>Ein Visum kann nicht ausgestellt werden.</p>
<br/>
<p>Ausl&auml;ndische Ehepartner deutscher Staatsb&uuml;rger ben&ouml;tigen eventuell ein Visum vor der Abreise. Bitte &uuml;berpr&uuml;fen Sie anhand der Tabelle f&uuml;r nicht-deutsche Staatsb&uuml;rger, ob ein Visum vorab beantragt werden muss.</p>
<p>Die Europ&auml;ische Union und der Schengen-Raum sind auf der Grundlage des Gegenseitigkeitsprinzips vom Einreisevisum des Staates befreit.</p>
						 ',
                        'page_id' => '',
                    ],
                    [
                        'title_en' => 'Non EU/EEA Citizens',
                        'title_ar' => '	الجنسيات الأخرى',
                        'title' => 'Nicht-EU/EWR Bürger',
                        'content_en' => '
                            <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p>1) Countries whose ordinary passport holders may enter the UAE without the need for a prior visa and stay for a period not exceeding 30 days in accordance with ministerial decisions:</p>
                            
<div class="row">
    <div class="col-md-6">
        <ul><li>Andorra</li>
                            <li>Argentina</li>
                            <li>Australia</li>
                            <li>Bahamas</li>
                            <li>Barbados</li>
                            <li>Brazil</li>
                            <li>Canada</li>
                            <li>Chile</li>
                            <li>China</li>
                            <li>Costa Rica</li>
                            <li>El Salvador</li>
                            <li>United Kingdom</li>
                            <li>Honduras</li>
                            <li>Hong Kong</li>
                            <li>Japan</li>
                            <li>Kazakhstan</li>
                            <li>Malaysia</li>
                            <li>Mauritius</li>
                            <li>Mexico</li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
                            <li>Monaco</li>
                            <li>Nauru</li>
                            <li>New Zealand</li>
                            <li>Russia</li>
                            <li>Saint Vincent and the Grenadines</li>
                            <li>San Marino</li>
                            <li>Seychelles</li>
                            <li>Singapore</li>
                            <li>Salomon Islands</li>
                            <li>Ukraine</li>
                            <li>Uruguay</li>
                            <li>United States of America</li>
                            <li>Vatican City</li>
        </ul>
    </div>
</div>

                            
                            <br/>
                            <p><strong>India:</strong> Visa required. (Exceptions: Holders of an American Green Card or a residency visa valid for the EU and the United Kingdom receive a 14-day visa upon arrival for a fee.</p>
                            <br/>
                            <p>2) Further Nationalities&nbsp;</p>
                            <p>For more information about UAE visas (types, fees, and requirements), please visit the&nbsp;website of the General Administration for Residency and Foreigners Affairs of the concerned emirate.</p>
                            <br/>
                            <p><u>Important information for travel document holders:</u></p>
                            <p>The Embassy of the UAE in Germany and the Immigration Office in the UAE cannot issue visa for travel document holders.</p>
                            <ul>
                            <li>Diplomatic &amp; Service passport holders please contact the Embassy via email for more details.</li>
                            <li>Ordinary passport holders are kindly requested to obtain a visa according to the immigration law in the United Arab Emirates prior to their departure to the UAE.</li>
                            </ul>
                            <br/>
                            <p><u>How to apply for a visa:</u></p>
                            <ol>
                            <li>Applying for a tourist visa: For those who are traveling for leisure and spending their holidays in the UAE, travel companies, hotels in the UAE, and UAE national carriers (Emirates Airline Visa Service or Etihad Airways Visa Service) can arrange a proper visa for this purpose.</li>
                            <li>The application for a transit visa can be arranged by UAE National Airlines or other airlines, depending on their regulations.</li>
                            </ol>
                            <br/>
                            <p>For information on other types of visa and the conditions, please&nbsp;visit the website of the General Administration of Residency and Foreigners Affairs of the concerned Emirate. </p>
                        ',
                        'content_ar' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/><p>بالنسبة للمواطنين الذين يحملون جواز سفر عاديًا من الدول التالية ، لا يلزم الحصول على تأشيرة مسبقة لدخول الإمارات العربية المتحدة يمكنهم البقاء في الإمارات لمدة 30 يومًا:</p>
                  <br/>       
<div class="row">
    <div class="col-md-6">
        <ul>
<li>أندورا</li>
<li>الأرجنتين</li>
<li>أستراليا</li>
<li>جزر البهاما</li>
<li>بربادوس</li>
<li>البرازيل</li>
<li>كندا</li>
<li>تشيلي</li>
<li>الصين</li>
<li>كوستا ريكا</li>
<li>السلفادور</li>
<li>بريطانيا العظمى</li>
<li>هندوراس</li>
<li>هونج كونج</li>
<li>اليابان</li>
<li>كازاخستان</li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
<li>ماليزيا</li>
<li>موريشيوس</li>
<li>المكسيك</li>
<li>موناكو</li>
<li>ناورو</li>
<li>نيوزيلندا</li>
<li>روسيا</li>
<li>سانت فنسنت وجزر غرينادين</li>
<li>سان مارينو</li>
<li>سيشيل</li>
<li>سنغافورة</li>
<li>جزر سليمان</li>
<li>أوكرانيا</li>
<li>أوروغواي</li>
<li>الولايات المتحدة الامريكية</li>
<li>مدينة الفاتيكان</li>
        </ul>
    </div>
</div>
<p>&nbsp;</p>
<p><strong>الهند: </strong>التأشيرة مطلوبة (الاستثناءات: سيحصل حاملو البطاقة الخضراء الأمريكية وتأشيرة الإقامة سارية في الاتحاد الأوروبي والمملكة المتحدة على تأشيرة لمدة 14 يومًا عند الوصول مقابل رسوم.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>الجنسيات الأخرى</strong><strong>: مزيد من المعلومات حول تأشيرات دولة الإمارات العربية المتحدة (يمكن الاطلاع على التأشيرات والرسوم ونماذج الطلبات على الصفحة التالية:</strong></p>
<p>الموقع الإلكتروني للإدارة العامة للإقامة وشؤون الأجانب في الإمارة المعنية</p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p>1) F&uuml;r Staatsangeh&ouml;rige folgender L&auml;nder im Besitz eines normalen Reisepasses ist zur Einreise in die Vereinigten Arabischen Emirate ein vorheriges Visum nicht erforderlich!</p>
                            <p>Sie d&uuml;rfen sich h&ouml;chstens 30 Tage in den VAE aufhalten.</p>
                            <br/>
                                             
<div class="row">
    <div class="col-md-6">
                            <li>Andorra</li>
                            <li>Argentinien</li>
                            <li>Australien</li>
                            <li>Bahamas</li>
                            <li>Barbados</li>
                            <li>Brasilien</li>
                            <li>Kanada</li>
                            <li>Chile</li>
                            <li>China</li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
                            <li>Costa Rica</li>
                            <li>El Salvador</li>
                            <li>Gro&szlig;britannien</li>
                            <li>Honduras</li>
                            <li>Hong Kong</li>
                            <li>Japan</li>
                            <li>Kasachstan</li>
                            <li>Malaysia</li>
        </ul>
    </div>
</div>
                            <br/>
                            <p>2) Weitere Nationlit&auml;ten&nbsp;</p>
                            <p>Weitere Informationen zu VAE-Visa (Arten, Geb&uuml;hren und Antragsformulare) finden Sie auf der Webseite der Allgemeinen Verwaltung f&uuml;r Aufenthalts- und Ausl&auml;nderangelegenheiten des betreffenden Emirats.</p>
                            <p>&nbsp;<u>Wichtige Informationen f&uuml;r Inhaber eines blauen Reiseausweises (<em>travel document)</em>:</u></p>
                            <br/>
                            <p>Die Botschaft der VAE in Deutschland sowie die Migrationsbeh&ouml;rde in den VAE k&ouml;nnen keine Visa f&uuml;r Inhaber von blauen Reiseausweisen (<em>travel document</em>) ausstellen.</p>
                            <ul>
                            <li>Inhaber von diplomatischen oder Dienstreisep&auml;ssen kontaktieren bitte die Botschaft per Email f&uuml;r weitere Informationen.</li>
                            <li>Inhaber eines normalen Reisepasses werden gebeten, ihr Visum vor ihrer Reise gem&auml;&szlig; den Einreisegesetzen der Vereinigten Arabischen Emirate zu beantragen.</li>
                            </ul>
                            <br/>
                            <p><u>Wie beantrage ich ein Visum?:</u></p>
                            <ol>
                            <li>Touristenvisum: Touristenreisende, die ihren Urlaub in den Vereinigten Arabischen Emiraten verbringen, k&ouml;nnen &uuml;ber einen Reiseveranstalter, ein Hotel in den Vereinigten Arabischen Emiraten oder &uuml;ber die nationalen Fluggesellschaften der VAE (Emirates Airline Visa Service oder Etihad Airways Visa Service) ein Visum beantragen.</li>
                            <li>Der Antrag auf ein Transitvisum abh&auml;ngig von ihren jeweiligen Bestimmungen von den nationalen Fluggesellschaften der VAE gestellt werden.</li>
                            <li>Informationen zu anderen Visumarten und Antragsbedingungen finden Sie auf der Webseite der Allgemeinen Verwaltung f&uuml;r Aufenthalts- und Ausl&auml;nderangelegenheiten des betreffenden Emirats (<em>General Administration of Residency and Foreigners Affairs</em>).</li>
                            </ol>',
                        'page_id' => '',
                    ],
                    [
                        'title_en' => 'Diplomatic & Official Pass Holders',
                        'title_ar' => '	حاملو الجوازات الدبلوماسية والرسمية',
                        'title' => 'Inhaber von diplomatischen & Dienstpässen',
                        'content_en' => '
                        <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                        <br/>
                        <br/>
                        <p><strong>Only for Diplomatic, Official, and UN Pass Holders</strong></p>
                        <p>Please follow the steps below in order to obtain the appropriate visa:</p>
                        <ol>
                        <li>Complete the visa application form electronically:&nbsp;(The application MUST be typed).</li>
                        <li>Original passport, valid for at least another six (6) months, signed by the bearer.&nbsp;&nbsp;</li>
                        <li>Scan your passport in color (cover and first page of the passport).</li>
                        <li>Send two original passport photos, in color and with a white background (4 cm &ndash; 6 cm)</li>
                        <li>Obtain an original verbal note from a government department, governmental agency, Embassy or any UN organization. The verbal note should include the following details: name, status, passport type &amp; number, purpose of the visit, date of the departure from Germany, date of arrival to the UAE and the duration of your stay in the UAE.</li>
                        <li>Send your application form and the necessary requirements mentioned above by email to the UAE Embassy in Berlin:&nbsp;<a href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a></li>
                        <li>As soon as we receive the approval from the home government, we will notify you by email to your email address provided in your application form.</li>
                        <li>The Embassy only issues two types of visas (Official visit visa):</li>
                        </ol>
                        <p>&nbsp;</p>
                        <table width="100%">
                        <tbody>
                        <tr>
                        <th width="340">Type of Visa
                        </th>
                        <th width="258">Validity Fee (in Euro)
                        </th>
                        </tr>
                        <tr>
                        <td width="340">
                        <p>Short-term Visa (single entry)<br /> Maximum stay is 30 days, non-renewable.</p>
                        </td>
                        <td width="258">
                        <p>Valid for two (2) months - Free</p>
                        </td>
                        </tr>
                        <tr>
                        <td width="340">
                        <p>Long-term Visa (single entry)<br /> Maximum stay is 90 days, non-renewable.</p>
                        </td>
                        <td width="258">
                        <p>Valid for two (2) months - Free</p>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        ',
                        'content_ar' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/><p><strong>الخطوات المتبعة في طلب الحصول على التأشيرة</strong> <strong>&nbsp;للجوازات الدبلوماسية &ndash; الخاصة &ndash; الأمم المتحدة </strong></p>
<ol>
<li>تحميل استمارة الطلب وتعبئتها، مع إضافة صورتين ملونتين بخلفية بيضاء مقاس 4x&nbsp;6 سم وذكر سبب الزيارة، ونوع التأشيرة المطلوبة.</li>
<li>بالنسبة لتأشيرات المتعددة الدخول يرجى ذكر بيانات عن سير الرحلة الرسمية وكتاب من الجهة الداعية &nbsp;في دولة الإمارات.</li>
<li>يمكن طلب تأشيرة السياحة عن طريق فندق أو أحد مكاتب السياحة أو شركات الطيران الوطنية.</li>
<li>يرجى عمل مسح ضوئي (سكان) من طلب التأشيرة وجواز السفر (الغلاف والصفحة الأولى بالألوان)، ومذكرة السفارة والمستندات الأخرى، وإرسالها على البريد أو تسليمها شخصيا للقسم القنصلي . &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
<li>سوف تتصل السفارة بمقدم الطلب فور حصولها على الرد من الدولة عن طريق البريد الألكتروني. <a href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a></li>
</ol>
<br/>

                        <table width="100%">
                        <tbody>
                        <tr>
                        <th width="340">نوع التأشيرة
                        </th>
                        <th width="258">رسوم الصلاحية (باليورو)
                        </th>
                        </tr>
                        <tr>
                        <td width="340">
                        <p>تأشيرة قصيرة المدى (دخول واحد)
<br /> الحد الأقصى للإقامة 30 يومًا غير قابلة للتجديد.</p>
                        </td>
                        <td width="258">
                        <p>صالحة لمدة شهرين (مجاني)</p>
                        </td>
                        </tr>
                        <tr>
                        <td width="340">
                        <p>تأشيرة طويلة الأجل (دخول واحد)
<br /> الحد الأقصى للإقامة 90 يومًا غير قابلة للتجديد.</p>
                        </td>
                        <td width="258">
                        <p>صالحة لمدة شهرين (مجاني)

</p>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        <br/>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p>Bitte befolgen Sie folgende Schritte zwecks Visumsvergabe</p>
                            <ol>
                            <li>F&uuml;llen Sie das Visumsantragsformular elektronisch aus&nbsp;(Der Antrage muss maschinengeschrieben sein).</li>
                            <li>Original pass, g&uuml;ltig f&uuml;r mindestens weitere sechs (6) Monate vom Inhaber unterschrieben.</li>
                            <li>Scan your passport in color (cover and first page of the passport).</li>
                            </ol>
                            <ol>
                            <li>F&uuml;gen Sie eine Farbkopie des Reisepasses bei.</li>
                            <li>Reichen Sie zwei originale Passfotos in Farbe und mit wei&szlig;em Hintergrund (4 cm &ndash; 6 cm) ein.</li>
                            <li>F&uuml;gen Sie eine Original-Verbalnote einer Regierungsabteilung, einer Regierungsbeh&ouml;rde, der Botschaft oder einer anderen UN-Organisation bei. Die Verbalnote sollte folgende Angaben enthalten: Name, Status, Pass Art und -nummer, Zweck des Besuchs, Datum der Abreise aus Deutschland, Datum der Ankunft in den Vereinigten Arabischen Emiraten und die Dauer&nbsp; des Aufenthalts in den Vereinigten Arabischen Emiraten.</li>
                            <li>Senden Sie Ihr Antragsformular und die oben genannten erforderlichen Unterlagen per E-Mail an die Botschaft der VAE in Berlin: <a href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a>.</li>
                            <li>Sobald wir die Genehmigung der Regierung erhalten haben, werden wir Sie per E-Mail an die in Ihrem Antrag angegebene E-Mail-Adresse informieren.</li>
                            <li>Die Botschaft erteilt nur zwei Arten von Visa (Offizielles Visum und Besuchervisum f&uuml;r Diplomaten- ,Dienst-und UN- Passinhaber):</li>
                            </ol>
                            <br/>
                            <table width="100%">
                            <tbody>
                            <tr>
                            <th width="363">
                            Art des Visums
                            </th>
                            <th width="235">
                            Geb&uuml;hr (in Euro)
                            </th>
                            </tr>
                            <tr>
                            <td width="363">
                            <p>Kurzfristiges Visum (Einmalige Einreise)<br /> Maximale Aufenthaltsdauer 30 Tage, nicht verl&auml;ngerbar.</p>
                            </td>
                            <td width="235">
                            <p>G&uuml;ltig f&uuml;r zwei (2) Monate &ndash; Gratis</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="363">
                            <p>Langzeitvisum (Einmalige Einreise)</p>
                            <p>Maximale Aufenthaltsdauer 30 Tage, nicht verl&auml;ngerbar.</p>
                            </td>
                            <td width="235">
                            <p>G&uuml;ltig f&uuml;r zwei (2) Monate - Gratis</p>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p>
						',
                        'page_id' => '',
                    ],
                    [
                        'title_en' => 'Other Visa Types ',
                        'title_ar' => '	تأشيرات أخرى',
                        'title' => 'Andere Arten von Visa',
                        'content_en' => '
                            <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>Student Visas</strong></p>
							<p>Student visas must be sponsored by one of the licensed Universities or educational institutions in the UAE, which are recognized by the Ministry of Higher Education.</p>
							<p><a target="_blank" href="https://www.moi.gov.ae/en/default.aspx">Click here for more information</a>.</p>
							<br/>
							<p><strong>Transit Visa</strong></p>
							<p>Any person transiting less than 12 hours can stop at the airport until they board their connection flight, provided they have a visa and confirmed ticket to their next / final destination.</p>
							<p>All passengers passing through UAE airports while flying to or from Europe, United States, Asia or Africa can obtain a special 96-hour transit visa, which is sponsored by any airline operating in the UAE. The traveler must have a valid ticket for an onward flight and the duration between the two trips must not be more than 8 hours.</p>
							<p><a target="_blank" href="https://www.moi.gov.ae/en/default.aspx">Click here for more information</a>.</p>
                        ',
                        'content_ar' => '
                    <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
                    <br/>
                    <br/><p><strong>تأشيرات</strong> <strong>الطلبة</strong></p>
<p>يجب أن تكفل الجامعات أو المؤسسات التعليمية المعتمدة من وزارة التعليم العالي بدولة الإمارات العربية المتحدة الحصول على تأشيرة الطلبة. </p>
<p><a href="https://www.moi.gov.ae/ar/default.aspx" target="_blank">لمزيد من المعلومات يرجى زيارة موقع الإدارة العامة للإقامة وشؤون الأجانب في الإمارة المعنية. </a></p>
<p><strong>&nbsp;</strong></p>
<p><strong>تأشيرة العبور (الترانزيت)</strong></p>
<p>يمكن للمسافرين الذين يقيمون أقل من 12 ساعة في الإمارات انتظار رحلتهم الجوية التالية في المطار، ويجب أن يكون لديهم تذكرة سفر مؤكدة وتأشيرة لدخول الدولة المراد السفر إليها.</p>
<p>يمكن لجميع المسافرين عبر الإمارات العربية المتحدة القادمين من أو المتجهين إلى أوروبا أو الولايات المتحدة الأمريكية أو أسيا أو أفريقيا الحصول على تأشيرة دخول عبور (ترانزيت) خاصة صالحة لفترة 96 ساعة برعاية شركة الطيران العاملة في الإمارات العربية المتحدة.</p>
<p><a href="https://www.moi.gov.ae/ar/default.aspx" target="_blank">لمزيد من المعلومات يرجى زيارة موقع الإدارة العامة للإقامة وشؤون الأجانب في الإمارة المعنية.</a></p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>Studentenvisum</strong></p>
                            <p>Studentenvisa m&uuml;ssen von einer der lizenzierten Universit&auml;ten oder Bildungseinrichtungen in den Vereinigten Arabischen Emiraten gesponsert werden, die vom Ministerium f&uuml;r Hochschulbildung anerkannt sind.</p>
                            <p><strong>F&uuml;r weitere Informationen besuchen Sie bitte die Webseite der Allgemeinen Verwaltung f&uuml;r Aufenthalts- und Ausl&auml;nderangelegenheiten (</strong><strong><em>General Administration of Residency and Foreigners Affairs</em></strong><strong>) des betreffenden Emirats. </strong></p>
                            <p><br /> </p>
                            <p><strong>Transitvisum</strong></p>
                            <p>Reisende, die sich auf der Durchreise f&uuml;r weniger als 12 Stunden in den VAE aufhalten, k&ouml;nnen die Wartezeit bis zum Antritt ihres Anschlussfluges am Flughafen verbringen. Sie m&uuml;ssen ein Visum und ein best&auml;tigtes Ticket f&uuml;r das Anschlussziel vorweisen k&ouml;nnen.</p>
                            <p>All passengers passing through UAE airports while flying to or from Europe, United States, Asia or Africa can obtain a special 96-hour transit visa, which is sponsored by any airline operating in the UAE. The traveler must have a valid ticket for an onward flight and the duration between the two trips must not be more than 8 hours.</p>
                            <p><strong>F&uuml;r weitere Informationen besuchen Sie bitte die Webseite der Allgemeinen Verwaltung f&uuml;r Aufenthalts- und Ausl&auml;nderangelegenheiten (</strong><strong><em>General Administration of Residency and Foreigners Affairs</em></strong><strong>) des betreffenden Emirats.</strong></p>
						',
                        'page_id' => '',
                    ],
                    [
                        'title_en' => 'FAQs',
                        'title_ar' => '	أسئلة متكررة',
                        'title' => 'FAQs',
                        'content_en' => '
                        <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>
                        ',
                        'content_ar' => '
                        <img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/visa.jpg" width="100%">
							<br/>
							<br/>',
                        'page_id' => '',
                    ],
                ]
            ],
            [
                'title_en' => 'Legalization',
                'title_ar' => '	التصديقات',
                'title' => 'Legalisation',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 4,
                'parent_section_id' => '',
                'children' => [
                    [
                        'title_en' => 'Certificates of Origin & Commercial Invoice',
                        'title_ar' => 'شهادات المنشأ',
                        'title' => 'Ursprungszeugnisse & Handelsrechnungen',
                        'content_en' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<p>Please make sure that the following items are mentioned in the commercial invoices and certificates of origins:</p>
                            <ul>
                            <li>The name of the buyer and/or consignee and the full address with P.O. Box no. and name of the city and country have to be mentioned in both documents</li>
                            <li>Port of Loading and Port of Embarkation should be clarified on both documents.</li>
                            <li>The name of the manufacturer and their full address should be clarified in both documents.</li>
                            </ul>
                            <br/>
                            <p><u>Notes:</u></p>
                            <ul>
                            <li>Certificates of Origin and Commercial Invoices will only be legalised together.</li>
                            <li>At the time of application, the Embassy requires one copy set of the commercial documents together with the original.</li>
                            <li>Certificates of Origin and Commercial Invoices should be first legalised by the German Chamber of Commerce and Industry (Deutscher Industrie- und Handelskammertag DIHKand then by the Arab-German Chamber of Commerce and Industry.</li>
                            <li>The original signature of both the specific company and the Chamber of Commerce and Industry in your country are required; a rubber stamp will not be accepted (electronic signatures and stamps are acceptable if they are registered in the Embassy system).</li>
                            <li>The legalisation fees for the original invoice are charged according to their total amount. The legalisation fee for a certificate of origin is 150 AED.</li>
                            <li>All applications can be submitted either by mail or in person.</li>
                            <li>If you are to send the document(s) for legalisation by mail, kindly include a return envelope so the Embassy can return the document(s) after completing the legalisation process. The Embassy is not responsible for returning the fees or for any loss of the document(s) in the returning process.</li>
                            <li>Kindly provide proof of payment in advance with your document(s) to prove that you have already transferred the fees to the MOFA account.</li>
                            <li>If the Certificate of Origin and the invoice are not issued in Germany, the Embassy will not legalise it (for Croatia, the final legalisation must be at MOFA in the respective country after the Chamber of Commerce).</li>
                            <li>All documents and commercial invoices must be legalised by the Embassy first, before submitting them to the UAE.</li>
                            <li>Payment can be made through the MOFA Website only, following the links below:</li>
                            
                            </ul>
                            <br/>
                            <p><a target="_blank" href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListBusiness.aspx">For invoices via: businesses</a></p>
                            <p><a target="_blank" href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx">For Certificate of Origin and other documents via: individuals</a></p>
                            <br/>
                            <p><strong>Contact information of Consular Department in Berlin:</strong></p>
                            <table width="100%">
                            <tbody>
                            <tr>
                            <td width="265">
                            <p>Embassy of the United Arab Emirates<br /> Consular Section / Legalisation<br /> Hiroshimastra&szlig;e 18-20<br /> D-10785 Berlin<br /> Germany</p>
                            </td>
                            <td width="329">
                            <p>Tel: + 49 (0) 30 516 51-6<br /> Fax:+ 49 (0) 30 516 51-900<br /> Email&nbsp;<a target="_blank" href="mailto:BerlinEMB.CONS@mofaic.gov.ae">BerlinEMB.CONS@mofaic.gov.ae</a></p>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            <br/>
                            <p><strong>Since 01 November 2019, German public documents in legalization process for the United Arab Emirates must be certified by the German Federal Administration Office (Bundesverwaltungsamt) in Cologne.</strong></p>
                            <br/>
                            <p><strong>Commercial invoices and certificates of origin will continue to be certified by the German Commercial and Industrial Chamber (Deutscher Industrie- und Handelskammertag DIHK)</strong></p>
                        ',
                        'content_ar' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/><p><strong><u>الفواتير:</u></strong></p>
<p>يتم دفع الرسوم عبر خدمة تصديقات الأعمال التجارية&nbsp; على الرابط :</p>
<p><a target="_blank" href="https://www.mofaic.gov.ae/">https://www.mofaic.gov.ae/</a></p>
<p><strong><u>يتم تحديد الرسوم بشكل تلقائي بعد إدخال القيمة الكلية ( بدون خصومات ) للفاتورة .</u></strong></p>
<p>&nbsp;</p>
<p>يرجى مراعاة الخطوات التالية في عملية دفع الفواتير:</p>
<ul>
<li>التأكد من اختيار مكان التصديق (برلين)</li>
<li>إتمام عملية الدفع وطباعة الإيصال لكل عملية</li>
<li>إرسال المستند وإيصال الدفع مع مظروف مزود بطابع بريدي على العنون التالي:</li>
</ul>
<br/>
<ul style="list-style: none;">
<li>
<p>Botschaft der&nbsp; Vereinigten Arabischen Emirate <br/>Konsularabteilung<br/>Hiroshimastr. 18-20 10785 Berlin</p></li></li>
</ul>
<p>&nbsp;</p>
<p><strong><u>شهادات المنشأ:</u></strong></p>

<p>قيمة رسوم تصديق شهادات المنشأ: 150 درهم إماراتي للمستند الواحد يتم دفع الرسوم عبر خدمة تصديقات الأفراد على الرابط :</p>
<p>يتم دفع الرسوم عبر خدمة تصديقات الأفراد &nbsp;&nbsp;على الرابط</p>
<p><a target="_blank" href="https://www.mofaic.gov.ae/">https://www.mofaic.gov.ae/</a></p>
<p>&nbsp;</p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<p>Bitte stellen Sie sicher, dass die folgenden Angaben in den Handelsrechnungen und Ursprungszeugnissen gemacht werden:</p>
<ul>
<li>Der Name des K&auml;ufers und/oder Empf&auml;ngers und die vollst&auml;ndige Adresse mit Postfachnummer sowie der Name der Stadt und des Landes m&uuml;ssen in beiden Dokumenten angegeben werden</li>
<li>Verladehafen und Einschiffungshafen sollten in beiden Dokumenten gekl&auml;rt werden.</li>
<li>Der Name des Herstellers und seine vollst&auml;ndige Adresse sollten in beiden Dokumenten dargelegt werden".</li>
</ul>
<br/>
<p><u>Anmerkungen:</u></p>
<ul>
<li>Ursprungszeugnis und Handelsrechnung werden ausschlie&szlig;lich zusammen beglaubigt.</li>
<li>Bei Antragstellung verlangt die Botschaft zus&auml;tzlich zum Original eine Kopie aller eingereichten Handelspapiere.</li>
<li>Ursprungszeugnisse und Handelsrechnungen m&uuml;ssen zun&auml;chst von dem Deutschen Industrie- und Handelskammertag (DIHK) und anschlie&szlig;end von der Ghorfa Arab-German Chamber of Commerce and Industry e. V. beglaubigt werden.</li>
<li>Die Originalunterschriften des jeweiligen Unternehmers und des Deutschen Industrie- und Handelskammertages sind erforderlich, ein Gummistempel wird nicht akzeptiert(elektronische Unterschriften und Stempel werden akzeptiert, wenn sie im System der Botschaft hinterlegt wurden).</li>
<li>Die Beglaubigungsgeb&uuml;hren f&uuml;r Originalrechnungen werden nach ihrem Gesamtbetrag berechnet.</li>
<li>Die Beglaubigungsgeb&uuml;hr f&uuml;r ein Ursprungszeugnis betr&auml;gt 150 AED</li>
<li>Alle Antr&auml;ge k&ouml;nnen entweder per Post oder pers&ouml;nlich eingereicht werden.</li>
<li>Sollten Sie das/die Dokument(e)) zur Legalisation per Post einschicken, bitten wir Sie, einen frankierten R&uuml;ckumschlag beizuf&uuml;gen, damit die Botschaft Ihnen das/die Dokument&euro; nach Abschluss der Legalisation zur&uuml;cksenden kann. Die Botschaft &uuml;bernimmt keine Versandkosten und &uuml;bernimmt auch keine Haftung f&uuml;r einen eventuellen Verlust bei der R&uuml;cksendung.</li>
<li>F&uuml;gen Sie Ihrem Antrag auch einen Beleg bei, aus dem hervorgeht, dass die Legalisationsgeb&uuml;hren bereits auf das MOFA Konto &uuml;berwiesen wurden.</li>
<li>Wurden Ursprungszeugnis und Rechnung nicht in Deutschland ausgestellt, werden sie von der Botschaft nicht beglaubigt (f&uuml;r Kroatien muss die Beglaubigung im Anschluss an die Handelskammer durch das Au&szlig;enministerium im jeweiligen Land erfolgen)</li>
<li>Alle Dokumente und Handelsrechnungen m&uuml;ssen zun&auml;chst von der Botschaft beglaubigt werden, bevor sie in den VAE eingereicht werden k&ouml;nnen.</li>
<li>Geb&uuml;hrenzahlungen k&ouml;nnen ausschlie&szlig;lich &uuml;ber die Webseite des MOFA, &uuml;ber die nachfolgenden Links erfolgen:</li>
</ul>
<p>F&uuml;r Rechnungen &uuml;ber: <a target="_blank" href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListBusiness.aspx">Businesses</a></p>
<p>F&uuml;r Ursprungszeugnisse und weitere Dokumente &uuml;ber: <a target="_blank" href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx">Individuals</a></p>
<br/>
<p><strong>Seit dem 01.11.2019 m&uuml;ssen deutsche &ouml;ffentliche Urkunden, die sich im Legalisierungsverfahren f&uuml;r die Vereinigten Arabischen Emirate befinden, vom Bundesverwaltungsamt (BVA) in K&ouml;ln beglaubigt werden.</strong></p>
<br/>
<p><strong>Handelsrechnungen und Ursprungszeugnisse werden weiterhin von dem Deutschen Industrie- und Handelskammertag beglaubigt.</strong></p>',
                        'page_id' => '',
                        'parent_section_id' => ''
                    ],
                    [
                        'title_en' => 'Further Types of Commercial Certificates',
                        'title_ar' => 'أنواع أخرى من الشهادات التجارية',
                        'title' => 'Weitere Arten von Handelsdokumenten',
                        'content_en' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<p>Procedures for legalizing the certificates before submitting them to the Embassy:</p>
                            <table width="100%">
                            <tbody>
                            <tr>
                            <th width="74%">Commercial Certificates
                            </th>
                            <th width="25%">Fees (in AED)
                            </th>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Commercial Power of Attorney</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Minutes of the Meeting of the Board of Directors</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Resolution of the Board of Directors</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Status</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Incorporation</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Deed of Amalgamation</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Incumbency</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Amendment</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Articles of Incorporation, Articles of Amalgamation,<br /> Articles of Amendment, etc.</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Agency Agreements (Distribution, Agricultural etc.)</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Authorisation of Agent</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Deed of assignment</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Financial statements</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Undertaking/Confirmation</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Memorandum of Association</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Compliance</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Trademarks</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="74%">
                            <p>Certificate of Registration&nbsp;</p>
                            </td>
                            <td width="25%">
                            <p>2000 AED</p>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p>
                            <p>Kindly follow the Instructions and pay 2000 AED by credit card for each of the above mentioned documents.</p>
                            <p><a target="_blank" href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx">Click here to register</a></p>
                            <ul>
                            <li>Choose attestation</li>
                            <li>Choose commercial agencies</li>
                            <li>Select quantity (number of documents you have)</li>
                            <li>Specify Berlin as location</li>
                            <li>Pay</li>
                            <li>Download proof of payment</li>
                            <li>Send the document with proof of payment and return envelope to the Embassy</li>
                            </ul>
                            <p>&nbsp;</p>
                            <p>Note:</p>
                            <ul>
                            <li>Any commercial document already legalised by any of the UAE Embassies worldwide or the UAE Foreign Ministry, is charged with a legalization fee of 150 AED for each document.</li>
                            <li>For any additional copy of the above mentioned commercial certificates, the same fee amount will be charged.</li>
                            <li>For Croatia the final legalisation of the above documents must be completed at the Ministry of Foreign Affairs (MOFA) of the respective country.</li>
                            </ul>
                            <p>&nbsp;</p>
                        ',
                        'content_ar' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
<p>رسوم تصديق المستندات طبقاً للائحة الرسوم بدولة الإمارات العربية المتحدة:</p>
<br/>
<table>
<tbody>
<tr>
<th width="74%">الشهادات التجاري
</th>
<th width="25%">الرسوم (بالدرهم)
</th>
</tr>
<tr>
<td width="480">
<p>المستندات التجارية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>شهادات الممارسات التصنيعية الجيدة ، شهادات الأدوية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>وكالة تصرف</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>محضر جلسة اللجنة الرقابية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>قرار اللجنة الرقابية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>شهادة براءة ذمة</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>شهادة تأسيس</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>إثبات وظيفة، منصب</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>وثيقة تغيير</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>عقد شركة، عقد اندماج، عقد تغيير</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>عقود وكالات (توزيع، زراعة، إلخ)</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>توكيل</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>وثيقة تنازل</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>تقارير مالية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>تعهد/ إقرار</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>وثيقة تأسيس (نظام) شركة رأس مال</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>علامات تجارية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>مستخرج من السجل التجاري مع عقد شريك تجاري في دولة الإمارات العربية المتحدة</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>مستخرج من السجل التجاري دون عقد</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>مجلس إدارة</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>المجلس التنفيذي</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>محضر اجتماع</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>شهادة عضوية</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
<tr>
<td width="480">
<p>شهادة حسن سير وسلوك</p>
</td>
<td width="172">
<p>2000&nbsp; درهم</p>
</td>
</tr>
</tbody>
</table>
<br/><p>يتم دفع الرسوم عبر خدمة تصديقات الأفراد على الرابط على الرابط :</p>
<p><a target="_blank" href="https://www.mofaic.gov.ae/">https://www.mofaic.gov.ae/</a></p>
<p>يجب تصديق جميع المستندات التجارية مسبقاً من قبل المكتب <strong>الاتحادي للإدارة</strong> (Bundesverwaltungsamt)</p>
                            ',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<p>Verfahren zur Beglaubigung der Dokumente vor ihrer Einreichung bei der Botschaft.</p>
<p>Gem&auml;&szlig; der Geb&uuml;hrenordnung der VAE betr&auml;gt die Geb&uuml;hr f&uuml;r folgende Dokumente:</p>
<table width="100%">
<tbody>
<tr>
<th width="74%">Handelsdokumentart
</th>
<th width="25%">Geb&uuml;hr (in AED)
</th>
</tr>
<tr>
<td width="74%">
<p>Handlungsvollmacht</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Sitzungsprotokoll des Aufsichtsgremiums</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Beschluss des Aufsichtsgremiums</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Certificate of Status</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Gr&uuml;ndungsurkunde</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Fusionsvertrag</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Best&auml;tigung der Amtsinhaberschaft</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>&Auml;nderungsurkunde</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Gesellschaftsvertrag, Fusionsvertrag, &Auml;nderungsvertrag</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Agenturvereinbarung (Vertrieb, Landwirtschaft, etc.)</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Authorisation of Agent</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Abtretungsurkunde</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Finanzbericht</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Verpflichtungserkl&auml;rung / Best&auml;tigung</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Gr&uuml;ndungsurkunde (Satzung) einer Kapitalgesellschaft</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Konformit&auml;tsbescheinigung / &Uuml;bereinstimmungsnachsweis</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Handelsmarken</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
<tr>
<td width="74%">
<p>Handelsregisterauszug&nbsp;</p>
</td>
<td width="25%">
<p>2000 AED</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Bitte befolgen Sie folgende Anweisungen und zahlen Sie per Kreditkarte 2000 AED f&uuml;r jedes der oben aufgef&uuml;hrten Dokumente.</p>
<p><a href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx" target="_blank">Registrieren Sie sich hier.</a></p>
<p>Registrieren Sie sich:</p>
<ul>
<li>W&auml;hlen Sie <em>attestation </em></li>
<li>W&auml;hlen Sie <em>commercial agencies</em></li>
<li>Geben Sie die Anzahl der eingereichten Dokumente an</li>
<li>Geben Sie bei der Zahlung &sbquo;Berlin&lsquo; als <em>location</em> an</li>
<li>Zahlung ausf&uuml;hren</li>
<li>Einzahlungsbeleg herunterladen / ausdrucken</li>
</ul>
<ul>
<li>Dokumente mit dem Einzahlungsbeleg und einem ausreichend frankierten, adressierten R&uuml;ckumschlag an folgende Adresse senden:</li>
</ul>
<p>Anmerkungen:</p>
<ul>
<li>Auf jedes bereits von einer der weltweiten Botschaften der VAE oder dem emiratischen Au&szlig;enministerium (MoFAIC) beglaubigte Handelsdokument wird eine Legalisationsbearbeitungsgeb&uuml;hr in H&ouml;he von 150 AED pro Dokument erhoben.</li>
<li>F&uuml;r jede weitere Kopie der o.g. Handelsdokumente wird eine Geb&uuml;hr in gleicher H&ouml;he erhoben.</li>
<li>F&uuml;r Kroatien muss die abschlie&szlig;ende Legalisation der o.g. Dokumente durch das Au&szlig;enministerium (MOFA) des jeweiligen Landes ausgef&uuml;hrt werden.</li>
</ul>
<p>&nbsp;</p>',
                        'page_id' => '',
                        'parent_section_id' => ''
                    ],
                    [
                        'title_en' => 'Other Types of Certificates',
                        'title_ar' => 'الوثائق، الشهادات والمستندات الأخرى',
                        'title' => 'Weitere Arten von Urkunden',
                        'content_en' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<table width="100%">
                            <tbody>
                            <tr>
                            <th width="37%">Certificate
                            </th>
                            <th width="41%">Procedures
                            </th>
                            <th width="20%">Fees (in AED)
                            </th>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Judicial Decrees</p>
                            <ol>
                            <li>Decrees issued by individuals (support, concession and gift)</li>
                            <li>Court decrees of all levels</li>
                            <li>Inheritance documents and contracts of selling and buying</li>
                            <li>Sponsorship (child adoption)</li>
                            </ol>
                            </td>
                            <td width="41%">
                            <p>Notary Public Office +&nbsp; Court</p>
                            <p>&nbsp;</p>
                            <p>Ministry of Foreign Affairs&nbsp; Germany&nbsp;</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <th width="37%">General Certificates
                            </th>
                            <th width="41%">Procedures
                            </th>
                            <th width="20%">Fees (in AED)
                            </th>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Academic Certificates</p>
                            <p>(School or university)</p>
                            </td>
                            <td width="41%">
                            <p>School or University&nbsp; + Higher Education (for German Documents)</p>
                            <p>&nbsp;</p>
                            <p>Ministry of Foreign Affairs for Croatian Documents</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Death Certificate</p>
                            </td>
                            <td width="41%">
                            <p>Death Certificate</p>
                            <p>Ministry of Foreign Affairs&nbsp; Germany&nbsp;&nbsp;</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Divorce Certificate</p>
                            </td>
                            <td width="41%">
                            <p>Court + Ministry of Foreign Affairs&nbsp; Germany&nbsp;&nbsp;</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Medical Certificate</p>
                            </td>
                            <td width="41%">
                            <p>Hospital + Ministry of Health + Ministry of Foreign Affairs Germany</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Birth Certificate</p>
                            </td>
                            <td width="41%">
                            <p>Department of Civil Status + Ministry of Foreign Affairs Germany</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Marriage Certificate</p>
                            </td>
                            <td width="41%">
                            <p>Department of Civil Status + Ministry of Foreign Affairs Germany</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>&ldquo;To whom it may concern&rdquo;</p>
                            </td>
                            <td width="41%">
                            <p>Notary Public Office + Court + BVA</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Employment Contract</p>
                            </td>
                            <td width="41%">
                            <p>Company + relevant chamber + BVA</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Passport Copy</p>
                            </td>
                            <td width="41%">
                            <p>Notary Public Office + Court + BVA</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="37%">
                            <p>Power of Attorney - personal</p>
                            </td>
                            <td width="41%">
                            <p>Notary Public Office + Court + BVA</p>
                            </td>
                            <td width="20%">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            <p>Kindly follow the Instructions and pay 150 AED by credit card for each of the above mentioned documents.</p>
                            <br/>
                            <p><a href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx" target="_blank">Click here to register</a>.</p>
                            
                            <ul>
                            <li>Choose attestation</li>
                            <li>Choose regular certificates</li>
                            <li>Select quantity (number of documents you have)</li>
                            <li>Specify Berlin as location</li>
                            <li>Pay</li>
                            <li>Download proof of payment</li>
                            <li>Send the document with proof of payment and return envelope to the Embassy</li>
                            </ul>
                            <p>&nbsp;</p>
                            <p>Notes:</p>
                            <ul>
                            <li>The certificates must be typed, as hand written documents - except for the signatures - are not accepted.</li>
                            <li>The Embassy cannot legalise any international school certificates, if the school is not accredited or officially registered with the Ministries of Education and Cultural Affairs in Germany.</li>
                            <li>For Croatia the final legalisation of the above documents must be completed at the Ministry of Foreign Affairs (MOFA) of the respective country.</li>
                            </ul>
                        ',
                        'content_ar' => '
                            <img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
<table>
<tbody>
<tr>
<th width="217">المستند
</th>
<th width="347">العملية
</th>
<th width="87">الرسوم
</th>
</tr>
<tr>
<td width="217">
<p>الأحكام القضائية</p>
<p>1. الأحكام المتعلقة بالأفراد (تأييد، امتياز، منح)</p>
<p>2. قرارات المحاكم بجميع مستوياتها</p>
<p>3. شهادات حصر الميراث</p>
<p>4. التبني</p>
</td>
<td width="347">
<p>توثيق كاتب العدل + محكمة الولاية +&nbsp; المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>الوثائق العامة، الشهادات، الشهادات الأكاديمية</p>
</td>
<td width="347">
<p>توثيق الجهة الخارجية المعنية (مثل المدرسة أو الجامعة أو المعهد) + اعتماد وزارة الثقافة أو ممثلية الولاية + تصديق المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>شهادات الوفاة</p>
</td>
<td width="347">
<p>مكتب الأحوال الشخصية + المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>شهادات الطلاق</p>
</td>
<td width="347">
<p>محكمة الولاية + المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>الشهادات الطبية</p>
</td>
<td width="347">
<p>المستشفى + تصديق نقابة الأطباء أو وزارة الصحة + المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>شهادات الميلاد، عقود الزواج</p>
</td>
<td width="347">
<p>مكتب الأحوال الشخصية + المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>شهادة&nbsp; "إلى من يهمه الأمر"</p>
</td>
<td width="347">
<p>توثيق كاتب العدل + محكمة الولاية +&nbsp; المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>صورة جواز سفر</p>
</td>
<td width="347">
<p>توثيق كاتب العدل + محكمة الولاية +&nbsp; المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>توكيل خاص</p>
</td>
<td width="347">
<p>توثيق كاتب العدل + محكمة الولاية +&nbsp; المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
<tr>
<td width="217">
<p>شهادة غرفة الصناعة والتجارة (IHK)</p>
</td>
<td width="347">
<p>غرفة الصناعة والتجارة + المكتب الاتحادي للإدارة</p>
</td>
<td width="87">
<p>150 درهم</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>يتم دفع الرسوم عبر خدمة تصديقات الأفراد &nbsp;&nbsp;على الرابط</p>
<p><a href="https://www.mofaic.gov.ae/"  target="_blank">https://www.mofaic.gov.ae/</a></p>
<p>&nbsp;</p>
<p>يرجى مراعاة الخطوات التالية في عملية دفع الفواتير:</p>
<ul>
<li>التأكد من أن يكون مكان (برلين)</li>
<li>إتمام عملية الدفع وطباعة الإيصال لكل عملية</li>
<li>إرسال المستند وإيصال الدفع مع مظروف مزود بطابع بريدي مناسب على العنون التالي:</li>
</ul>
<br/>
<ul style="list-style: none;">
<li>
<p>Botschaft der&nbsp; Vereinigten Arabischen Emirate <br/>Konsularabteilung<br/>Hiroshimastr. 18-20 10785 Berlin</p></li></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/legalization.jpg" width="100%">
							<br/>
							<br/>
							<p>&nbsp;</p>
                            <table width="652">
                            <tbody>
                            <tr>
                            <th width="217">
                            <p>Dokumentenart</p>
                            </th>
                            <th width="257">
                            <p>Verfahren</p>
                            </th>
                            <th width="178">
                            <p>Geb&uuml;hr</p>
                            </th>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Gerichtsbeschl&uuml;sse</p>
                            <p>1. Beschl&uuml;sse von Einzelpersonen (Unterst&uuml;tzung, Konzession und Geschenk)</p>
                            <p>&nbsp;2. Gerichtsbeschl&uuml;sse auf allen Ebenen.</p>
                            <p>3. Erbscheine</p>
                            <p>4. Patenschaften (Adoption eines Kindes)&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>Notarielle Beglaubigung+ Landgericht + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Allgemeine Urkunden, Zeugnisse oder</p>
                            <p>Akademische&nbsp; Zeugnisse</p>
                            <p>&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>Beglaubigung der Ausstellungsstelle z.B. Schule, Hochschule oder Universit&auml;t +Beglaubigung</p>
                            <p>des Kultusministeriums oder der Vertretung des jeweiligen Bundeslandes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Sterbeurkunde</p>
                            </td>
                            <td width="257">
                            <p>Standesamt + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Scheidungsurkunde&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>Landgericht + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>&Auml;rztliches Attest&nbsp;&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>Krankenhaus + &Auml;rztekammer oder Gesundheitsministerium + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Geburtsurkunde Heiratsurkunde</p>
                            </td>
                            <td width="257">
                            <p>Standesamt+ BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Bescheinigung &bdquo;An die zust&auml;ndige Abteilung&ldquo;</p>
                            </td>
                            <td width="257">
                            <p>Notarielle Beglaubigung + Landgericht + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Passkopie&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>Notarielle Beglaubigung + Landgericht + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>Vollmacht - pers&ouml;nlich</p>
                            </td>
                            <td width="257">
                            <p>Notarielle Beglaubigung + Landgericht + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            <tr>
                            <td width="217">
                            <p>IHK-Zeugnis&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            </td>
                            <td width="257">
                            <p>IHK + BVA</p>
                            </td>
                            <td width="178">
                            <p>150 AED</p>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p>
                            <p>Beachten Sie folgende Schritte zur Zahlung der Geb&uuml;hren&nbsp; und nutzen Sie folgenden<a target="_blank" href="https://www.mofaic.gov.ae/" target="_blank">Link</a>.</p>
                            
                            <ul>
                                <li><p>Bitte geben Sie bei der Zahlung Berlin als <u>Location </u>an</p></li>
                                <li><p>Zahlung ausf&uuml;hren und &nbsp;&nbsp;&nbsp;&nbsp; Einzahlungsbeleg ausdrucken</p></li>
                            </ul>
                            <p>&nbsp;</p>
                            <p>Dokumente mit dem Einzahlungsbeleg und einem ausreichend frankierten, adressierten R&uuml;ckumschlag an folgende Adresse senden:</p>
                            <p>Botschaft der&nbsp; Vereinigten Arabischen Emirate</p>
                            <p>Konsularabteilung</p>
                            <p>Hiroshimastr. 18-20</p>
                            <p>10785 Berlin</p>
                            <p>&nbsp;</p>
                            ',
                        'page_id' => '',
                        'parent_section_id' => ''
                    ],
                ],
            ],
            [
                'title_en' => 'Important Notes',
                'title_ar' => 'إرشادات هامة',
                'title' => 'Wichtige Hinweise',
                'content_en' => '
                        <img src="//uae-embassy.de/public/img/Medication.jpg" width="100%">
                        <br/>
                        <br/>
                        <p><strong><u>Medication</u></strong></p>
                        <p>Note on carrying medication for personal use</p>
                        <p>You need a medical certificate in English for prescription medication.</p>
                        <p>To take narcotics containing medication for personal use, you will need a prior approval from the UAE Ministry of Health.</p>
                        <br/>
                        <p>Please apply online <a href="http://www.mohap.gov.ae/en/services/Pages/361.aspx" target="_blank">here</a>.</p>
                        <br/>
                        <p>See below for more information:</p>
                        <p><em>Issuing permission to import medicines for personal use \'</em>.</p>
                        <p>Please use the&nbsp;e-service of the Ministry of Health of the UAE to submit your application.</p>
                        <br/>
                        <p><strong><u>Travelling with a pet</u></strong></p>
                        <p>For more information, please visit the <a  target="_blank" href="https://www.moccae.gov.ae/en/our-services/certificates/animal-health-certificate-for-export-re-export-of-live-animals.aspx.aspx">UAE Ministry of Environment and Water</a> website.</p>
                        
                        <p><a target="_blank" href="https://www.moccae.gov.ae/en/our-services/certificates/animal-health-certificate-for-export-re-export-of-live-animals.aspx.aspx" target="_blank">Issue a Veterinary Health Certificate for Exporting or Re-Exporting Live Animals</a></p>
                        <br/>
                        <p><strong><u>Police Clearance Certificate from the UAE</u></strong></p>
                        <p>Please note that obtaining a police permission for the Ministry of Interior in the United Arab Emirates can be requested online at the following <a href="https://www.moi.gov.ae/de/eservices/login.aspx?sType=27&amp;sCode=1289" target="_blank">website</a>.</p>
                        <br/>
                        <p>If you have any further questions, please contact the <a target="_blank" href="http://www.moi.gov.ae">hotline number</a> on the website.</p>
                        <br/>
                        <p>Please note that you can also receive a direct police authorization from the Dubai Police. You can apply online on the following websites:</p>
                        <p><a href="http://dubai.ae/de/lists/howtoguide/dispform.aspx?ID=3" target="_blank">http://dubai.ae/de</a></p>
                        <p><a href="https://www.dubaipolice.gov.ae/wps/portal/home" target="_blank">https://www.dubaipolice.gov.ae</a></p>
                        <br/>
                        <p><u><strong>Missing passport for foreigner with residence permission in the UAE</strong></u></p>
<ul>
<li>A copy of the old passport and a copy of the certificate of residence</li>
<li>A copy of the new passport/ Ordinary passport</li>
<li>Minutes of the German police documenting theft of passport with a translation of the ratification of our consulate into Arabic</li>
<li>A letter from the employer</li>
<li>2 color photographs</li>
<li>Pay (2 X 150 AED) per person, according to the following steps</li>
</ul>
<br/>

<p>Please click on the link below:</p>
<p><a href="https://sp.mofaic.gov.ae/EN/ConsularServices/Pages/ServiceListIndividual.aspx" target="_blank">The UAE Ministry of Foreign Affairs offers a range of services, view them here</a>.</p>
                        ',
                'content_ar' => '
                        <img src="//uae-embassy.de/public/img/Medication.jpg" width="100%">
                        <br/>
                        <br/><p>يستلزم حمل الأدوية الشخصية التي تحتوي على مواد ذات تأثير مخدر الحصول على تصريح مسبق من وزارة الصحة بدولة الإمارات العربية المتحدة:</p>
<p>لتقديم الطلب يرجى الدخول عبر الرابط التالي: <a target="_blank" href="http://www.mohap.gov.ae/en/services/Pages/361.aspx">http://www.mohap.gov.ae/en/services/Pages/361.aspx</a></p>
<p>يرجى استخدام الخدمة الإلكترونية (e-service) الخاصة بوزارة الصحة بدولة الإمارات العربية المتحدة</p>
<p>&nbsp;</p>
<p><strong>اصطحاب الحيوانات المنزلية في السفر</strong></p>
<p>لمزيد من المعلومات يرجى زيارة موقع وزارة التغير المناخي والبيئة :</p>
<p><a  target="_blank" href="http://www.moccae.gov.ae/en">www.moccae.gov.ae/en</a></p>
<p><strong><u>شهادة حسن السير والسلوك</u></strong><u>&nbsp; (</u>Clearance Certificate from the UAE<u>)</u></p>
<p>&nbsp;</p>
<p>يمكن طلب استخراج شهادة حسن السير والسلوك من خلال موقع <a target="_blank" href="https://www.moi.gov.ae/ar/default.aspx">وزارة الداخلية</a> بدولة الإمارات العربية المتحدة. لمزيد من المعلومات يرجى الاتصال بأرقام الخط الساخن المذكورة على صفحة وزارة الداخلية.</p>
<p>كما يرجى مراعاة إمكانية استخراج صحيفة الحالة الجنائية من شرطة دبي مباشرة من خلال الرابط التالي:</p>
<p><a href="http://dubai.ae/de/lists/howtoguide/dispform.aspx?ID=3" target="_blank">http://dubai.ae/de</a></p>
                        <p><a href="https://www.dubaipolice.gov.ae/wps/portal/home" target="_blank">https://www.dubaipolice.gov.ae</a></p>\'

<p><strong>فقدان جواز السفر (للمسافرين)</strong></p>
<p>يرجى تقديم المستندات التالية:</p>
<ol>
<li>تقرير من الشرطة يذكر فيه رقم الجواز المفقود</li>
<li>ترجمة لتقرير الشرطة إلى اللغة العربية من قبل مترجم معتمد</li>
<li>خطاب الكفيل في دولة الإمارات العربية المتحدة</li>
<li>صورة من جواز السفر المفقود</li>
<li>صورة من الإقامة الموجودة بجواز السفر المفقود</li>
<li>صورة من بطاقة الهوية الإماراتية</li>
<li>صورة ملونة من الجواز الجديد</li>
</ol>
<p>&nbsp;</p>
<p>يرجى دفع الرسوم (2x150  درهم إماراتي) من صفحة "خدمات الأفراد" على موقع <a target="_blank" href="https://www.mofaic.gov.ae/">وزارة الخارجية والتعاون الدولي</a>.&nbsp;</p>',
                'content' => '
                        <img src="//uae-embassy.de/public/img/Medication.jpg" width="100%">
                        <br/>
                        <br/><p><strong><u>Medikamente</u></strong></p>
<p>Zur Mitnahme von bet&auml;ubungsmittelhaltigen Medikamenten f&uuml;r den pers&ouml;nlichen Gebrauch ben&ouml;tigen Sie eine vorherige vom Gesundheitsministerium der VAE ausgestellte Genehmigung.</p>
<p>Online-Antrag unter&nbsp; <a target="_blank" href="http://www.mohap.gov.ae/en/services/Pages/361.aspx">http://www.mohap.gov.ae/en/services/Pages/361.aspx</a></p>
<ul>
<li><em>Issuing permission to import medicines for personal use`</em></li>
</ul>
<p>Nutzen Sie bitte den E-Service des Gesundheitsministeriums der VAE.</p>
<br/>
<p><strong><u>Reise mit einem Haustier </u></strong></p>
<p>F&uuml;r n&auml;here Informationen hierzu, besuchen Sie bitte die Webseite des <a target="_blank" href="http://www.moccae.gov.ae/en">Umwelt- und Wasserministeriums der VAE</a>.</p>
<p><a target="_blank" href="http://www.moccae.gov.ae/en">Klicken Sie bitte hier</a>.</p>
<br/>
<p><strong><u>Antrag auf ein polizeiliches F&uuml;hrungszeugnis (<em>Police Clearance certificate</em>) ausgestellt in der VAE</u></strong></p>
<p><em>Clearance Certificate from the UAE</em></p>
<br/>
<p>Bitte beachten Sie, dass ein polizeiliches F&uuml;hrungszeugnis auf der Webseite des <a target="_blank" href="https://www.moi.gov.ae/de/eservices/login.aspx?sType=27&amp;sCode=1289">Innenministeriums der Vereinigten Arabische Emirate</a> beantragt werden kann:</p>
<br/>
<p>Bei weiteren Fragen wenden Sie sich bitte an die <a target="_blank" href="http://www.moi.gov.ae">Hotline-Nummer</a> auf der Website.</p>
<br/>
<p>Bitte beachten Sie, dass Sie ein polizeiliches F&uuml;hrungszeugnis auch unmittelbar von der Dubai Police erhalten k&ouml;nnen. Sie k&ouml;nnen es online &uuml;ber die folgende Website beantragen:</p>
<p><a target="_blank" href="http://dubai.ae/de/lists/howtoguide/dispform.aspx?ID=3">http://dubai.ae/de/lists/howtoguide/dispform.aspx?ID=3</a></p>
<p><a target="_blank" href="https://www.dubaipolice.gov.ae/wps/portal/home">https://www.dubaipolice.gov.ae/wps/portal/home</a></p>
<br/>
<p><strong>Verlust eines Passes (f&uuml;r Residenten)</strong></p>
<p>Bitte reichen Sie folgenden Unterlagen ein:</p>
<ul>
<li>Polizeiliches Bericht enth&auml;lt die Nummer des Verlorenen Passes</li>
<li>&Uuml;bersetzung des Polizeiberichts ins Arabische durch einen vereidigten &Uuml;bersetzer</li>
<li>Schreiben des Sponsors in den VAE</li>
<li>Kopie des verlorenen Reiseasses</li>
<li>Kopie der Aufenthalt auf dem verlorenen Reisepass</li>
<li>Kopie des VAE-Personalausweises</li>
<li>Farbige Kopie des neuen Reisepasses</li>
<li>Bezahlen Sie (2 x 150 AED) beim Klicken auf den folgenden <a href="https://www.mofaic.gov.ae/" target="_blank">Link</a>.</li>
<li>Individual Services</li>
</ul>',
                'page_id' => 4,
                'parent_section_id' => ''
            ],
            [
                'title_en' => 'Citizen Affairs',
                'title_ar' => 'شؤون المواطنين',
                'title' => 'Citizen Affairs',
                'content_en' => '',
                'content_ar' => '
                        <img src="//uae-embassy.de/public/img/pages/citizen-affairs.jpg" width="100%">
                        <br/>
                        <br/><p><strong>شؤون المواطنين</strong></p>

<br/>
<p><strong>ترحب بكم سفارة الإمارات العربية المتحدة في برلين وتتمنى لكم الإقامة الطيبة في كل من جمهورية ألمانيا الاتحادية وجمهورية كرواتيا</strong><strong>. &nbsp;</strong></p>
<p>ونهيب بالأخوة المواطنين التسجيل في خدمة تواجدي على موقع وزارة الخارجية والتعاون الدولي حتى يتم التواصل من قبل البعثة مع المواطنين المتوجدين في جمهوريتي ألمانيا الاتحادية وكرواتيا في حالة الأزمات والطوارئ وتأمين عودتهم سالمين إلى الدولة، والتسجيل يتم من خلال الرابط التالي:</p>
<p><a target="_blank" href="https://sp.mofaic.gov.ae/Citizen/Pages/default.aspx">https://www.mofa.gov.ae/Citizen/Pages/default.aspx</a></p>

<br/>
<p><strong>معلومات عامة</strong><strong>:</strong></p>
<p><strong>شروط الدخول إلى ألمانيا ودول الشنغن</strong></p>
<p>يحصل المواطن على ختم في منافذ دول الشنغن يخوله البقاء لمدة لا تتجاوز 90 يوما متصلة أو منفصلة خلال 6 أشهر، وما زاد عن ذلك يعتبر مخالف لقانون الهجرة والإقامة، إلا في حالة تمديد خاص من قبل الجهات الرسمية الألمانية ، ولا يتم التمديد إلا وفق شروط خاصة.</p>

<br/>
<p><strong>الإقامة في ألمانيا</strong></p>
<p><strong>الهوية بطاقة التعريف</strong>: احمل معك دائماً بطاقة التعريف الشخصية أو صورة منها أو جواز سفرك وسجل اسمك وبيانات جواز سفرك في خدمة تواجدي على موقع وزارة الخارجية من باب الحيطة، خاصة إذا كانت إقامتك طويلة الأمد. وإذا أردت قيادة سيارة فلا تنسى رخصة القيادة.</p>
<p><strong>العادات والتقاليد</strong>: من يذهب إلى بلد أجنبي عليه أن يكون مطلعاً على عاداته وتقاليده، خاصة إذا أراد الاختلاط أو التعامل مع المجتمع المضيف. يلاحظ الزائر عند وصوله إلى المطار أو الفندق بأن الجو هادئ وعملي ويتعامل المسؤولون مع الزائر بأدب واحترام بغض النظر عن جنسه وجنسيته وشكله،&nbsp; انطلاقاً من واقع الأمر والأوراق والوثائق وما شابه ذلك، وقلما يأخذون بعين الاعتبار مركز أو مقام الشخص المعني. &nbsp;</p>

<br/>
<p><strong>قيادة السيارات في ألمانيا والمساعدة عند الحاجة</strong></p>
<p>يحق للسائح الأجنبي قيادة السيارات العادية، لا يدخل فيها الحافلات أو الشاحنات، برخصة القيادة الصادرة له من بلده، شريطة أن تكون هذه الرخصة سارية المفعول لمدة لا تنقص عن سنة. وتفادياً للتعقيدات يستحسن أن تكون رخصة القيادة مترجمة إلى اللغة الألمانية أو الإنجليزية وموثقة من قبل جهة رسمية.</p>
<p><u>ويرجى الافصاح على المنفذ الحدودي عن المبالغ النقدية التي تزيد عن 10.000 (عشرة آلاف) يورو وذلك تفاديا لمصادرة المبالغ الكبيرة ودفع الغرامات.</u></p>
<p>وأيضا الإفصاح بدقه لموظف الجمارك عند الوصول عن كل ما تصطحبه من أمتعة، علماً بأن ألمانيا تحتم على القادم تسجيل ما لديه من مجوهرات ونقود وشيكات وغيرها من المتعلقات الشخصية إذا تجاوزت حداً معين.</p>

<br/>
<p><strong>نصائح وإرشادات هامة للمواطنين</strong><strong>:</strong></p>
<ul>
<li>التأكد من صلاحية جواز السفر وأن تكون المدة المتبقية تكفي لفترة المغادرة والعودة، على أن لا تقل عن ستة أشهر.</li>
<li>ننصح بعمل تأمين صحي خاص بالسفر على خدمة مسافر المتوفرة على موقع وزارة الخارجية والتعاون الدولي على الرابط: <a target="_blank" href="https://www.musafer.ae/musaferApp/loadHomePage.action?request_locale=ar">https://www.musafer.ae/musaferApp/loadHomePage.action?request_locale=ar</a></li>
<li>للتأكد من الإعفاء من تأشيرة الدخول إلى المانيا وكرواتيا ودول الاحاد الاوروبي على موقع وزارة الخارجية والتعاون الدولي على الرابط <a target="_blank" href="https://www.mofaic.gov.ae/ar-ae/visa-exemptions-for-citizen">https://www.mofaic.gov.ae/ar-ae/visa-exemptions-for-citizen</a>.</li>
<li>في حال فقدان جواز السفر والوثائق الرسمية ، يرجى الذهاب إلى أقرب مركز شرطة والتبليغ للحصول على تقرير رسمي بفقدان أو سرقة الوثائق الرسمية المذكورة ومن ثم التواصل مع السفارة على الرقم 030516516 حتى يتم تقديم الإرشادات اللازمة لمساعدتكم.</li>
<li>الحرص على التعامل مع مكاتب سياحية مرخصة وموثوق بها أثناء الحجوزات الخاصة بالسفر.</li>
<li>تجنب حمل مبالغ مالية كبيرة، واستخدام بطاقات الإئتمان بدلاً من ذلك.&nbsp;</li>
<li>ضرورة الحرص على استلام إيصال بالمبالغ التي تم تحويلها إلى عملة اليورو.</li>
<li>الاحتفاظ بجميع إيصالات المشتريات (مثل المبالغ التي تم تحويلها إلى عملة اليورو ، أو المجوهرات والأشياء الثمينة المقتناة) وذلك لإبرازها عند المغادرة تفاديا لتعرضها للمصادرة.</li>
<li>تفادي رهن جواز السفر أو الهوية الوطنية لدى أي جهة من الجهات مهما كانت الأسباب.</li>
<li>الحرص على مراجعة غرف التجارة والصناعة في ألمانيا للتأكد من مصداقية الشركات التي يتم التعامل معها إذا كانت الزيارة تتسم بطابع تجاري، تجنباً للوقوع ضحية للغش والتزوير.</li>
<li>ضرورة التقيد بإجراءات الأمن والسلامة في المطار، بما في ذلك تقنيات المسح الضوئي عند التفتيش الشخصي.</li>
<li>التحقق من الصفة الرسمية للأشخاص الذين يطلبون إبراز الأوراق الثبوتية.</li>
<li>ضرورة المحافظة على جواز السفر وتذاكر الطيران والمتعلقات الشخصية في أماكن آمنة في مقر الإقامة ، حيث توفر الفنادق صندوق أمانات مخصص لحفظ المقتنيات. وكذلك المبادرة بتسجيل عنوان الإقامة ورقم جواز السفر لدى بعثة الإمارات.</li>
<li>عدم منح توكيل عام خارج الدولة لأي فرد أو مؤسسة أو محامٍ، وفي حالة الضرورة يقتصر التوكيل على الموضوع المعني فقط.</li>
<li>فتح حساب مصرفي في ألمانيا: يحق لكل مواطن ألماني أو أجنبي مقيم في ألمانيا أن يفتح حساباً أو حسابات مصرفية من أي نوع كان، ويشترط لهذا تقديم جواز السفر الشخصي وكذلك مستند إثبات تسجيل محل السكن الذي يتم إصداره من مكتب الحي.&nbsp;</li>
</ul>
<br/>
<p><strong>الخدمات الخاصة بشؤون المواطنين</strong><strong>:</strong></p>
<ul>
<li>استقبال بلاغات عن الحوادث والحالات الطارئة للمواطنين: <br/>في حال التعرض لحادث أو أي حالة طارئة يرجى التواصل مع الخط الساخن في السفارة 030516516 حتى نتمكن من مساعدتكم.</li>
</ul>
<p></p>
<ul>
<li>طلب تقديم شكوى: <br/>تستقبل السفارة شكاوى مواطني دولة الإمارات العربية المتحدة وتتعامل معها بجدية مطلقة ، كما يمكنكم تقديم الشكوى عن طريق موقع وزارة الخارجية والتعاون الدولي على الرابط التالي: <br/><a  target="_blank"  href="https://www.mofaic.gov.ae/ar-ae/Services/forms/complaint">https://www.mofaic.gov.ae/ar-ae/Services/forms/complaint</a></li>
</ul>

<br/>
<p><strong>كما وتقوم السفارة بتقديم الخدمات التالية للمواطنين المتواجدين في جمهورية ألمانيا الاتحادية وكرواتيا:</strong></p>
<ul>
<li>خدمة طلب شهادة لمن يهمه الامر للمواطنين</li>
</ul>

<br/>
<p>إن خدمة إصدار شهادة لمن يهمه الأمر هي وثيقة يتم إصدارها توضح أسباب تواجد المواطن خارج الدولة خلال فترة زمنية محدده لتقديمها للجهات الرسمية.</p>

<br/>
<p>متطلبات شهادة لمن يهمه الأمر لمريض أو لمرافق مريض:</p>
<ul>
<li>صورة عن جواز سفر المريض</li>
<li>صورة عن ختم الدخول والخروج لألمانيا (المريض)</li>
<li>صورة عن التقرير الطبي للمريض أو خطاب من الهيئة الصحية المعنية التي يتبع المواطن لها</li>
<li>صورة عن جواز سفرالمرافق</li>
<li>صورة عن ختم الدخول والخروج لألمانيا (المرافق)</li>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى&nbsp;التالي.</a></li>
<li>ملئ استمارة طلب شهادة لمن يهمه الأمر (مرفق استمارة طلب شهادة لمن يهمه الأمر)</li>
<li>خدمة إصدار جواز طوارئ</li>
</ul>

<br/>
<p>لإصدار جواز طوارئ للأفراد يرجى التسجيل على طلب الخدمة على موقع وزارة الخارجية والتعاون الدولي على الرابط التالي&nbsp;</p>
<ul>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services/Emergency-Passport-Services" target="_blank">خدمة إصدار جواز طوارئ في حالة انتهاء أو تلف أو فقدان جواز السفر</a></li>
</ul>

<br/>
<p>تزويد السفارة بالأوراق الثبوتية التالية:</p>
<ul>
<li>صورة جواز السفر و صورة بطاقة الهوية</li>
<li>جواز السفر التالف عند الحضور إلى السفارة</li>
<li>صورة عن الرقم الموحد في آخر الجواز</li>
<li>صورة الجنسية كاملة (خلاصة القيد)</li>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى&nbsp;التالي.</a></li>
<li>في حال فقدان جواز السفر يجب أيضا الحصول على تقرير الشرطة وتصديقه في السفارة</li>
<li>تحديد موعد السفر وعنوان السكن كاملا وأرقام الهواتف بالدولة&nbsp;وذلك من خلال ملئ&nbsp;استمارة طلب جواز&nbsp;سفر&nbsp;طوارىء (مرفق استمارة طلب جواز طوارئ)</li>
</ul>
<ul>
<li>خدمة إصدار جواز طوارئ لمواليد جدد</li>
</ul>

<br/>
<p><strong>الأوراق الثبوتية المطلوبة:</strong></p>
<ul>
<li>شهادة ميلاد الطفل</li>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع رسوم تصديق شهادة الميلاد مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى.</a></li>
<li>صورة عن (خلاصة القيد) كاملة</li>
<li>إذا كانت الزوجة غير مضافة في خلاصة القيد (يرجى تزويدنا بصورة عقد الزواج)</li>
<li>صورة من جوازات سفر الوالدين</li>
<li>صورة بطاقة الهوية للأب</li>
<li>صورة عن بطاقة الهوية للأم</li>
<li><a href=" https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع رسوم إصدار جواز الطوارئ مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى</a>.</li>
<li>تحديد موعد السفر وعنوان السكن كاملا وأرقام الهواتف بالدولة&nbsp;وذلك من خلال ملئ&nbsp;استمارة طلب جواز&nbsp;سفر&nbsp;طوارىء (مرفق استمارة طلب جواز طوارئ)</li>
<li>خدمة إصدار وكالة خاصة أو عامة (خاصة بمواطني دولة الإمارات العربية المتحدة)</li>
</ul>

<br/>
<p><strong>المتطلبات:</strong></p>
<ul>
<li>صورة عن جواز سفر الموكل أو هوية الموكل</li>
<li>صورة عن جواز سفر الوكيل أو هوية الوكيل</li>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع رسوم إصدار الوكالة مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى&nbsp;التالي</a>.&nbsp;</li>
<li>لإصدار الوكالة يجب حضور الموكل شخصيا إلى السفارة.</li>
</ul>

<br/>
<p><strong>خدمة تصديق الشهادات الألمانية والوثائق الطبية:</strong></p>
<ul>
<li>لتصديق التقارير الطبية يرجى مراعاة تصديق التقرير الطبي من وزارة الصحة الألمانية أو الهيئات الصحية التابعة لها (دائرة الصحة &ndash; نقابة الأطباء)</li>
<li><a href="https://www.bva.bund.de/EN/Home/home_node.html" target="_blank">تصديق التقارير الطبية الألمانية من مكتب التصديقات الاتحادي في مدينة كولون</a></li>
<li><a href="https://www.mofaic.gov.ae/ar-ae/Services?Category={ad9bf278-9962-43d0-9a0a-19eb6dda89bb}" target="_blank">دفع رسوم التصديق مبلغ 150 درهم أو ما يعادلها بالعملة الأوروبية مع وجود إمكانية الدفع أيضا&nbsp;عن طريق الموقع الإلكترونى&nbsp;التالي.</a></li>
</ul>
<p>&nbsp;</p>',
                'content' => '',
                'page_id' => 4,
                'parent_section_id' => ''
            ],
        ];

        foreach($data as $item){
            $section['title_en'] = $item['title_en'];
            $section['title_ar'] = $item['title_ar'];
            $section['title'] = $item['title'];
            $section['content_en'] = $item['content_en'];
            $section['content_ar'] = $item['content_ar'];
            $section['content'] = $item['content'];
            $section['page_id'] = $item['page_id'];
            $section['parent_section_id'] = $item['parent_section_id'];
            $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
            $new = \App\Models\PageSection::create($section);

            if($new && isset($item['children'])){
                foreach ($item['children'] as $child){
                    $section['title_en'] = $child['title_en'];
                    $section['title_ar'] = $child['title_ar'];
                    $section['title'] = $child['title'];
                    $section['content_en'] = $child['content_en'];
                    $section['content_ar'] = $child['content_ar'];
                    $section['content'] = $child['content'];
                    $section['page_id'] = $item['page_id'];
                    $section['slug'] = \Illuminate\Support\Str::slug($child['title_en']);
                    $section['parent_section_id'] = $new->id;

                    \App\Models\PageSection::create($section);
                }
            }
        }
    }
}
