<?php

use Illuminate\Database\Seeder;

class BilateralContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'Bilateral Relations',
                'title_ar' => 'العلاقات الثنائية ',
                'title' => 'Bilaterale Beziehungen',
                'content_en' => '<img src="//uae-embassy.de/public/img/pages/flags-2.jpg" width="100%">
							<br/>
							<br/>
							<p>The United Arab Emirates established diplomatic relations with the Federal Republic of Germany in May 1972. UAE Diplomatic representation in Germany goes back to 1976 when the first UAE Embassy was initially founded in Bonn, West Germany, and subsequently in Berlin after Germany’s reunification. With the enhancement of bilateral relations, diplomatic representation expanded to include: The Embassy in Berlin, the General Consulate in Munich, the Embassy Branch Office in Bonn.</p>
							<br/>
							<p><strong>UAE – Germany: Nearly 50 years of close bilateral cooperation</strong></p>
							',
                'content_ar' => '<img src="//uae-embassy.de/public/img/pages/flags-2.jpg" width="100%">
							<br/>
							<br/><p>بدأت دولة الإمارات العربية المتحدة في إنشاء علاقات دبلوماسية مع جمهورية المانيا الاتحادية عام 1972. ويعود تمثيل دولة العربية المتحدة الدبلوماسي في جمهورية المانيا الاتحادية إلى عام 1976، حيث تم تأسيس أول سفارة للدولة في بون، المانيا الغربية، ثم انتقلت إلى برلين بعد توحيد المانيا. مع تطور وتعزيز العلاقات الثنائية بين البلدين، توسع التمثيل الدبلوماسي لدولة الإمارات العربية المتحدة في جمهورية المانيا الاتحادية ليشمل سفارة دولة الإمارات في برلين، والقنصلية العامة لدولة الإمارات في ميونخ، وفرع سفارة دولة الإمارات في بون.</p>
<p>&nbsp;</p>
<p><strong>دولة الإمارات العربية المتحدة &ndash; جمهورية المانيا الاتحادية: ما يقارب 50 عاماً من التعاون الثنائي الوثيق</strong></p>',
                'content' => '<img src="//uae-embassy.de/public/img/pages/flags-2.jpg" width="100%">
							<br/>
							<br/>
							<p>Die Vereinigten Arabischen Emirate haben im Mai 1972 diplomatische Beziehungen zur Bundesrepublik Deutschland aufgenommen. Die diplomatische Vertretung der VAE in Deutschland geht auf das Jahr 1976 zurück, als die erste Botschaft der VAE zunächst in Bonn und nach der Wiedervereinigung Deutschlands in Berlin gegründet wurde. Mit der Vertiefung der bilateralen Beziehungen wurde die diplomatische Vertretung um das Generalkonsulat in München und die Botschaftsfiliale in Bonn erweitert.</p>
							<br/>
							<p><strong>UAE – Germany: Nearly 50 years of close bilateral cooperation</strong></p>
							<br/>',
                'page_id' => 2,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Political Relations',
                'title_ar' => '	العلاقات السياسية',
                'title' => 'Politische Beziehungen',
                'content_en' => '<img src="//uae-embassy.de/public/img/bilateral.jpg" width="100%">
							<br/>
							<br/>
							<p>Our countries have convergent interests in many global and regional matters and have worked together in the United Nations and other international fora on issues ranging from fighting terrorism to stabilization in Iraq and Syria and Afghanistan and the protection of the environment.</p>
							<p>The official and unofficial delegation trips and visits, the exchange of ideas and the mutual consultations in regard to regional and international matters are a distinct expression of both countries&rsquo; strong and sincere will to advance and deepen the bilateral relations. Both countries are convinced of and committed to the importance of their cooperation for the sake of fostering international peace and global security, both of which are necessary requirements for the economic prosperity of the world.</p>
							<p>The unanimity between both governments reached its climax with the signing of a memorandum of understanding in April 2004 by then Deputy Prime Minister of the United Arab Emirates, H.H. Sheikh Hamdan Bin Zayed Al Nahyan, and retired Federal Chancellor Gerhard Schr&ouml;der. Since then, this memorandum has been the foundation of the advancement of the cooperation of both states in all areas. The memorandum is the foundation for the political consultations between both governments.</p>
							<p>During Federal Chancellor Angela Merkel&rsquo;s visit to Abu Dhabi in May 2010, H.H. Sheikh Khalifa Bin Zayed Al Nahyan, President of the United Arab Emirates, highlighted the UAE&rsquo;s wish to further expand the bilateral cooperation between both countries in all areas. The Federal Chancellor stated the United Arab Emirates is Germany&rsquo;s most important trade partner in the Arab world and declared that the strategic partnership between both countries represented a model for all German-Arab relations. The significance of the Gulf region has in all respects been increasing not only for Germany, but for the entire European Union.</p>
							<p>Regular visits reflect the close cooperation between the two countries. In June 2019, H.H. Sheikh Mohamed bin Zayed and H.H. Sheikh Abdulla bin Zayed came to Berlin for extensive talks with Federal President Frank-Walter Steinmeier, Federal Chancellor Angela Merkel, Federal Minster for Foreign Affairs Heiko Maas, and Federal Minister for Economic Affairs and Energy Peter Altmaier.</p>
							<p>On the occasion of this visit, the UAE and Germany issued a joint statement in which they reiterated their unequivocal rejection of all forms of terrorism that represent a threat to international peace and stability, and ensured cooperation in matters of security policy. Crown Prince Mohammed bin Zayed Al Nahyan and Chancellor Angela Merkel also emphasized the friendly bond and stable partnership between the two countries.</p>
							<br/>
							<p><strong>Related links: </strong></p>
							<p><a target="_blank" href="https://www.mofaic.gov.ae/en">UAE Ministry of Foreign Affairs and International Cooperation</a>.</p>
							<p><a target="_blank" href="https://www.auswaertiges-amt.de/en">German Federal Foreign Office (Ausw&auml;rtiges Amt)</a>.</p>',
                'content_ar' => '<img src="//uae-embassy.de/public/img/bilateral.jpg" width="100%">
							<br/>
							<br/><p>يرجع تاريخ العلاقات الدبلوماسية بين الإمارات العربية المتحدة وجمهورية ألمانيا الاتحادية إلى عام 1972 أي بعد عام واحد من تأسيس دولة الإمارات. وتم إنشاء سفارة دولة الإمارات العربية المتحدة في بون في العام 1976 انتقلت إلى برلين بعد إعادة توحيد ألمانيا.</p>
<p>ونشأت الشراكة الاستراتيجية بين البلدين في أبريل عام 2004 بموجب مذكرة التفاهم التي وقعها صاحب السمو الشيخ حمدان بن زايد آل نهيان نائب رئيس الوزراء، والمستشار الألماني الأسبق غيرهارد شرودر. وفي يناير عام 2009 اتفق البلدان على عقد مشاورات سياسية منتظمة لوزراء الخارجية، كما أعلنت حكومتا البلدين عن رغبتهما في زيادة التعاون السياسي بين البلدين.</p>
<br/>
<p>وقد شهدت الأعوام الماضية تبادل الزيارات بين قيادات البلدين، ففي شهر مايو 2010 أجرت المستشارة الألمانية أنغيلا ميركل زيارة إلى أبوظبي التقت فيها سمو الشيخ خليفة بن زايد آل نهيان رئيس الدولة وحاكم أبوظبي، حيث منحها سمو الشيخ خليفة "وسام زايد" تجسيداً لعمق علاقات التعاون والصداقة بين البلدين، هذا بالإضافة إلى زيارتها أبوظبي في مايو 2017 ولقائها سمو الشيخ محمد بن زايد آل نهيان، ولي عهد أبوظبي ونائب القائد الأعلى للقوات المسلحة. كما تتوالى زيارة الوفود السياسية الإماراتية لألمانيا بشكل متواصل، وكان آخرها زيارة صاحب السمو الشيخ محمد بن زايد آل نهيان على رأس وفد سياسي رفيع المستوى في شهر يناير 2020 رافقه فيه سمو الشيخ عبدالله بن زايد آل نهيان وزير الخارجية والتعاون الدولي، ومعالي سلطان الجابر وزير الدولة، التقوا فيه فخامة رئيس الجمهورية الألماني د. فرانك فالتر شتاينماير ومعالي المستشارة الألمانية د. أنغيلا ميركل.</p>
<br/>
<p>وهناك تعاون سياسي وطيد بين دولة الإمارات العربية المتحدة وجمهورية ألمانيا الاتحادية في عدد من القضايا المطروحة على الساحة الدولية، أبرزها التعاون في إطار مجموعة أصداقاء الشعب السوري، حيث يرأس البلدان مجموعة عمل إعادة الإعمار والتنمية وكذلك صندوق إعادة إعمار سوريا، كما يتشاركان في قيادة مجموعة عمل إرساء الاستقرار في إطار مكافحة تنظيم داعش.&nbsp;</p>
<br/>
							<p><strong>Related links: </strong></p>
							<p><a target="_blank" href="https://www.mofaic.gov.ae/en">UAE Ministry of Foreign Affairs and International Cooperation</a>.</p>
							<p><a target="_blank" href="https://www.auswaertiges-amt.de/en">German Federal Foreign Office (Ausw&auml;rtiges Amt)</a>.</p>',
                'content' => '<img src="//uae-embassy.de/public/img/bilateral.jpg" width="100%">
							<br/>
							<br/>
							<p>Unsere L&auml;nder verfolgen gemeinsame Interessen hinsichtlich vieler globaler und regionaler Themen und haben innerhalb des Verbundes der Vereinten Nationen und anderer internationaler Organisationen bei zahlreichen Anl&auml;ssen &ndash; von der Bek&auml;mpfung des islamistischen Terrors &uuml;ber die Stabilisierung der Lage im Irak, Syrien und in Afghanistan bis hin zum Umweltschutz &ndash;&nbsp; eng zusammengearbeitet.</p>
                            <p>Die offiziellen und inoffiziellen Delegationsreisen und Besuche, der Meinungsaustausch und die gegenseitigen Konsultationen &uuml;ber regionale und internationale Fragen sind ein deutlicher Ausdruck des starken und aufrichtigen Willens beider Seiten, ihre bilateralen Beziehungen voranzubringen und zu vertiefen.. Beide L&auml;nder sind von der Bedeutung ihrer Zusammenarbeit f&uuml;r die F&ouml;rderung des internationalen Friedens und der globalen Sicherheit, die auch notwendige Voraussetzungen f&uuml;r den wirtschaftlichen Wohlstand in der Welt sind, &uuml;berzeugt.</p>
                            <p>Seinen H&ouml;hepunkt erreichte das Einvernehmen zwischen beiden Regierungen mit der Unterzeichnung einer Absichtserkl&auml;rung im April 2004 durch den damaligen stellvertretenden Ministerpr&auml;sidenten der Vereinigten Arabischen Emirate, S.H. Sheikh Hamdan Bin Zayed Al Nahyan, und den damaligen deutschen Bundeskanzler Gerhard Schr&ouml;der. Seither bildet diese Erkl&auml;rung die Grundlage f&uuml;r den Ausbau der Zusammenarbeit beider Staaten in allen Bereichen und hat sich f&uuml;r die Ausgestaltung der zwischenstaatlichen Beziehungen sowie in internationalen Krisen bew&auml;hrt. Sie dient auch als Basis f&uuml;r die politischen Konsultationen zwischen beiden Regierungen.</p>
                            <p>Der Pr&auml;sident der Vereinigten Arabischen Emirate, S.H. Scheikh Khalifa Bin Zayed Al Nahyan, betonte w&auml;hrend des Besuchs von Bundeskanzlerin Angela Merkel im Mai 2010 den Wunsch seines Landes, die bilaterale Zusammenarbeit zwischen beiden L&auml;ndern auf allen Gebieten weiter auszubauen. Die Bundeskanzlerin hob hervor, dass die Vereinigten Arabischen Emirate der erste Handelspartner Deutschlands in der arabischen Welt sind und dass die strategische Partnerschaft zwischen beiden L&auml;ndern ein Vorbild f&uuml;r die deutsch-arabischen Beziehungen darstellt. Die Bedeutung der Golfregion nimmt in jeder Hinsicht nicht nur f&uuml;r Deutschland, sondern f&uuml;r die gesamte Europ&auml;ische Union zu.</p>
                            <p>Regelm&auml;&szlig;ige Besuche spiegeln die enge Zusammenarbeit zwischen den beiden L&auml;ndern wider. Im Juni 2019 kamen S.H. Scheikh Mohamed bin Zayed und S.H. Scheikh Abdulla bin Zayed (<a href="https://www.mofaic.gov.ae/en/">Ministerium f&uuml;r Ausw&auml;rtige Angelegenheiten und Internationale Zusammenarbeit der VAE</a>) im Juni 2019 zu ausf&uuml;hrlichen Gespr&auml;chen mit Bundespr&auml;sident Frank-Walter Steinmeier, Bundeskanzlerin Angela Merkel, Au&szlig;enminister Heiko Maas (<a href="https://www.auswaertiges-amt.de/de">Ausw&auml;rtiges Amt</a>) und Bundesminister f&uuml;r Wirtschaft und Energie Peter Altmaier nach Berlin.</p>
                            <p>Anl&auml;sslich dieses Besuchs gaben die VAE und Deutschland eine gemeinsame Erkl&auml;rung ab, in der sie ihre unmissverst&auml;ndliche Ablehnung aller Formen des Terrorismus, die eine Bedrohung des Weltfriedens und der internationalen Stabilit&auml;t darstellen, bekr&auml;ftigten, und sicherheitspolitische Zusammenarbeit in diesem Bereich versicherten. Kronprinz Mohammed bin Zayed Al Nahyan und Bundeskanzlerin Angela Merkel unterstrichen zudem die freundschaftliche Verbundenheit und die stabile Partnerschaft beider L&auml;nder.</p>
						',
                'page_id' => 2,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Economic Relations',
                'title_ar' => 'العلاقات الاقتصادية',
                'title' => 'Wirtschaftsbeziehungen',
                'content_en' => '<img src="//uae-embassy.de/public/img/pages/economic.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>Strategic Economic Cooperation between the UAE and Germany</strong></p>
							<p>STRATEGIC PARTNERSHIP GERMANY-UAE Germany and the UAE have maintained close and friendly relations since 1972, in which economic cooperation plays an important role. The economic activities of companies in both countries are effectively supported by high-level dialogue. A strategic partnership was agreed upon in April 2004 and regular political consultations of the Foreign Ministries in January 2009. The purpose of this strategic partnership has been affirmed by both sides to help create greater security, stability and prosperity for both countries and the region as a whole. In recent years, a significant number of high-level visits and a lively exchange of delegations have taken place. To further strengthen their ties, both countries signed a joint declaration of intent on the occasion of the visit to Berlin by Crown Prince Mohammed bin Zayed Al Nahyan in June 2019.</p>
							<p>The strategic objectives and priorities of economic cooperation are agreed upon by the two governments at meetings of the Joint Commission for Economic and Technical Cooperation. The 11th and most recent meeting of the Joint Commission took place in June 2019 in Berlin. The delegations were led by the two ministers of economic affairs Sultan Bin Saeed Al Mansoori and Peter Altmaier. The main topics discussed were issues of mutual easing of market access and diversification of goods and services to be exchanged. In addition, both governments committed to encourage their companies to strengthen cooperation in energy, climate, infrastructure, metrology, health, education and vocational training, and to make the necessary investments. Key focus was also on the ongoing preparations for the World Expo 2020.</p>
							<p>In January 2017 in Abu Dhabi, both governments agreed to an energy partnership. The aim of this partnership was to strengthen bilateral dialogue and practical exchange in the fields of renewable energy, energy efficiency, electricity market development, electro-mobility and sustainable transport, as well as research and development in the energy sector. The cooperation includes delegation visits, expert workshops and other event formats that provide a platform for exchange between government officials, business and science representatives from both countries. Overall, the economic dialogue has intensified considerably in the recent past.</p>
							<br/>
							<p><strong>Trade </strong></p>
							<p>The United Arab Emirates is Germany\'s most important Arab trading partner, while Germany is the UAE\'s largest European trading partner, accounting for about 22% of total Arab-German trade. Germany\'s main export goods to the UAE are aircraft, cars, machinery, electrical appliances and chemical products. German imports amounted to around 1.2 billion euros in 2018. These are mainly aluminium and petrochemical products (no crude oil). The non-oil trade between the two countries recorded a total growth of 60 percent from 2010 to 2017 and reached around 13.45 billion US dollars in 2017. Hundreds of German companies are now operating in the UAE, as well as 14,000 German citizens living in the UAE and contributing to the country\'s development process.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
							<p>According to the German-Emirati Chamber of Commerce in the UAE, products &ldquo;Made in Germany&rdquo; are highly valued and are associated with particularly high-quality standards, reliability and state-of-the-art technology. In addition to its motor vehicles, Germany is known above all for machines that enjoy an excellent reputation and are considered to be reliable, durable and technologically advanced. Good business opportunities open up for German companies in the energy sector, especially in renewable energy and generators, and in environmental technology, such as waste disposal, recycling or water treatment. In the healthcare sector, German companies are sought-after partners with their modern medical technology and comprehensive range of services. Germany is also very sought after for medical tourism.</p>',
                'content_ar' => '<img src="//uae-embassy.de/public/img/pages/economic.jpg" width="100%">
							<br/>
							<br/><p>تربط دولة الإمارات العربية المتحدة منذ تأسيسها وجمهورية ألمانيا الاتحادية علاقات اقتصادية وثيقة قائمة على التعاون والمنفعة المتبادلة بما يخدم مصالح البلدين. ويرجع اهتمام ألمانيا بدولة الامارات اقتصادياً إلى الموقع الاستراتيجي في منطقة الخليج العربي والبنية التحتية الاستثمارية المتميزة التي تجعل من دولة الإمارات مركزاً متميزاً للشركات العالمية المهتمة بالعمل في الأسواق الأسيوية والشرق الأوسط. في الوقت نفسه تساهم الكفاءة العالية لأسواق رأس المال والتأمينات القانونية في أن تحتل ألمانيا مكانة متقدمة في مراكز الأعمال الدولية، خاصة مع كون ألمانيا قاطرة الاقتصاد في القارة الأوروبية وما يحظى به اقتصادها من تنوع وارتفاع مستوى المصنوعات وجودة قطاع الخدمات والعلاقات التجارية الضخمة مع جيرانها الأوروبيين.&nbsp;</p>
<p>وفي إطار الحرص على دعم العلاقات الاقتصادية اتفقت حكومتا البلدين على إنشاء اللجنة الإماراتية الألمانية للتعاون الاقتصادي والتجاري والفني الذي تجتمع بشكل منتظم كل عامين ويشارك فيها كبار المسؤولين الحكوميين والهيئت الاقتصادية والشركات الخاصة من كلا البلدين لبحث أفاق التعاون الاقتصادي وتعزيز أطر التجارة والاستثمار. وقد أسفر اجتماع اللجنة المشتركة الحادي عشر الذي أقيم في شهر مارس 2019 الذي ترأسها معالي المهندس سلطان بن سعيد المنصوري وزير الاقتصاد الإماراتي ومعالي بيتر ألتماير وزير الاقتصاد والطاقة الألماني عن اتفاق الجانبين على مجموعة من المسارات لتنمية الشراكة والتعاون في مجالات الطاقة وسياسات المناخ والاقتصاد الأخضر وتطوير البنية التحتية&nbsp; كمحور رئيسي للتعاون ضمن برنامج اللجنة، بما يشمل شبكات القطارات والنقل البحري والطيران المدني.</p>
<p>كما يدل مستوى التجارة الخارجية بين البلدين على متانة العلاقات الاقتصادية، حيث بلغ حجم التبادل التجاري المباشر عام 2015 إلى إجمالي 10.5 مليار يورو، بما جعل دولة الإمارات أحد أكبر الشركاء التجاريين لألمانيا في منطقة الشرق الأوسط وشمال أفريقيا. وتتمثل أهم صادرات ألمانيا إلى الإمارات في الطائرات والسيارات والماكينات والمنتجات الكيميائية والكهربائية، بينما تتمثل صادرات دولة الإمارات إلى ألمانيا في الألومنيوم والمنتجات البتروكيميائية. وليس هناك تجارة نفطية بين البلدين.</p>
<p>وبالإضافة للتبادل التجاري، هناك أكثر من 900 شركة ألمانية أقامت مركزاً رئيسيا أو أفرعاً لها في دولة الإمارات، من أبرزها "سيمنز" و"SAP"، وتعمل الشركات الألمانية في قطاعات التجارة وصيانة المحركات والخدمات اللوجستية ةالمالية والتأمينات والأنشطة العقارية والنقل وقطاع المعلومات. من جهة أخرى تعتبر شركة "غلوبال فاونداريز" العاملة بصناعة الشرائح الإلكترونية هي أهم الاستثمارات الإماراتية في ألمانيا.&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>وعملاً على تشجيع فرص الاستثمار أمام الشركات في كلا البلدين تم في العام&nbsp; 2009 تأسيس <a href="https://vae.ahk.de/en/?mobileversion=2&amp;cHash=5312ffcf4a54343b86ff7a639884ff97">المجلس المجلس الألماني الإماراتي المشترك للصناعة والتجارة</a> الذي يضم حالياً أكثر من 500 شركة ألمانية وإماراتية، ويعمل المجلس على تقديم الخدمات الاستشارية للشركات الراغبة في الاستثمار في البلدين.</p>
<p>&nbsp;</p>
<p>لمزيد من المعلومات عن التعاون الاقتصادي بين البلدين يرجى زيارة المواقع التالية:</p>
<p>&nbsp;</p>
<p><a href="https://www.economy.gov.ae/Arabic/pages/default.aspx">وزارة الاقتصاد لدولة الإمارات</a></p>
<p><a href="https://ghorfa.de/ar/">غرفة التجارة والصناعة العربية الألمانية</a></p>
<p><a href="http://www.uaefreezones.com/">المناطق الحرة بدولة الإمارت العربية المتحدة</a></p>
<p><a href="http://www.dubaifdi.gov.ae/Arabic/pages/default.aspx">مؤسسة دبي لتنمية الاستثمار</a></p>
<p><a href="https://www.abudhabichamber.ae/ar-AE">غرفة أبوظبي</a></p>
<p><a href="https://www.dubaichamber.com/ar">غرفة دبي</a></p>
<p><a href="https://added.gov.ae/ar-AE">دائرة التنمية الاقتصادية - أبوظبي</a></p>
<p><a href="http://www.dubaided.ae/Arabic/pages/default.aspx">اقتصادية دبي</a></p>
<p><a href="http://www.ded.rak.ae/en/Pages/default.aspx">دائرة التنمية الاقتصادية &ndash; رأس الخيمة</a></p>
<p><a href="https://www.sedd.ae/web/sedd/home">دائرة التنمية الاقتصادية &ndash; الشارقة</a></p>
<p><a href="https://www.ajmanded.ae/default.aspx">دائرة التنمية الاقتصادية &ndash; عجمان</a></p>
<p><a href="http://ded.uaq.ae/ar/home.html?&amp;languageCode=en">دائرة التنمية الاقتصادية &ndash; أم القيوين</a></p>
<p><a href="https://www.smartdubai.ae/ar/home-page">دبي الذكية</a></p>
<p><a href="https://www.expo2020dubai.com/ar">الموقع الرسمي لإكسبو 2020 دبي</a></p>
<p>&nbsp;</p>',
                'content' => '<img src="//uae-embassy.de/public/img/pages/economic.jpg" width="100%">
							<br/>
							<br/>
							<p>STRATEGISCHE PARTNERSCHAFT DEUTSCHLAND-VAE. Deutschland und die VAE unterhalten seit 1972 enge und freundschaftliche Beziehungen, in denen die wirtschaftliche Zusammenarbeit eine wichtige Rolle spielt. Die wirtschaftlichen Aktivit&auml;ten der Unternehmen in beiden L&auml;ndern werden durch Dialog auf hoher Ebene wirksam unterst&uuml;tzt. Im April 2004 wurden eine strategische Partnerschaft und im Januar 2009 regelm&auml;&szlig;ige politische Konsultationen der Au&szlig;enministerien vereinbart. Beide Seiten haben den Zweck dieser strategischen Partnerschaft damit untermauert, mehr Sicherheit, Stabilit&auml;t und Wohlstand f&uuml;r beide L&auml;nder und die gesamte Region zu schaffen. In den letzten Jahren haben zahlreiche hochrangige Besuche und ein reger Austausch von Delegationen stattgefunden. Um ihre Beziehungen weiter zu st&auml;rken, unterzeichneten beide L&auml;nder anl&auml;sslich des Besuchs von Kronprinz Mohammed bin Zayed Al Nahyan im Juni 2019 in Berlin eine gemeinsame Absichtserkl&auml;rung.</p>
                            <p>Die strategischen Ziele und Priorit&auml;ten der wirtschaftlichen Zusammenarbeit werden von den beiden Regierungen im Rahmen der Sitzungen der Gemeinsamen Kommission f&uuml;r wirtschaftliche und technische Zusammenarbeit festgelegt. <a href="https://sp.mofaic.gov.ae/EN/DiplomaticMissions/Embassies/Berlin/AboutMission/Pages/11th-Session-of-the-German-%E2%80%93-UAE-Joint-Commission.aspx">Die 11. und vorerst letzte Sitzung der Gemeinsamen Kommission</a> fand im Juni 2019 in Berlin statt. Die Delegationen wurden von den beiden Wirtschaftsministern Sultan Bin Saeed Al Mansoori (<a href="https://www.economy.gov.ae/english/Pages/default.aspx">Wirtschaftsministerium der VAE</a>) und Peter Altmaier (<a href="https://www.bmwi.de/Navigation/DE/Home/home.html">Bundesministerium f&uuml;r Wirtschaft und Energie</a>) geleitet. Die Hauptthemen waren Fragen der gegenseitigen Erleichterung des Marktzugangs und der Diversifizierung der auszutauschenden Waren und Dienstleistungen. Dar&uuml;ber hinaus haben sich beide Regierungen verpflichtet, ihre Unternehmen darin zu best&auml;rken, die Zusammenarbeit in den Bereichen Energie, Klima, Infrastruktur, Messtechnik, Gesundheit, allgemeine und berufliche (Aus-)Bildung zu intensivieren und die daf&uuml;r erforderlichen Investitionen zu t&auml;tigen. Im Mittelpunkt standen auch die laufenden Vorbereitungen f&uuml;r die <a href="https://www.expo2020dubai.com/de">Weltausstellung Expo 2020</a> in Dubai.</p>
                            <p>Im Januar 2017 einigten sich die Regierungen beider L&auml;nder in Abu Dhabi auf eine Energiepartnerschaft. Ziel dieser Partnerschaft ist es, den bilateralen Dialog und den praktischen Austausch in den Bereichen erneuerbare Energien, Energieeffizienz, Entwicklung des Strommarktes, Elektromobilit&auml;t und nachhaltiger Verkehr sowie Forschung und Entwicklung im Energiesektor zu st&auml;rken. Die Zusammenarbeit umfasst Delegationsbesuche, Expertenworkshops und andere Veranstaltungsformate, die eine Plattform f&uuml;r den Austausch zwischen Regierungsbeamten, Vertretern aus Wirtschaft und Wissenschaft aus beiden L&auml;ndern bieten. Insgesamt hat sich der wirtschaftliche Dialog in der j&uuml;ngeren Vergangenheit erheblich intensiviert.</p>
                            <p><strong>Handel</strong></p>
                            <p>Die Vereinigten Arabischen Emirate sind der wichtigste arabische Handelspartner Deutschlands, w&auml;hrend Deutschland der gr&ouml;&szlig;te europ&auml;ische Handelspartner der VAE ist, auf den etwa 22% des gesamten arabisch-deutschen Handels entfallen. Deutschlands Hauptexportg&uuml;ter in die VAE sind Flugzeuge, Autos, Maschinen, Elektroger&auml;te und chemische Produkte. Deutsche Importe waren haupts&auml;chlich Aluminium und petrochemische Produkte (kein Roh&ouml;l). Der nicht-&Ouml;l-Handel zwischen den beiden L&auml;ndern verzeichnete von 2010 bis 2017 ein Gesamtwachstum von 60 Prozent und erreichte 2017 rund 13,45 Milliarden US-Dollar (11,92 Milliarden Euro). Hunderte von deutschen Unternehmen sind heute in den VAE t&auml;tig, ebenso wie 14.000 deutsche Staatsb&uuml;rger, die in den VAE leben und zum Entwicklungsprozess des Landes beitragen.</p>
                            <p>Laut der <a href="https://vae.ahk.de/">Deutsch-Emiratischen Industrie- und Handelskammer (AHK)</a> in den Vereinigten Arabischen Emiraten haben Produkte &bdquo;Made in Germany&ldquo; einen hohen Stellenwert und werden mit besonders hohen Qualit&auml;tsstandards, Zuverl&auml;ssigkeit und modernster Technologie verbunden. Deutschland ist neben seinen Kraftfahrzeugen vor allem f&uuml;r Maschinen bekannt, die einen hervorragenden Ruf genie&szlig;en und als zuverl&auml;ssig, langlebig und technologisch fortschrittlich gelten. Im Energiesektor er&ouml;ffnen sich deutschen Unternehmen gute Gesch&auml;ftsm&ouml;glichkeiten, insbesondere im Bereich erneuerbare Energien und Generatoren sowie bei Umwelttechnologien wie Abfallentsorgung, Recycling oder Wasseraufbereitung. Im Gesundheitswesen sind deutsche Unternehmen mit ihrer modernen Medizintechnik und ihrem umfassenden Leistungsspektrum gefragte Partner. Deutschland ist auch bei dem Thema Medizintourismus sehr gefragt.</p>
						',
                'page_id' => 2,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Cultural Relations',
                'title_ar' => '	العلاقات الثقافية',
                'title' => 'Kulturelle Beziehungen',
                'content_en' => '<img src="//uae-embassy.de/public/img/pages/louvre-2.jpg" width="100%">
							<br/>
							<br/>
							<p>Cultural relations between the United Arab Emirates and Germany date back to the early years of the United Arab Emirates and have since resulted in a continuously growing number of bilateral cooperation initiatives.</p>
							<p>Since May 2006, the Federal Republic of Germany&rsquo;s cultural institute, <a target="_blank" href="https://www.goethe.de/ins/ae/en/index.html">Goethe-Institut</a> (GI), has been represented with an Institute for the Gulf region in Abu Dhabi Certified Goethe-Institut German classes have also been offered in Dubai since 2007.</p>
							<p>Literature is at the forefront of bilateral cultural cooperation. <a target="_blank" href="https://adbookfair.com/en/default.aspx">The annual Abu Dhabi International Book Fair (ADIBF)</a> has cooperated closely with the Frankfurt Book Fair.</p>
							<p>The Abu Dhabi International Book Fair is the most professionally organized, most diverse, and fastest growing book fair in the Middle East and North Africa. Abu Dhabi is the motor driving the professional and commercial development of the regional publishing industry, and is financially and philosophically committed to the long-term expansion of the global book business. In 2019, the <a target="_blank" href="https://tcaabudhabi.ae/en/default.aspx">Department of Culture and Tourism - Abu Dhabi (DCT Abu Dhabi)</a> has announced Germany as Guest of Honour for the 31st edition of the Abu Dhabi International Book Fair (ADIBF) 2021.</p>
							<p>In 2009, a memorandum of understanding was signed between the Johannes Gutenberg-University Mainz and the Kalima Abu Dhabi section to foster the translation of German literature. Each year approximately 20 books are translated into the Arabic language and presented during the Book Fair Frankfurt and the International Book Fair Abu Dhabi. Since the beginning of this unique and long-term project, more than 150 translations of novels have come into life.</p>
							<p>In the area of museology, the <a target="_blank" href="https://www.sharjahmuseums.ae/">Sharjah Museums Authority</a>, Staatliche Museen Berlin / Stiftung Preu&szlig;ischer Kulturbesitz, the University of Applied Sciences Berlin (HTW) and the Goethe-Institut Gulf Region first agreed upon a comprehensive Memorandum of Understanding in 2013 which was to include exhibition development, museum education and professional development initiatives. The <a target="_blank" href="https://www.goethe.de/ins/ae/en/kul/sup/sawa-museum-academy.html">SAWA Museum Academy</a> is the first ever explicitly transcultural capacity building program for current and future museum personnel in the Arab countries and Germany. The program is dedicated to the exchange of ideas on appropriate museum studies across continents and cultures to encourage learning from and with each other.</p>
							<p>When the Emirate of Sharjah in the United Arab Emirates celebrated its nomination as Capital of Islamic Culture in 2014, the <a target="_blank" href="https://www.sharjahmuseums.ae/en-US/Museums/Sharjah-Museum-of-Islamic-Civilization">Sharjah Museum of Islamic Civilisation</a> and the Museum f&uuml;r Islamische Kunst in Berlin fostered this cooperation with workshops and exchanges.</p>
							<p>Most recently, March 2019 saw the launch of cooperation in the film sphere between the <a target="_blank" href="https://admaf.org/">Abu Dhabi Music &amp; Arts Foundation (ADMAF)</a> and the Filmakademie Baden‑W&uuml;rttemberg in Ludwigsburg.</p>
							<br/>
							<p><strong>Education and Training</strong></p>
							<p>There are 15 university partnerships between universities and academic institutions in the two countries. There are officially recognized German schools abroad in <a target="_blank" href="https://www.gisad-school.com/">Abu Dhabi</a>, <a target="_blank" href="http://www.dssharjah.org/">Sharjah</a>, and <a target="_blank" href="http://germanschool.ae/?lang=de">Dubai</a>. Since 2011 and 2012, pupils can earn the German International Abitur in Abu Dhabi and Dubаi, respectively, and since the 2014/15 academic year these schools also offer the Multilingual International Baccalaureate (IB).</p>
							<p>In regard to bilateral cooperation in the area of Higher Education, some of the most important agreements between Emirati and German Universities are listed below:</p>
							<ul>
							<li>German-UAE College of Logistics (GUCL), cooperation agreement since 2011 between the Higher Colleges of Technology (HCT) in Abu Dhabi, the TU Wildau and Jade Hochschule</li>
							<li>Cooperation agreement between Zayed University and the Hochschule f&uuml;r angewandte Wissenschaften M&uuml;nchen (2016)</li>
							<li>Cooperation agreement between the Dubai Health Authority and the medical association Berlin (&Auml;rztekammer Berlin) and the German Association for Orthopedics and Emergency Surgery (Deutsche Gesellschaft f&uuml;r Orthop&auml;die und Unfallchirurgie) (medical specialist training program) since 2014</li>
							<li>Cooperation agreement between Charit&eacute; and the Department of Education and Knowledge in Abu Dhabi (medial specialist training program) since 2018</li>
							<li>Cooperation agreement between medbo Bezirksklinikum Regensburg and the United Arab Emirates University (medical specialist training program) since 2018</li>
							</ul>',
                'content_ar' => '<img src="//uae-embassy.de/public/img/pages/louvre-2.jpg" width="100%">
							<br/>
							<br/>
							<p>تشهد العلاقات الثقافية بين دولة الإمارات وألمانيا نمواً متزايداً في العقود الماضية الثلاثة الماضية. فقد أفتتح المركز الثقافي الألماني <a href="https://www.goethe.de/ins/ae/en/index.html" target="_blank">(معهد غوته)</a> في أبوظبي في العام 2006، ليكون منصة لتعليم اللغة الألمانية في دولة الإمارات ومركزاً للتعريف بالثقافة والهوية والأدب والفن.</p>
<br/>
<p>وهناك العديد من المشروعات الثقافية المشتركة بين البلدين لإبراز الهوية الحضارية لكل منهما، منها التعاون في إدارة معارض الكتاب. وقد أعلنت دائرة السياحة والثقافة - أبوظبي عن اختيار ألمانيا ضيف شرف الدورة الحادية والثلاثين <a href="https://adbookfair.com/en/default.aspx" target="_blank">لمعرض أبوظبي الدولي للكتاب</a>&nbsp;التي سيقام في عام 2021 وذلك بعد توقيع مذكرة تفاهم مع معرض فرانكفورت الدولي للكتاب أكبر المعارض المختصة في صناعة الكتاب والنشر في العالم.</p>
<p>وأطلقت هيئة أبوظبي للثقافة والتراث في عام 2007 مشروعاً رائداً يحمل اسم (<a href="https://kalima.ae/ar/default.aspx">كلمة</a>) يهدف لترجمة المعارف الإنسانية في الأدب والفكر والعلوم من من لغات العالم المختلفة إلى اللغة العربية، وقامت هيئة أبوظبي وجامعة يوهانس غوتمبرغ ماينتس بتوقيع مذكرة تفاهم في عام 2009 لترجمة الكتب من اللغة الألمانية إلى اللغة العربية باعتبار جامعة كلية العلوم الثقافية واللسانيات (<a href="https://fb06.uni-mainz.de/">FTSK</a>) والترجمة أحد أعرق المؤسسات الأكاديمية والبحثية الأوروبية في مجال الترجمة.</p>
<p>وفي مجال المتاحف، وقعت كل من هيئة الشارقة للمتاحف، وهيئة المتاحف برلين الحكومية، ومؤسسة الإرث الثقافي البروسي (Stiftung Preu&szlig;ischer Kulturbesitz)، وكلية العلوم التقنية والاقتصاد ببرلين (HTW)، ومعهد غوته بمنطقة الخليج في العام 2013 إعلان نوايا تم بموجبه إنشاء أكاديمية "سوا" كمبادرة مشتركة للتدريب الطلاب في مجال دراسات المتاحف والمختصين في علم المتاحف من منطقة الشرق الأوسط وألمانيا.</p>
<br/>
<p><strong>المجال التعليمي</strong></p>
<p>هناك العديد من المشروعات التعليمية المشتركة بين المؤسسات التعليمية في دولة الإماات وألمانيا، وهناك مدارس ألمانية في كل من أبوظبي ودبي والشارقة، ويستطيع التلاميذ الحصول على شهادة الثانوية العامة الألمانية الدولية "الأبيتور الدولي" في أبوظبي ودبي منذ عام 2011، كما يمكن الحصول على البكالوريا الدولية في المدرسة الألمانية الدولية بالشارقة منذ العام الدراسي 2014-2015.</p>
<p>علاوة على ذلك، هناك العديد من الاتفاقيات والمشروعات المشتركة بين مؤسسات التعليم العالي الإماراتية والألمانية ومنها:</p>
<br/>
<p>الكلية الألمانية الإماراتية للعلوم اللوجستية بأبوظبي (GUCL) التي قامت على أساس الاتفاق المبرم بين المعهد العالي للتكنولوجيا في أبوظبي (HCT) وجامعة فيلداو الألمانية المعهد العالي في يادي بألمانيا (منذ 2011)</p>
<p>اتفاقية التعاون المشترك بين جامعة زايد في أبوظبي وكلية العلوم التطبيقية في ميونيخ (منذ 2016)</p>
<p>اتفاقية التعاون بين هيئة الصحة بدبي ونقابة الأطباء ببرلين والجمعية الألمانية لطب العظام وجراحة الحوادث ( منذ 2014)</p>
<p>اتفاقية التعاون بين المستشفى الجامعي ببرلين (شاريتيه) ودائرة التعليم والمعرفة في أبوظبي (منذ 2018)</p>
<p>اتفاقية التعاون بين مستشفى (medbo) بمدينة ريغنسبورغ وجامعة الإمارات العربية المتحدة (برنامج التدريب الطبي المتخصص) منذ عام 2018.</p>
<br/>',
                'content' => '<img src="//uae-embassy.de/public/img/pages/louvre-2.jpg" width="100%">
							<br/>
							<br/>
							<p>Die kulturellen Beziehungen zwischen den Vereinigten Arabischen Emiraten und Deutschland reichen bis in die fr&uuml;hen Jahre der VAE zur&uuml;ck und haben seitdem zu einer stetig wachsenden Zahl bilateraler Kooperationsinitiativen gef&uuml;hrt.</p>
                            <br/>
                            <p>Seit Mai 2006 ist das Kulturinstitut der Bundesrepublik Deutschland, das <a href="https://www.goethe.de/ins/ae/de/index.html">Goethe-Institut (GI)</a>, mit einem Institut f&uuml;r die Golfregion in Abu Dhabi vertreten. Auch in Dubai werden zertifizierte Deutschkurse des Goethe-Instituts angeboten.</p>
                            <p>Die Literatur steht in der bilateralen kulturellen Zusammenarbeit an vorderster Front. Die j&auml;hrlich stattfindende <a href="https://adbookfair.com/en/default.aspx">Abu Dhabi International Book Fair (ADIBF)</a> hat eng mit der Frankfurter Buchmesse zusammengearbeitet.</p>
                            <p>Die Abu Dhabi International Book Fair ist die am professionellsten organisierte, vielf&auml;ltigste und am schnellsten wachsende Buchmesse im Nahen Osten und Nordafrika. Abu Dhabi ist der Motor f&uuml;r die berufliche und kommerzielle Entwicklung der regionalen Verlagsbranche und setzt sich finanziell wie auch philosophisch f&uuml;r die langfristige Expansion des weltweiten Buchgesch&auml;fts ein. Im Jahr 2019 hat die <a href="https://tcaabudhabi.ae/en/default.aspx">Beh&ouml;rde f&uuml;r Kultur und Tourismus - Abu Dhabi</a> Deutschland als Ehrengast f&uuml;r die 31. Auflage der Abu Dhabi International Book Fair (ADIBF) 2021 angek&uuml;ndigt.</p>
                            <p>2009 wurde zwischen der Johannes Gutenberg-Universit&auml;t Mainz und der Kalima Sektion Abu Dhabi eine Absichtserkl&auml;rung unterzeichnet, um die &Uuml;bersetzung deutscher Literatur ins Arabische zu f&ouml;rdern. Jedes Jahr werden etwa 20 B&uuml;cher in die arabische Sprache &uuml;bersetzt und im Rahmen der Buchmesse Frankfurt und der Internationalen Buchmesse Abu Dhabi pr&auml;sentiert. Seit Beginn dieses einzigartigen und auf Langfristigkeit ausgelegten Projekts sind mehr als 150 &Uuml;bersetzungen von Romanen entstanden.</p>
                            <p>Im Bereich der Museumskunde haben die <a href="https://www.sharjahmuseums.ae/en-US/Museums/Sharjah-Museum-of-Islamic-Civilization">Sharjah Museums Authority</a>, die Staatlichen Museen Berlin / Stiftung Preu&szlig;ischer Kulturbesitz, die Fachhochschule f&uuml;r Technik und Wirtschaft Berlin (HTW) und das Goethe-Institut Golfregion 2013 erstmals eine umfassende Absichtserkl&auml;rung vorgelegt, die Konzepte zur Ausstellungsentwicklung, Museumsp&auml;dagogik und Initiativen zur beruflichen Entwicklung im Feld Museumswissenschaft beinhaltet. Die <a href="https://www.goethe.de/ins/ae/de/kul/sup/sawa-museum-academy.html">SAWA Museum Academy</a> ist das erste explizit transkulturelle Hilfe-zur-Selbsthilfe-Programm f&uuml;r das heutige und zuk&uuml;nftige Museumspersonal in arabischen L&auml;ndern und in Deutschland. Das Programm widmet sich dem Gedankenaustausch zu kontinent- und kultur&uuml;bergreifenden Museumsstudien, mit dem Ziel, das Lernen voneinander und miteinander zu f&ouml;rdern. Als das Emirat Sharjah 2014 seine Nominierung zur Hauptstadt der islamischen Kultur feierte, festigten das <a href="https://www.sharjahmuseums.ae/en-US/Museums/Sharjah-Museum-of-Islamic-Civilization">Sharjah Museum of Islamic Civilization</a> und das Museum f&uuml;r Islamische Kunst in Berlin diese Zusammenarbeit mit zahlreichen Workshops und Seminaren.</p>
                            <br/>
                            <p>Im M&auml;rz 2019 startete zudem eine Zusammenarbeit im Filmbereich zwischen der staatlichen emiratischen Kulturstiftung <a href="https://admaf.org/">ADMAF</a> und der Filmhochschule Ludwigsburg.</p>
                            <br/>
                            <p><strong>Kooperationen in der Schul-und Berufsbildung </strong></p>
                            <br/>
                            <p>Eine sehr fruchtbare Partnerschaft besteht im Bildungsbereich. Insgesamt gibt es 15 Kooperationsprojekte zwischen deutschen und emiratischen Hochschul- und Bildungseinrichtungen. Anerkannte deutsche Auslandsschulen befinden sich in <a href="https://www.gisad-school.com/">Abu Dhabi</a>, <a href="http://www.dssharjah.org/">Sharjah</a> und <a href="http://germanschool.ae/?lang=de">Dubai</a>. In Abu Dhabi und Dubai k&ouml;nnen Sch&uuml;ler seit 2011 bzw. 2012 das &bdquo;Deutsche Internationale Abitur&ldquo; ablegen, an der Deutschen Internationalen Schule Sharjah wurde im Schuljahr 2014/2015 das gemischtsprachige Internationale Baccalaureat (IB) eingef&uuml;hrt.</p>
                            <br/>
                            <p>In Bezug auf die bilaterale Zusammenarbeit im Bereich der Hochschulbildung sind nachfolgend einige der wichtigsten Vereinbarungen zwischen den Emiraten und den deutschen Universit&auml;ten aufgef&uuml;hrt:</p>
                            
                            <ul>
                            <li>Deutsch-VAE College of Logistics (GUCL), seit 2011 Kooperationsvertrag zwischen den <a href="http://www.hct.ac.ae/en/">Higher Colleges of Technology (HCT)</a> in Abu Dhabi und der TU Wildau sowie der Jade Hochschule.</li>
                            <li>Kooperationsvereinbarung zwischen der <a href="https://www.zu.ac.ae/main/en/">Zayed University</a> (Abu Dhabi) und der Hochschule f&uuml;r angewandte Wissenschaften M&uuml;nchen (2016)</li>
                            <li>Seit 2014 Kooperationsvertrag zwischen der <a href="https://www.dha.gov.ae/en/pages/dhahome.aspx">Gesundheitsbeh&ouml;rde Dubai</a>, der &Auml;rztekammer Berlin und der Deutschen Gesellschaft f&uuml;r Orthop&auml;die und Unfallchirurgie</li>
                            <li>Seit 2018 Kooperationsvereinbarung zwischen der Charit&eacute; und dem <a href="https://www.adek.gov.ae/">Department of Education and Knowledge Abu Dhabi</a> (mediales Fachausbildungsprogramm)</li>
                            <li>Kooperationsvereinbarung zwischen dem medbo Bezirksklinikum Regensburg und der <a href="https://www.uaeu.ac.ae/en/">University of the United Arab Emirates</a> (medizinisches Fachausbildungsprogramm) seit 2018</li>
                            </ul>
						',
                'page_id' => 2,
                'parent_section_id' => '',
            ],
        ];

        foreach($data as $item){
            $section['title_en'] = $item['title_en'];
            $section['title_ar'] = $item['title_ar'];
            $section['title'] = $item['title'];
            $section['content_en'] = $item['content_en'];
            $section['content_ar'] = $item['content_ar'];
            $section['content'] = $item['content'];
            $section['page_id'] = $item['page_id'];
            $section['parent_section_id'] = $item['parent_section_id'];
            $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
            $new = \App\Models\PageSection::create($section);
        }
    }
}
