<?php

use Illuminate\Database\Seeder;

class HomeExternalLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            // 'title','title_ar','title_de','details','details_ar','details_de','link','link_ar','link_de','icon'
            [
                'title' => 'EXPO 2020 UPDATES',
                'title_ar' => null,
                'title_de' => null,
                'details' => '<p>Be the first to get all information on the world’s greatest show, the Expo 2020.</p>',
                'details_ar' => null,
                'details_de' => null,
                'link' => 'https://www.expo2020dubai.com/de',
                'link_ar' => null,
                'link_de' => null,
                'icon' => 'img/home-icon-expo.png'
            ],
            [
                'title' => 'THE LATEST UAE NEWS',
                'title_ar' => null,
                'title_de' => null,
                'details' => '<p>Stay up-to-date with what is happening in the UAE by visiting our official news and communications partner, WAM-The Emirates News Agency.</p>',
                'details_ar' => null,
                'details_de' => null,
                'link' => 'https://www.wam.ae/en',
                'link_ar' => null,
                'link_de' => null,
                'icon' => 'img/home-icon-news.png'
            ],
            [
                'title' => 'CORONAVIRUS (COVID-19) GUIDANCE & SUPPORT',
                'title_ar' => null,
                'title_de' => null,
                'details' => '<p>Find out the latest Coronavirus (COVID-19) updates and developments by visiting our official sources below.</p>',
                'details_ar' => null,
                'details_de' => null,
                'link' => 'https://www.mofaic.gov.ae/en/corona-virus-latest-updates',
                'link_ar' => null,
                'link_de' => null,
                'icon' => 'img/home-icon-alert.png'
            ],
        ];

        foreach($data as $item)
            \App\Models\HomeExternalLink::create($item);
    }
}
