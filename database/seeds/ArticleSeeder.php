<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticleSeeder extends Seeder
{
    use \App\Services\CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','slug','content_en','content_ar','content','publish_date','thumbnail','featured_image'

        $data = [
            [
                'title_en' => 'Ambassador Bio',
                'title_ar' => '',
                'title' => 'Kurzbiografie',
                'content_en' => '
                            <h1>H.E. Hafsa Al Ulama</h1>
							<h3>UAE Ambassador to Germany</h3>
							<br/>
							<p><strong>Message from the Ambassador</strong></p>
							<p>Welcome to the official website of the Embassy of the United Arab Emirates (UAE) in Berlin. I hope that you will be able to find helpful information and resources on our website. I encourage you to take a look at the services we offer and we are available for any questions you might have. You could also get more information about the UAE by following us @UAEinBerlin on our Facebook, Instagram and Twitter pages.</p>
							<p>I look forward to working towards strengthening the relations between our two countries.</p>
							<p>- Hafsa Al Ulama</p>
							<p><strong>About the Ambassador</strong></p>
							<p>Her Excellency Hafsa Al Ulama took up the position of Ambassador of UAE to Germany in February 2020.</p>
							<p>Prior to this position, she was the Ambassador of UAE to the Federative Republic of Brazil. She had also served as the Ambassador of UAE to Montenegro and non-resident Ambassador of UAE to Kosovo. In addition, she acted as the Managing Director of Abu Dhabi Capital Group, as well as held significant positions in the Ministry of Economy and Planning and Citibank UAE, where she became the first UAE woman VP in the UAE.</p>
							<p>H.E. Al Ulama&rsquo;s personal and social achievements include receiving the UAE Pride Medal for her achievements as Ambassador of UAE to Brazil at the Mohammed Bin Rashid Government Excellence Award in 2019, being named the Federal Personality of 2017 by the Emirates Centre for Strategic Studies and Research (ECSSR), and receiving the Order of Merit from the President of Montenegro in 2016.</p>
							<p>H.E. Al Ulama holds a BA in Economics from San Diego State University and an MSc in Analysis, Design and Management of Information Systems from London School of Economics and Political Science.</p>',
                'content_ar' => '
                            <h1>،سعادة حفصة العلماء</h1>
                            <h3>سفيرة دولة الإمارات العربية المتحدة</h3>
                            <br/>
                           <p><strong>كلمة السفيرة</strong></p>
<p>يسرني أن أرحب بكم في الموقع الرسمي لسفارة دولة الإمارات العربية المتحدة لدى جمهورية ألمانيا الاتحادية، وآمل أن يقدم لكم الموقع معلومات مفيدة عن خدمات السفارة وأنشطتها الهادفة لدعم التعاون الوثيق وعلاقات الصداقة التي تجمع دولة الإمارات وألمانيا. كما يسعد السفارة تلقي استفساراتكم ومقترحاتكم عبر خدمة البريد الإلكتروني، ويمكنكم متابعتنا أيضاً على حساباتنا على وسائل التواصل الاجتماعي الخاصة بالسفارة عبر UAEinBerlin@.</p>
<p>حفصة العلماء</p>
<br/>
<p><strong>نبذة عن السفيرة</strong></p>
<p>تولت سعادة حفصة العلماء منصب سفير دولة الامارات العربية المتحدة لدى جمهورية ألمانيا الإتحادية في فبراير 2020. &nbsp;وقد مثلت سعادتها دولة الامارات العربية المتحدة كسفير لدى كل ٍمن جمهورية البرازيل، ومونتينغرو، وكسفير غير مقيم لدى جمهورية كوسوفو. بالإضافة الى ذلك، شغلت سعادتها العديد من المناصب مثل الرئيس التنفيذي لشركة أبوظبي كابيتال جروب، والوكيل المساعد لشؤون التخطيط والإحصاء بوزارة الاقتصاد الإماراتية، ونائب رئيس سيتي بنك الشرق الأوسط.&nbsp;</p>
<p>وعلى صعيد الإنجازات الشخصية فقد منحها صاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة رئيس مجلس الوزراء وزير الدفاع وحاكم دبي ميدالية فخر الإمارات، كما كرمها مركز الدراسات والبحوث الاستراتيجية في أبوظبي بجائزة الشخصية الاتحادية التي يمنحها صاحب السمو الشيخ محمد بن زايد آل نهيان ولي عهد أبوظبي ونائب القائد الأعلى للقوات المسلحة ورئيس المجلس التنفيذي لإمارة أبو ظبي تقديراً لدورها الوطني البارز، كما منحها سعادة رئيس مونتنيغرو وسام الإستحقاق في عام 2016.</p>
<br/>
<p>سعادة حفصة العلماء حاصلة على درجة البكالوريوس في الإقتصاد من جامعة سان دييغو بالولايات المتحدة الأمريكية، وعلى درجة الماجستير في إدارة نظم المعلومات من جامعة لندن للإقتصاد والعلوم السياسية.</p>
							',
                'content' => '
                            <h1>H.E. Hafsa Al Ulama</h1>
							<h3>Botschafterin der Vereinigten Arabischen Emirate</h3>
                            <br/>
							<p><strong>Gru&szlig;wort der Botschafterin</strong></p>
<p>Willkommen auf der offiziellen Website der Botschaft der Vereinigten Arabischen Emirate (VAE) in Berlin. Wir hoffen, dass Sie auf unseren Seiten hilfreiche Informationen und anschauliche Materialien finden werden und m&ouml;chten Sie anregen, einen Blick auf die von uns angebotenen Dienstleistungen zu werfen. Bei Fragen stehen wir Ihnen gerne zur Verf&uuml;gung.</p>
<p>Ich freue mich darauf, auf eine St&auml;rkung der Beziehungen zwischen unseren beiden L&auml;ndern hinzuarbeiten.</p>
<p>Hafsa Al Ulama</p>
<p><strong>&Uuml;ber die Botschafterin </strong></p>
<p>Ihre Exzellenz Hafsa Al Ulama trat im Februar 2020 das Amt der Botschafterin der VAE in Deutschland an.</p>
<p>Vor dieser Position war sie Botschafterin der VAE in der F&ouml;derativen Republik Brasilien. Sie hatte auch als Botschafterin der VAE in Montenegro und als nicht-residierende Botschafterin der VAE f&uuml;r den Kosovo gedient. Dar&uuml;ber hinaus war sie als Gesch&auml;ftsf&uuml;hrerin der Abu Dhabi Capital Group t&auml;tig und hatte wichtige Positionen im Ministerium f&uuml;r Wirtschaft und Planung und bei der Citibank VAE inne, wo sie die erste weibliche Vizepr&auml;sidentin der VAE wurde.</p>
<p>Zu Al Ulamas pers&ouml;nlichen und sozialen Verdiensten z&auml;hlen die Verleihung der UAE Pride Medal f&uuml;r ihre Verdienste als Botschafterin der VAE in Brasilien im Rahmen des Mohammed Bin Rashid Government Excellence Award 2019, die Ernennung zur Bundespers&ouml;nlichkeit des Jahres 2017 durch das Emirates Centre for Strategic Studies and Research (ECSSR) und die Verleihung des Verdienstordens durch den Pr&auml;sidenten von Montenegro 2016.</p>
<p>I.E. Al Ulama hat einen Bachelor-Abschluss in Wirtschaftswissenschaften der San Diego State University und einen MSc-Abschluss in Analyse, Design und Management von Informationssystemen der London School of Economics and Political Science.</p>',
                'publish_date' => '2020-06-11',
                'thumbnail' => 'img/hafsa-full.jpg',
                'featured_image' => 'img/hafsa-full.jpg',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Newsletter',
                'title_ar' => 'Newsletter',
                'title' => 'Newsletter',
                'content_en' => '
							<h1>Stay informed</h1>
							<p>Be a part of our mailing list to receive updates, news and our newsletters.</p>
							<br/>',
                'content_ar' => '
							<h1>ابق على علم</h1>
							<p>	يمكنك تسجيل عنوانك الإلكتروني للحصول على آحدث المستجدات والأخبار والنشرة الدورية.</p>
							<br/>',
                'content' => '
							<h1>Bleiben Sie informiert</h1>
							<p>Bitte registrieren Sie sich hier, um unser Newsletter zu erhalten.</p>
							<br/>',
                'publish_date' => '2020-06-11',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Zaki Anwar Nusseibeh, Director-General of Staatliche Kunstsammlungen Dresden discuss cultural ties.',
                'title_ar' => 'معالي زكي نسيبة ومديرة متحف دريسدن ماريون أكرمان القومي يناقشان العلاقات الثقافية',
                'title' => 'Staatsminister Zaki Anwar Nusseibeh und Generaldirektorin der Staatlichen Kunstsammlungen Dresden erörtern kulturelle Beziehungen',
                'content_en' => '<p>Zaki Anwar Nusseibeh, Minister of State, today held a remote meeting with Dr. Marion Ackermann, Director-General of the Staatliche Kunstsammlungen Dresden in Germany, in the presence of Hafsa Al Ulama, UAE Ambassador to Germany.</p>
							<p>At the start of the meeting, Nusseibeh welcomed Dr. Ackermann and wished her success, stressing the importance of continuing their cultural communication and dialogue despite the coronavirus crisis affecting the world, in light of the role of culture in bringing a positive change to the lives of people.</p>
							<p>Nusseibeh also discussed ways of supporting the dialogue and enhancing the bilateral relations between the two countries, especially in the area of culture, as well as strengthening the relations between the two parties by promoting cultural and scientific exchange, to provide opportunities for cultural and social interactions.</p>
							<p>He praised the cultural relations between the UAE and Germany, which are part of their cultural diplomacy and soft power, and are helping to achieve the goals of their leaderships to enhance cooperation in the areas of culture, politics, diplomacy and the economy.</p>
							<p>Al Ulama pointed out that there are many common cultural factors that bring together the UAE and Germany, which have helped consolidate their relations while praising the existence of many joint cultural initiatives for writers and researchers seeking knowledge, under the framework of strengthening their overall relationship.</p>
						',
                'content_ar' => '<p>عقد معالي زكي أنور نسيبة، وزير الدولة، اليوم اجتماعا عن بعد مع الدكتورة ماريون أكرمان، مديرة متحف دريزدن القومي في ألمانيا، بمشاركة سعادة/ حفصة العلماء، سفيرة الدولة لدى جمهورية ألمانيا الاتحادية.</p>
<p>وقد رحب معاليه في بداية الاجتماع بالدكتورة أكرمان، وتمنى لها مزيداً من النجاح في مهمتها، مؤكداً على أهمية استمرار التواصل والتحاور الثقافي بالرغم من أزمة فيروس كورونا المستجد التي اجتاحت العالم، وذلك نظرا لدور الثقافة الهام في إحداث تغيير إيجابي في حياة الشعوب.&nbsp;</p>
<p>كما تتطرق معالي زكي أنور نسيبة خلال الاجتماع إلى سبل دعم الحوار وتعزيز العلاقات الثنائية بين البلدين، وخاصة في المجال الثقافي، بالإضافة إلى تقوية العلاقات بين الدولة وجمهورية ألمانيا الاتحادية، من خلال تعزيز التبادل الثقافي والعلمي، والذي من شأنه أن يوفر فرص تعاون وتفاعل بين الطرفين في المجال الثقافي والإجتماعي.</p>
<p>وأشاد معاليه بالعلاقات الثقافية بين دولة الإمارات العربية المتحدة وجمهورية ألمانيا الاتحادية، والتي هي جزء من الدبلوماسية الثقافية والقوة الناعمة لكلا البلدين، والتي تساهم في تحقيق أهداف قيادتهما فيما يختص بتعزيز التعاون الثقافي والسياسي والدبلوماسي والإقتصادي بين البلدين.&nbsp;</p>
<p>كما أشارت سعادة/ حفصة العلماء، سفيرة الدولة، إلى وجود قواسم ثقافية مشتركة عديدة تجمع بين دولة الإمارات العربية المتحدة وجمهورية ألمانيا الإتحادية، والتي ساهمت في تعزيز العلاقات بين البلدين، مشيدة بوجود العديد من المبادرات الثقافية المشتركة من قبل الكتاب والأدباء والباحثين عن المعرفة من الطرفين، ضمن إطار تقوية العلاقات بين البلدين بشكل عام.</p>',
                'content' => '
                            <p>Staatsminister Zaki Anwar Nusseibeh traf heute mit der Generaldirektorin der Staatlichen Kunstsammlungen Dresden, Prof. Marion Ackermann, zu einem Ferngespr&auml;ch zusammen. Das virtuelle Treffen fand in Anwesenheit der Botschafterin der VAE in Deutschland, Hafsa Al Ulama, statt.</p>
                            <p>Zu Beginn des Treffens begr&uuml;&szlig;te Nusseibeh Prof. Dr. Ackermann, w&uuml;nschte ihr viel Erfolg f&uuml;r ihre Arbeit und betonte wie wichtig es sei, die beidseitige kulturelle Kommunikation und den Dialog trotz der weltweiten Coronavirus-Krise fortzusetzen. Dies sei angesichts der Rolle der Kultur bei der Herbeif&uuml;hrung einer positiven Ver&auml;nderung im Leben der Menschen entscheidend.</p>
                            <p>Nusseibeh kam auch auf M&ouml;glichkeiten zur Unterst&uuml;tzung des Dialogs und zur Vertiefung der bilateralen Beziehungen zwischen den beiden L&auml;ndern, insbesondere im Bereich der Kultur, zu sprechen und betonte den Willen seines Landes, den kulturellen und wissenschaftlichen Austausch zwischen beiden Seiten zu st&auml;rken. Damit sollten auch M&ouml;glichkeiten f&uuml;r kulturelle und soziale Interaktionen geschaffen werden.</p>
                            <p>Er verwies auf die fruchtbaren gemeinsamen Kulturinitiativen zwischen den Vereinigten Arabischen Emiraten und Deutschland, die Teil ihrer Kulturdiplomatie und Soft Power seien und - <em>wie von ihren Landesf&uuml;hrungen angestrebt</em> - zur Ausweitung der Zusammenarbeit in den Bereichen Kultur, Politik, Diplomatie und Wirtschaft beitragen w&uuml;rden.</p>
                            <p>Al Ulama wies auf die Existenz vieler gemeinsamer kultureller Faktoren hin, die die VAE und Deutschland zusammenf&uuml;hren und die zur Festigung ihrer Beziehungen beigetragen h&auml;tten. Gleichzeitig hob sie das Vorhandensein vieler kultureller Initiativen f&uuml;r informationssuchende Schriftsteller und Forscher positiv hervor.</p>
						',
                'publish_date' => '2020-06-11',
                'thumbnail' => 'img/home-news/zaki.jpg',
                'featured_image' => 'img/home-news/zaki.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => "World unites in 'Prayer for Humanity'",
                'title_ar' => 'العالم يتحد في "صلاة من أجل الإنسانية"',
                'title' => "Die Welt vereint im ‘Gebet für die Menschheit’",
                'content_en' => '<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Tomorrow, 14th May 2020, will witness a global united effort where individuals from all walks of life and belief systems will come together in reflection and prayer.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">The Higher Committee for Human Fraternity, HCHF, initiated the \'Prayer for Humanity\' call, inviting religious leaders and faithful around the world to a day of fasting, prayers and supplications for "the good of all humanity" and "an end to the COVID-19 pandemic".</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Commenting on the initiative, the HCHF Secretary-General Judge Mohamed Abdelsalam said that following the launch of the action on 2nd May 2020, the Committee had received global support from a large number of religious, political leaders and institutions from all over the world. Led by Sheikh Dr. Ahmed el-Tayyeb, Grand Imam of Al Azhar, and His Holiness Pope Francis, Head of the Catholic Church, many state presidents, prime ministers, along with religious leaders, political figures, media personalities, social influencers and prestigious educational institutions have also supported the call to pray for humanity, Abdelsalam told the Emirates News Agency, WAM.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">He extended the Higher Committee\'s appreciation and gratitude to His Highness Sheikh Mohamed bin Zayed Al Nahyan, Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces, for his continued efforts to bring the global community together, and for his support of the \'Prayer for Humanity\' initiative.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Abdelsalam explained that world peace and human fraternity form core pillars of UAE society, that has carried forward the legacy of the late Sheikh Zayed bin Sultan Al Nahyan, who created a nation that has become host to individuals from all walks of life, coexisting peacefully. He went on to note that the UAE helped oversee the creation of the Human Fraternity Document, adding that it is a true testament of the UAE leadership and its people\'s belief in unity, fraternity, and peace for all.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">For his part, Dr. Sultan Al Remeithi, Secretary-General of the Muslim Council of Elders and member of the HCHF, noted the need for a silver lining, a positive recognition during the coronavirus pandemic. The Prayer for Humanity has sparked hope, he said, adding that the call has united prayers and hearts around the world for the common good.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Dr. Al Remeithi noted that the global call for prayer seeks God the Almighty\'s salvation from the COVID-19 pandemic, as well as ridding global communities from the scourge of hate, discrimination and antipathy.</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Yasser Hareb, Emirati Writer and TV Presenter, and HCHF member, stressed that human solidarity is a value that transcends all limits. "On Thursday we will raise our voices to God Almighty in prayer and supplication to protect humanity from this pandemic as we have witnessed how harm afflicting one person, can in turn harm all of humanity."</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">Father Professor Dr. Ioan Sauca, Secretary-General of the World Council of Churches, WCC, and HCHF member, had called on members of the WCC to join in the global day of prayer. He noted, "Due to this pandemic, many of our people suffer from a state of fear, mistrust, shock, isolation, social distancing and have even experienced deaths within their families or their ecclesiastical communities. While we continue to suffer from this global health crisis, our global solidarity through prayer will reflect our innate feeling of responsibility and care towards one another."</span></span></p>
							<p style="text-align: justify; background: white; vertical-align: baseline; margin: 0in 0in 11.25pt 0in;"><span class="ms-rtefontsize-2"><span lang="DE" style="">The Higher Committee of Human Fraternity seeks to bring humanity together in prayer on Thursday, 14th May, in what will be the largest gathering of humanity for one goal. The event will be accompanied by unprecedented media coverage, through the Committee&rsquo;s social media accounts with two hashtags in Arabic and English, #صلاة_من_أجل_الإنسانية and #PrayForHumanity to allow people to interact and share their videos, photos and posts.</span></span></p>
							',
                'content_ar' => '<p>سيشهد يوم غد، ١٤ مايو 2020 مبادرة عالمية موحدة، حيث يتحد الجميع بمختلف إنتماءاته ومعتقداته الدينية من أجل الصلاة والدعاء.</p>
<p>&nbsp;</p>
<p>وقد أطلقت &ldquo;اللجنة العليا للأخوة الإنسانية" مبادرة "صلاة من أجل الإنسانية"، داعية جميع القيادات الدينية وجموع الناس حول العالم بأن يتضرعوا إلى الله بالدعاء والصوم والصلاة من أجل خير البشرية وأن يوفقهم في التغلب على جائحة فيروس كورونا المستجد.</p>
<p>&nbsp;</p>
<p>وتعليقاً على هذه المبادرة، صرح المستشار محمد عبد السلام، الأمين العام للجنة العليا للأخوة الإنسانية، خلال مقابلته مع وكالة أنباء الإمارات (وام)، أن اللجنة تلقت دعماً عالمياً من عدد كبير من الزعماء والمؤسسات الدينية والسياسية هذه المبادرة منذ إطلاقها في ٢ مايو 2020 وذلك تحت قيادة فضيلة الإمام الأكبر شيخ الأزهر الدكتور أحمد الطيب، وقداسة البابا فرنسيس بابا الكنيسة الكاثوليكية.&nbsp; هذا، وقد دعم العديد من رؤساء الدول ورؤساء الوزارات والقيادات الدينية والشخصيات الإعلامية والسياسية، والمؤثرين على قنوات التواصل الاجتماعي والمؤسسات التعليمية المرموقة الدعوة للصلاة من أجل الإنسانية.</p>
<p>&nbsp;</p>
<p>كما أعرب المستشار عبد السلام عن تقدير اللجنة العليا وامتنانها لصاحب السمو الشيخ محمد بن زايد آل نهيان، ولي عهد أبوظبي نائب القائد الأعلى للقوات المسلحة، لجهود سموه المتواصلة لتوحيد المجتمع الدولي ودعمه لمبادرة "الصلاة من أجل الإنسانية".</p>
<p>&nbsp;</p>
<p>وأوضح المستشار محمد عبد السلام أن السلام العالمي والأخوّة الإنسانية يشكلان الركائز الأساسية للمجتمع الإماراتي، التي عززت إرث المغفور له بإذن الله سمو لشيخ زايد بن سلطان آل نهيان "حفظه الله" والذي أسس دولة تستقبل الضيوف من كافة الانتماءات والمعتقدات ويتعايشون على أرضها في سلام. كما أشار إلى أن دولة الإمارات ساهمت في الاشراف على إعداد وثيقة الأخوة الإنسانية، والتي تدل على إيمان قيادة وشعب دولة الإمارات بالوحدة والأخوة والسلام للجميع.</p>
<p>&nbsp;</p>
<p>من جانبه، أشار الدكتور سلطان الرميثي عضو اللجنة العليا للأخوة الإنسانية والأمين العام لمجلس حكماء المسلمين، إلى إنه على الرغم من خطورة تفشي فيروس كورونا المستجد إلا الجائحة أظهرت نقاطا إيجابية لدى الناس، فقد جاءت مبادرة "الصلاة من أجل الإنسانية" لتجدد الأمل، وتوحد الصلوات والقلوب حول العالم من أجل الصالح العام. وأشار الرميثي إلى أن هذه الدعوة العالمية للصلاة يسعى ويبتهل العالم من خلالها إلى الله تعالى لرفع البلاء والوباء وتخليص العالم من الكراهية والتمييز والعداء.</p>
<p>&nbsp;</p>
<p>أكد ياسر حارب، كاتب ومقدم برامج تلفزيونية إماراتي، وعضو اللجنة العليا للأخوة الإنسانية، على أن التضامن الإنساني قيمة بشرية تتجاوز كل الحدود. قائلاً "سنتوجه يوم الخميس سنبتهل إلى الله سبحانه وتعالى ليحمي البشرية من هذا الوباء، فقد رأينا كيف أن الخطر الذي يصيب شخصاً واحدًا قد يؤذي البشرية كلها.</p>
<p>&nbsp;</p>
<p>كما دعا القس الدكتور إيوان سوكا، أمين عام مجلس الكنائس العالمي، عضو اللجنة العليا للأخوة الإنسانية، أعضاء مجلس الكنائس العالمي، للإنظام إلى يوم الصلاة العالمي، مشيراً أنه "بسبب هذا الوباء، يعاني الكثير من الخوف والريبة، والصدمة، والعزلة، والتشتت الاجتماعي، كما انهم تعرضوا لحالات وفيات إما داخل عائلاتهم، أو داخل مجتمعاتهم الكنسية. وبينما نواصل معاناتنا من تلك الأزمة الصحية العالمية، فإن تضامننا العالمي عبر الصلاة، سيعكس شعورنا الفطري بتحمل المسؤولية والإهتمام ببعضنا البعض".</p>
<p>تسعى اللجنة العليا للأخوة الإنسانية إلى توحيد الإنسانية يوم الخميس ١٤ مايو، فيما سيكون أكبر تجمع للبشرية من أجل هدف واحد. ومن المقرر أن يرافق هذا الحدث تغطية إعلامية غير مسبوقة، من خلال مواقع التواصل الاجتماعي الخاصة باللجنة باللغتين العربية والإنجليزية، #صلاة_من_أجل_الإنسانية و #PrayForHumanity ، والتي ستتيح للجميع التفاعل مع الحدث من خلال المشاركة بمقاطع فيديو وصور ومنشورات خاصة بهم.</p>',
                'content' => '<p>Der oberste Ausschuss zur Realisierung der Ziele des Manifests der menschlichen Br&uuml;derlichkeit (Higher Committee of Human Fraternity, HCHF) ruft mit dem &rsquo;Gebet f&uuml;r die Menschheit&lsquo; religi&ouml;se F&uuml;hrungspersonen und alle Menschen auf der Welt zu einem Tag des Fastens, des Gebets und des Bittens f&uuml;r das &bdquo;Wohl der Menschheit&ldquo; und f&uuml;r &bdquo;das Ende der COVID-19-Pandemie&ldquo; auf.</p>
                            <p>Der Generalsekret&auml;r des obersten Ausschusses, Richter Mohamed Abdelsalam, sagte &uuml;ber die Initiative, der Ausschuss habe seit dem Beginn der Aktion Unterst&uuml;tzung von zahlreichen religi&ouml;sen und politischen F&uuml;hrungspersonen sowie Institutionen weltweit erhalten. Unter F&uuml;hrung des Gro&szlig;imams der Al-Azhar, Scheikh Dr. Ahmed el-Tayyeb, Gro&szlig;imam, und Seiner Heiligkeit Papst Franziskus, Oberhaupt der katholischen Kirche, h&auml;tten viele Staatspr&auml;sidenten, Premierminister, aber auch religi&ouml;se F&uuml;hrungspersonen, politische Pers&ouml;nlichkeiten, Medienleute, Multiplikatoren und renommierte Bildungseinrichtungen den Aufruf zum Gebet f&uuml;r die Menschheit unterst&uuml;tzt, so Abdelsalam gegen&uuml;ber der Emirates News Agency, WAM.</p>
                            <p>Er sprach Seiner Hoheit Scheikh Mohamed bin Zayed Al Nahyan, dem Kronprinz von Abu Dhabi und stellvertretenden Oberbefehlshaber der Streitkr&auml;fte der VAE, die Anerkennung und Dankbarkeit des obersten Ausschusses f&uuml;r seine anhaltenden Bem&uuml;hungen um die Zusammenf&uuml;hrung der Weltgemeinschaft und f&uuml;r seine Unterst&uuml;tzung der Initiative "Gebet f&uuml;r die Menschheit" aus.</p>
                            <p>Abdelsalam erl&auml;uterte, dass der Weltfrieden und die menschliche Br&uuml;derlichkeit die Grundpfeiler der Gesellschaft der VAE bildeten, die das Erbe des verstorbenen Scheikhs Zayed bin Sultan Al Nahyan weiterf&uuml;hrten. Zayed h&auml;tte eine Nation geschaffen, die zu einem Gastgeber f&uuml;r Menschen aus allen Gesellschaftsschichten geworden ist, die friedlich miteinander zusammenlebten. Er f&uuml;gte hinzu, dass die VAE bei der Erstellung des Dokuments &uuml;ber menschliche Br&uuml;derlichkeit mitgewirkt h&auml;tten, und dass es den Glauben der F&uuml;hrung des Landes sowie seiner Bev&ouml;lkerung an Einheit, Br&uuml;derlichkeit und Frieden f&uuml;r alle untermauern w&uuml;rde.</p>
                            <p>Der Generalsekret&auml;r des muslimischen &Auml;ltestenrates und Mitglied des HCHF, Dr. Sultan Al Remeithi, verwies seinerseits darauf, wie n&ouml;tig ein positiver Ausblick w&auml;hrend der Coronavirus-Pandemie sei. Das Gebet f&uuml;r die Menschlichkeit habe Hoffnung geweckt, sagte er. Und: Der Aufruf habe die Gebete und Herzen auf der ganzen Welt f&uuml;r das Gemeinwohl vereint.</p>
                            <p>Al Remeithi sagte, bei der Initiative handele es sich um eine Bittstellung an Gott um Beendigung der COVID-19-Pandemie und es werde angestrebt, die globalen Gemeinschaften "von der Gei&szlig;el des Hasses, der Diskriminierung und der Antipathie zu befreien".</p>
                            <p>Yasser Hareb, emiratischer Schriftsteller und Fernsehmoderator und Mitglied des HCHF, betonte, dass zwischenmenschliche Solidarit&auml;t ein Wert sei, der alle Grenzen &uuml;berschreite. "Am Donnerstag werden wir unsere Stimmen in Gebet zu Gott erheben, auf dass Er die Menschheit vor dieser Pandemie sch&uuml;tzen m&ouml;ge, denn wir haben erlebt, wie der Schaden, den ein Mensch erleidet, wiederum der ganzen Menschheit schaden kann.</p>
                            <p>Der Generalsekret&auml;r des &Ouml;kumenischen Rates der Kirchen (World Council of Churches, WCC) und Mitglied des HCHF, Pater Professor Dr. Ioan Sauca, hatte die WCC-Mitglieder aufgerufen, sich am weltweiten Gebetstag zu beteiligen. "Aufgrund dieser Pandemie leiden viele unserer Leute unter einem Zustand der Angst, des Misstrauens, des Schocks, der Isolation, der sozialen Distanzierung und haben sogar Todesf&auml;lle innerhalb ihrer Familien oder ihrer kirchlichen Gemeinschaften erlebt. W&auml;hrend wir weiterhin mit den Folgen dieser globalen Gesundheitskrise hadern, wird unsere weltweite Solidarit&auml;t mithilfe des Gebets das Gef&uuml;hl von Verantwortung und F&uuml;rsorge f&uuml;reinander widerspiegeln".</p>
                            <p>Der oberste Ausschuss der menschlichen Br&uuml;derlichkeit ist bestrebt, am Donnerstag, dem 14. Mai, Menschen weltweit bei dieser gro&szlig;en zielgerichteten Zusammenkunft im Gebet zusammenzubringen. Die Veranstaltung wird von einer umfassenden Medienberichterstattung begleitet. Der Ausschuss wird dazu auf seinen Social Media-Kan&auml;len die beiden Hashtags #صلاة_من_من_أجل_الإنسانية und #PrayForHumanity verwenden, die eine Interaktion zwischen den Usern erm&ouml;glichen sollen und damit diese ihre Videos, Fotos und Beitr&auml;ge teilen k&ouml;nnen.</p>
							',
                'publish_date' => '2020-05-14',
                'thumbnail' => 'img/home-news/world-unite.jpg',
                'featured_image' => 'img/home-news/world-unite-full.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => 'UAE has taken early measures in fight against COVID-19: His Highness Sheikh Mohamed bin Zayed',
                'title_ar' => 'صاحب السمو الشيخ محمد بن زايد: دولة الإمارات اتخذت إجراءاتها الوقائية وتدابيرها مبكرا لمواجهة فيروس كورونا ',
                'title' => 'Scheikh Mohamed bin Zayed: VAE ergriffen frühzeitig Maßnahmen im Kampf gegen COVID-19',
                'content_en' => '<p>His Highness Sheikh Mohamed bin Zayed Al Nahyan, Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces, has expressed strong belief that the world will survive the current challenging circumstances.</p>
							<p>"We will get through the ongoing tough times and survive the myriad challenges we and the entire world are now experiencing. The hard time will pass anyway; we might just as well put that passing time to get stronger than before," His Highness Sheikh Mohamed told a number of Sheikhs, ministers and officials&nbsp;at Qasr Al Bahr Majlis today.</p>
							<p>"The world is going through tough times. We, in the UAE, are lucky, as our conditions are relatively better thanks to many factors, primarily the availability of qualified human cadres capable of efficiently shouldering the responsibility toward confronting the proliferation of the COVID-19. The UAE is faring well, all thanks to the early efforts and measures in place to face this virus."</p>
							<p>"We all have seen the difference between the nations which have adopted early precautionary measures and those which haven\'t. The former are more successful in containing the spread of the virus and addressing its repercussions, even though they\'ve started only a few days or weeks earlier than the latter," he continued.</p>
							<p>"We are racing against time in confronting the besetting challenges.&nbsp;In the UAE, we have adopted rational and early advanced precautionary measures before other countries around us to stand up to the challenge. And therefore, the infection tally in the UAE is less thanks to our early response to contain the virus at its onset."</p>
							<p>"The UAE has benefitted from the experience of other advanced countries, like Singapore, South Korea and China in confronting the virus. And we are still maintaining transparent channels of coordination with&nbsp;them. Until now we are among the most successful nations in the region in addressing this challenge, both in terms of the number of individuals who have been examined and the potential and resources boasted by the country in this regard."</p>
							<p>On the challenges faced by several world countries in relation to food and medical supplies, Sheikh Mohamed reassured all UAE nationals and residents that the nation is able to "provide all required food and medical supplies continuously and&nbsp;infinitely."</p>
							<p>"There are a lot of things the State is doing which we can\'t reveal. However, our officials in charge know pretty well that medications and food supplies are a red line in the UAE. I\'d like to reassure every citizen and resident of the UAE that our country is infinitely able to supply everyone with all the food and medicine they could ever need. We are well prepared to face any challenge that arises. We have started our preparation ahead of Coronavirus, and Thank God, the UAE is now secure and stable. We have an advanced infrastructure and we\'re geared for all challenges whatsoever they are."</p>
							<p>Sheikh Mohamed expressed sincere thanks to the entire medical and paramedical teams provided by the Ministry of Health and Prevention. "I send my sincerest appreciation and gratitude to all the health workers of the UAE. They stand in the front line of our defences, we owe them a great debt, and we will never forget their service."</p>
							<p>"Our traditions are very dear to us, but we must be practical during these times. I ask every Emirati and resident to do what is necessary, and let\'s not allow our traditions to become a source of harm to our families and society.Take care of your mothers and fathers and families," he added.</p>
							',
                'content_ar' => '<p>أعرب صاحب السمو الشيخ محمد بن زايد آل نهيان ولي عهد أبوظبي نائب القائد الأعلى للقوات المسلحة عن تفاؤله بأن الظروف الصعبة التي نشهدها والعالم ستمضي بإذن الله تعالى ونحن أكثر قوة وصلابة رغم التحديات العديدة التي نواجهها.</p>
<p>وقال سموه ــ في كلمة له بمجلس في قصر البحر بحضور عدد من الشيوخ والوزراء والمسؤولين ــ "أريدكم أن تتذكروا كلمة وهي أن الوقت الصعب الذي نعيشه سيمضي بإذن الله لكن يحتاج منا إلى صبر".</p>
<p>وأضاف سموه "أن العالم كله يمر بظروف صعبة ..ودولة الإمارات ولله الحمد محظوظة، فظروفنا أهون بسبب عدة عوامل رئيسة أولها همة الكوادر التي أخذت على عاتقها مسؤولية مواجهة فيروس كورونا المستجد في الصفوف الأمامية منذ بداية الأمر.</p>
<p>وقال سموه "جميعنا رأينا الفرق بين الدول التي اتخذت إجراءاتها الاحترازية مبكرا وبين غيرها من الدول التي تأخرت فيها ..فالدول التي باشرت تدابيرها الوقائية مبكرا ولو بفارق أيام أو أسابيع سبقت نظيراتها التي تأخرت في اتخاذ إجراءاتها في احتواء الفيروس ومواجهة تداعياته".</p>
<p>وأضاف صاحب السمو الشيخ محمد بن زايد آل نهيان "نحن في سباق مع الزمن ورغم التحديات والصعوبات في مواجهة هذا الوباء فإن هذا الوقت الصعب سيمضي بعون الله ..ونحن في دولة الإمارات علينا مسؤولية حماية وطننا وأهلنا والمقيمين على أرضنا ..نحن اتخذنا إجراءات مبكرة عقلانية ومتقدمة وسباقة للعديد من الدول من حولنا للتصدي لهذا الوباء ..لهذا السبب نجد أرقام الإصابات بفضل الله لدينا قليلة لأسباب رئيسة وهي أننا سابقنا الزمن في تقصيه ومواجهته والسيطرة عليه منذ بدايته ..فكان الأثر أقل ضررا أما إذا انتشر فيصعب السيطرة عليه وتتزايد تداعياته ومعدلاته وتتراكم بسرعة".</p>
<p>وأكد سموه "أن دولة الإمارات بادرت واتخذت إجراءات مبكرة وهي مستمرة في اتخاذ مزيد من التدابير بشكل متزايد وسريع لمواجهة تطورات هذا الوباء من خلال الاستفادة من تجارب دول العالم المتقدمة مثل سنغافورة وكوريا الجنوبية والصين في مواجهة الفيروس ..ونحن في تواصل مباشر وشفاف وواضح معهم ..كما أن سرعتنا متزايدة في إجراء الفحوصات الطبية للكشف عن فيروس كورونا للإماراتيين أو المقيمين على أرض الوطن ..نحن خلال هذه الفترة وبفضل الله نعد من أكثر الدول تقدما في المنطقة في مواجهة هذا التحدي سواء من حيث عدد الأشخاص الذين أجريت لهم الفحوصات أو من حيث الإمكانات التي تمتلكها الدولة ولله الحمد".</p>
<p>وأعرب سموه عن سعادته بمستوى الخدمات التي تقدم لكل من يرغب في إجراء الفحوصات من مواطنين ومقيمين على هذه الأرض ..فهم أمانة في أعناقنا وتأمين سلامتهم واجب.</p>
<p>وأرجع سموه الفضل وقدم الشكر للكوادر الطبية ووزارة الصحة من أصغر موظف إلى أكبرهم ..الذين وقفوا خط الدفاع الأول وفي الصفوف الأمامية ..</p>
<p>وقال "نحن مدينون لهم.. عملهم هذا لا يمكن أن ننساه .. فهم يحافظون على أمن وسلامة المجتمع".</p>
<p>كما قال صاحب السمو الشيخ محمد بن زايد آل نهيان "إن التحديات التي شهدتها دول العالم عديدة منها تأمين الفحوصات الطبية والأدوية أو توفير المواد الغذائية ..أريد أن أطمئن كل مواطن ومقيم على هذه الأرض الطيبة أن دولة الإمارات قادرة على تأمين الدواء والغذاء إلى ما لا نهاية".</p>
<p>وأوضح سموه "أن هناك الكثير من الأمور التي تقوم بها الدولة لا نتحدث عنها لكن المسؤولين القائمين على هذا العمل يعلمون جيدا أن الدواء والغذاء خط أحمر في دولة الإمارات يجب تأمينه لأهلنا إلى مالا نهاية بغض النظر عن أي تحديات سواء كورونا أو غيره ..نحن بدأنا استعداداتنا قبل كورونا ..وبفضل الله تعالى الإمارات آمنة ومستقرة ومتطورة طبيا ونحن في خير وأمان والحمد لله ..وجاهزيتنا مستدامة لمواجهة التحديات كافة".</p>
<p>وفي إشارة إلى موضوع تأثير بعض العادات والتقاليد خلال هذه المرحلة ..قال سموه "إن لدينا عاداتنا وتقاليدنا الغالية على نفوسنا ..لكن خلال هذه المرحلة وللضرورة أحكام ..أطلب من كل إماراتي ومقيم أن يتحفظ عليها ولا يجعلها سببا لضررنا ولا فرصة لإيذاء أهلنا وأسرنا ومجتمعنا".</p>
<p>كما أوصى سموه بضرورة العناية والاهتمام بالآباء والأمهات والأهل وكبار السن والمحافظة على صحتهم وسلامتهم وحياتهم خلال هذا الظرف الصعب وتقليل اختلاطهم بالغير وحضور أماكن الازدحام خاصة خلال المناسبات الاجتماعية والأفراح حتى تمر هذه الفترة بسلام وخير وأمان دون ضرر".</p>
<p>وقال صاحب السمو الشيخ محمد بن زايد آل نهيان "إن ما نمر به اليوم هو ذاته ما تمر به العديد من الدول ..لكننا في وضع ولله الحمد أكثر أمنا واستقرارا ..وكل عمل تقوم به دولة الإمارات هو لحماية الدولة وأهلها والمقيمين على أرضهما ..فهذه أمانة في أعناقنا وواجب علينا أن نؤديه ..ولن نترك أي فرصة إلا ونستغلها لحماية الدولة وأهلها".</p>
<p>ودعا سموه في ختام كلمته الجميع إلى التفاؤل والنظرة الإيجابية خلال مواجهة مختلف التحديات التي نشهدها .. وقال "إن هذا ما تعلمناه من الوالد المغفور له الشيخ زايد بن سلطان آل نهيان "طيب الله ثراه" أن نستغل الفرص ونستثمرها ونطوعها للتطوير والتقدم حتى نصبح أكثر قوة وتمكنا بعد اجتياز هذا التحدي".</p>
<p>واستمع سموه إلى إحاطة من معالي عبدالرحمن بن محمد العويس وزير الصحة ووقاية المجتمع وزير الدولة لشؤون المجلس الوطني الاتحادي ومعالي حسين بن إبراهيم الحمادي وزير التربية والتعليم وسعادة عبيد راشد الحصان الشامسي مدير عام الهيئة الوطنية لإدارة الطوارئ والأزمات والكوارث حول الإجراءات والتدابير الوقائية والاحترازية الاستباقية التي اتخذت في مواجهة تحدي انتشار "فيروس كورونا" واحتواء تداعياته.</p>
<p>من جانبه أكد معالي وزيرالتربية استعداد الوزارة لبدء عملية التعليم عن بعد حيث أعدت ودربت المعلمين لانطلاقها خلال الأسبوع المقبل في المدارس الحكومية.. مشيرا إلى فتح المجال للمدارس الخاصة للانضمام إلى منظومة التعليم عن بعد حيث انضمت 148 مدرسة خاصة.</p>
<p>ونوه بدعم عدد من المؤسسات الخيرية والجهات في الدولة نظام التعليم عن بعد.</p>
<p>ووجه صاحب السمو الشيخ محمد بن زايد آل نهيان معالي وزير التربية والتعليم الى الأخذ بالاعتبار المدارس التي لا تتمكن من تطبيق أنظمة وتقنيات التعليم عن بعد لتأهيلها تقنيًا وتقديم الدعم لها لتنضم إلى المدارس الأخرى.</p>
<p>من جانبه أعرب معالي وزير الصحة ووقاية المجتمع عن شكره وتقديره للدعم والتحفيز الذي يقدمه صاحب السمو الشيخ محمد بن زايد آل نهيان للقطاع الصحي في الدولة وكوادره ..وقال "إن أبناءك في قطاع الصحة في الصفوف الأمامية في مواجهة التحديات ويعملون على مدى 24 ساعة".</p>
<p>وأشار إلى الخطط والتدابير الاستباقية والاحتياطية التي اتخذتها الوزارة وجعلت الدولة على أعلى جاهزية ..منوها بأن عدد الفحوصات التي أجريت للكشف عن " فيروس كورونا " تجاوز 127 ألف فحص في الدولة لتكون دولة الإمارات من أعلى معدلات العالم التي تجري هذا العدد من الفحوصات وهي مستمرة في إجرائه ..مؤكدا أن التحضيرات جاريه لتنفيذ خيار الحجر الصحي والتطبب عن بعد إذا اقتضت الضرورة والحاجة.</p>
<p>من جانبه أكد سعادة عبيد راشد الحصان الشامسي العمل على تحقيق الهدف الاستراتيجي الذي وضعه صاحب السمو الشيخ محمد بن زايد آل نهيان لفرق العمل وهو "دولة آمنة وقادرة على الصمود" .. وقال "نحن نعمل ونسير في هذا الاتجاه".</p>
<p>حضر مجلس قصر البحر.. سمو الشيخ نهيان بن زايد آل نهيان رئيس مجلس أمناء مؤسسة زايد بن سلطان آل نهيان للأعمال الخيرية والإنسانية والفريق سمو الشيخ سيف بن زايد آل نهيان نائب رئيس مجلس الوزراء وزير الداخلية وسمو الشيخ منصور بن زايد آل نهيان نائب رئيس مجلس الوزراء وزير شؤون الرئاسة وسمو الشيخ عبدالله بن زايد آل نهيان وزير الخارجية والتعاون الدولي وسمو الشيخ خالد بن زايد آل نهيان رئيس مجلس إدارة مؤسسة زايد العليا لأصحاب الهمم وسمو الشيخ خالد بن محمد بن زايد آل نهيان عضو المجلس التنفيذي لإمارة أبوظبي رئيس مكتب أبوظبي التنفيذي وسمو الشيخ ذياب بن محمد بن زايد آل نهيان رئيس ديوان ولي عهد أبوظبي.</p>',
                'content' => '
							<p>Seine Hoheit Scheikh Mohamed bin Zayed Al Nahyan, Kronprinz von Abu Dhabi und stellvertretender Oberbefehlshaber der Streitkr&auml;fte der VAE, hat seine feste &Uuml;berzeugung zum Ausdruck gebracht, dass die Welt die gegenw&auml;rtigen schwierigen Umst&auml;nde &uuml;berstehen w&uuml;rde.</p>
                            <p>"Wir werden diese anhaltend schwierigen Zeiten durchstehen und die unz&auml;hligen Herausforderungen meistern, die wir und die ganze Welt aktuell erleben. Die harten Zeiten werden ohnehin vor&uuml;bergehen; wir aber k&ouml;nnen sie genauso gut nutzen, um gest&auml;rkt daraus hervorzugehen", sagte Seine Hoheit heute vor Scheikhs, Ministern und Regierungsbeamten im Qasr Al Bahr Majlis.</p>
                            <p>"Die Welt durchlebt harte Zeiten. Wir in den Vereinigten Arabischen Emiraten haben Gl&uuml;ck, denn unsere Bedingungen sind aufgrund vieler Faktoren verh&auml;ltnism&auml;&szlig;ig gut, vor allem dank der Verf&uuml;gbarkeit qualifizierter Fachkr&auml;fte, die in der Lage sind, effizient die Verantwortung f&uuml;r die Eind&auml;mmung der Verbreitung von COVID-19 zu &uuml;bernehmen. Den Vereinigten Arabischen Emiraten geht es gut, und das alles dank der fr&uuml;hzeitigen Bem&uuml;hungen und Ma&szlig;nahmen, die zur Bek&auml;mpfung dieses Virus ergriffen wurden.</p>
                            <p>"Wir alle haben den Unterschied zwischen den L&auml;ndern gesehen, die fr&uuml;hzeitige Vorsichtsma&szlig;nahmen ergriffen haben, und denen, die dies nicht getan haben. Erstere sind erfolgreicher bei der Eind&auml;mmung der Ausbreitung des Virus und der Bew&auml;ltigung seiner Auswirkungen, auch wenn sie nur mit wenigen Tagen oder Wochen Vorsprung als die letzteren begonnen haben", fuhr er fort.</p>
                            <p>"Wir befinden uns in einem Wettlauf gegen die Zeit und stellen uns den Herausforderungen, die vor uns liegen. In den Vereinigten Arabischen Emiraten haben wir vor anderen L&auml;ndern um uns herum vern&uuml;nftige und fr&uuml;hzeitige Vorsorgema&szlig;nahmen getroffen (...) und deshalb ist die Zahl der Coronavirus-Infektionen hier geringer.</p>
                            <p>"Die Vereinigten Arabischen Emirate haben bei der Bek&auml;mpfung des Virus von den Erfahrungen anderer fortschrittlicher L&auml;nder wie Singapur, S&uuml;dkorea und China&nbsp; profitiert. Nach wie vor wir koordinieren wir unsere Ma&szlig;nahmen eng mit ihnen. Bislang geh&ouml;ren wir bei der Bew&auml;ltigung der bestehenden Unberechenbarkeiten zu den erfolgreichsten L&auml;ndern in der Region, sowohl hinsichtlich der Anzahl der getesteten Personen als auch hinsichtlich des Potenzials und der uns zur Verf&uuml;gung stehenden Mittel.</p>
                            <p>Zu den Problemen, vor denen mehrere L&auml;nder der Welt in Bezug auf Lebensmittel und medizinische Versorgung stehen , versicherte der Kronprinz allen B&uuml;rgern und Einwohnern der VAE, dass das Land in der Lage sei, "alle ben&ouml;tigten Lebensmittel und medizinischen G&uuml;ter kontinuierlich und uneingeschr&auml;nkt bereitzustellen".</p>
                            <p>"Es gibt viele Dinge, die der Staat tut, die wir nicht preisgeben k&ouml;nnen. Unsere verantwortlichen Beamten wissen jedoch genau, dass das Recht auf Medikamente und Nahrungsmittellieferungen in den VAE unantastbar ist und garantiert werden muss. Ich m&ouml;chte allen B&uuml;rgern und Einwohnern der VAE versichern, dass unser Land in der Lage ist, alle Menschen unbegrenzt mit s&auml;mtlichen von ihnen ben&ouml;tigten Lebensmitteln und Medikamenten zu versorgen. Wir sind gut darauf vorbereitet, uns jeder Herausforderung zu stellen, mit der wir konfrontiert werden. Wir haben unsere Vorbereitungen vor dem Ausbruch des Coronavirus begonnen und gl&uuml;cklicherweise sind die VAE damit jetzt abgesichert und stabil. Wir verf&uuml;gen &uuml;ber eine fortschrittliche Infrastruktur und sind f&uuml;r alle denkbaren F&auml;lle ger&uuml;stet."</p>
                            <p>Scheikh Mohamed bedankte sich bei allen vom Ministerium f&uuml;r Gesundheit und Pr&auml;vention bereitgestellten medizinischen und paramedizinischen Teams. "Ich m&ouml;chte meine aufrichtige Anerkennung und Dankbarkeit an alle Mitarbeiter des Gesundheitswesens in den VAE &uuml;bermitteln. Sie stehen f&uuml;r unseren Schutz an vorderster Front, wir schulden ihnen viel, und wir werden ihre Dienste nie vergessen.</p>
                            <p>"Unsere Traditionen liegen uns sehr am Herzen, aber wir m&uuml;ssen in diesen Zeiten pragmatisch denken. Ich bitte jeden Emirati und Einwohner, das Notwendige zu tun, und nicht zuzulassen, dass unsere Traditionen zu einem Gesundheitsrisiko f&uuml;r unsere Familien und die Gesellschaft werden. K&uuml;mmert euch um eure M&uuml;tter und V&auml;ter und um eure Familien", f&uuml;gte er hinzu.</p>
							',
                'publish_date' => '2020-03-18',
                'thumbnail' => 'img/home-news/against-corona.jpg',
                'featured_image' => 'img/home-news/against-corona-full.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => 'His Highness Sheikh Mohamed bin Zayed receives phone call from German Chancellor.',
                'title_ar' => 'صاحب السمو الشيخ محمد بن زايد يتلقى اتصالا هاتفيا من المستشارة أنجيلا ميركل',
                'title' => 'Seine Hoheit Scheikh Mohamed bin Zayed erhält Anruf von deutscher Bundeskanzlerin',
                'content_en' => '<p>His Highness Sheikh Mohamed bin Zayed Al Nahyan, Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces, today received a phone call from German Chancellor Angela Merkel. During the call, Sheikh Mohamed and Chancellor Merkel discussed cooperation and friendship relations between the two countries and ways of developing them to achieve mutual interests. Also discussed were issues related to the region, as well as the developments in Libya, in the light of the outcome of \'Berlin Conference\' which Germany hosted recently. Sheikh Mohamed and Merkel exchanged views on the issues of mutual concern. They stressed the mutual keenness to continue cooperation with the international community for lasting security, stability and peace for the people of the region and the world.</p><br/>
							',
                'content_ar' => '<p>تلقى صاحب السمو الشيخ محمد بن زايد آل نهيان ولي عهد أبوظبي نائب القائد الأعلى للقوات المسلحة اتصالا هاتفيا من معالي أنجيلا ميركل مستشارة جمهورية ألمانيا الاتحادية الصديقة.</p>
<p>وبحث سموه وميركل .. تعزيز العلاقات الثنائية وسبل تطويرها بما يخدم المصالح المشتركة للبلدين.</p>
<p>كما جرى خلال الاتصال استعراض القضايا وتطورات الأوضاع في المنطقة وآخر المستجدات في ليبيا الشقيقة على ضوء نتائج " مؤتمر برلين " الذي استضافته ألمانيا مؤخرا.</p>
<p>وتبادلا وجهات النظر حول القضايا ذات الاهتمام المشترك.</p>
<p>وأكد سموه والمستشارة الألمانية اهتمام البلدين وحرصهما المشترك على مواصلة التعاون مع المجتمع الدولي لترسيخ أسس الأمن والاستقرار والسلام لشعوب المنطقة والعالم.</p>',
                'content' => '<p>Seine Hoheit Scheikh Mohamed bin Zayed Al Nahyan, Kronprinz von Abu Dhabi und stellvertretender Oberbefehlshaber der Streitkräfte der Vereinigten Arabischen Emirate, erhielt heute einen Anruf von Bundeskanzlerin Angela Merkel. Während des Telefongesprächs erörterten Al Nahyan und Merkel die Zusammenarbeit und die freundschaftlichen Beziehungen zwischen den beiden Ländern und sprachen über Möglichkeiten, diese im Sinne beiderseitiger Interessen auszubauen.</p><br/>
							',
                'publish_date' => '2020-02-03',
                'thumbnail' => 'img/home-news/hh-sheikh-2.jpg',
                'featured_image' => 'img/home-news/hh-full.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => 'His Highness Sheikh Mohamed bin Zayed and Merkel review bilateral ties, regional development.',
                'title_ar' => 'صاحب السمو الشيخ محمد بن زايد والمستشارة الألمانية يبحثان في برلين علاقات البلدين والتطورات الإقليمية',
                'title' => 'Seine Hoheit Scheikh Mohamed bin Zayed und Merkel besprechen bilaterale Beziehungen und regionale Entwicklung',
                'content_en' => '<p><span class="ms-rtefontface-1"><span lang="DE" style="">His Highness Sheikh Mohamed bin Zayed Al Nahyan, Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces, held a meeting with the German Chancellor, Angela Merkel, during which they discussed bilateral ties and ways of developing them at all levels. The leaders also spoke about the latest developments in the Gulf and the Middle East, especially Libya, besides issues of mutual interest.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">The meeting was attended by H.H. Sheikh Abdullah bin Zayed Al Nahyan, Minister of Foreign Affairs and International Cooperation, who will be leading the UAE delegation to the Berlin Conference to be held tomorrow. Dr Sultan bin Ahmad Al Jaber, Minister of State and Special Envoy of the UAE to the Federal Republic of Germany, was also present.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">At the start of the meeting, Sheikh Mohamed bin Zayed expressed his thanks to the German Chancellor for the warm reception and the efforts being made to strengthen bilateral ties. He wished the friendly German people more progress and development.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">His Highness Sheikh Mohamed said the UAE and Germany were major strategic partners, and their bilateral relations were growing thanks to the desire of the leadership of both countries to push forward ties in political, economic, cultural and other fields.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">Sheikh Mohamed bin Zayed said the security and stability of the Middle East is in the region and world\'s interest, given its global strategic importance. He added that the UAE felt great wisdom needs to be exercised in dealing with developments in this region.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">His Highness said Germany, which enjoyed considerable regional and global influence as well as good relations with most countries, had an important role to play in working for peace and stability in the region. He said both the UAE and Germany played a vital role in trying to achieve regional and international stability.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">Sheikh Mohamed bin Zayed expressed the UAE&rsquo;s appreciation for the positive and constructive role played by Germany in seeking to find a political settlement to the crisis in Libya through the Berlin Conference which will be held tomorrow. He affirmed the UAE&rsquo;s full support for the efforts by Germany and the United Nations envoy to Libya to achieve a ceasefire and reach a comprehensive political settlement which would strengthen Arab security and maintain stability in the Mediterranean.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">"A political and peaceful solution is the best approach for achieving security and stability in the region and fulfilling the aspirations of the Libyan people", His Highness said.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">His Highness said the UAE supported any initiative that would resolve the crisis and help Libya to overcome its suffering by putting an end to interference in its internal affairs, strengthen the pillars of the state and stop the inflow of terrorists into Libya. He added that the UAE has always been on the side of the Libyan people and fully supported their legitimate aspirations for peace, reconciliation, unity and development.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">Sheikh Mohamed bin Zayed said: "The UAE looks forward to Chancellor Angela Merkel leading the Berlin Conference on Libya, with such a large and influential international and regional presence, to provide a strong impetus for achieving its goals of returning to constructive dialogue and achieve peace and security in Libya."</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">The two sides agreed on the importance of building Libyan national institutions and restoring law and order to fight the forces of extremism and terrorism and to counter foreign interference in Libya.</span></span></p>
							<p><span class="ms-rtefontface-1"><span lang="DE" style="">For her part, Chancellor Merkel thanked Sheikh Mohamed bin Zayed for visiting Germany and for his important role in enhancing the UAE-German friendship. Merkel said she was keen to keep consulting with His Highness on matters relating to the Gulf and Middle East, and added that the UAE and Germany worked together for peace and development, promoting tolerance and coexistence, and opposing extremism and terrorism.</span></span></p>
							',
                'content_ar' => '<p>عقد صاحب السمو الشيخ محمد بن زايد آل نهيان ولي عهد أبوظبي نائب القائد الأعلى للقوات المسلحة اجتماعاً مع معالي الدكتورة أنجيلا ميركل مستشارة ألمانيا الاتحادية الصديقة حيث يقوم سموه بزيارة برلين حاليا بدعوة من المستشارة الألمانية.</p>
<p>وتم خلال اللقاء تناول العلاقات الثنائية المتميزة بين البلدين وسبل دعمها وتطويرها على المستويات كافة والتطورات والمستجدات في منطقتي الخليج العربي والشرق الأوسط خاصة الشأن الليبي، إضافة الى القضايا ذات الاهتمام المشترك.</p>
<p>حضر الاجتماع سمو الشيخ عبدالله بن زايد آل نهيان وزير الخارجية والتعاون الدولي - الذي يرأس وفد دولة الإمارات إلى "مؤتمر برلين" الذي يعقد غدا - ومعالي الدكتور سلطان بن أحمد الجابر وزير دولة المبعوث الخاص لدولة الإمارات إلى جمهورية ألمانيا الاتحادية.</p>
<p>وأعرب صاحب السمو الشيخ محمد بن زايد آل نهيان في بداية الاجتماع عن شكره للمستشارة الألمانية على حسن الاستقبال وجهودها في تعزيز العلاقات الثنائية، وتمنى للشعب الألماني الصديق المزيد من التقدم والنماء.</p>
<p>وأكد سموه أن الإمارات وألمانيا شريكان استراتيجيان رئيسييان، وتتسم العلاقات بينهما بالقوة والنمو وذلك بفضل حرص قيادتي البلدين على دفعها إلى الأمام في مختلف المجالات السياسية والاقتصادية والثقافية والاجتماعية وغيرها.</p>
<p>وقال سموه إن أمن واستقرار منطقة الشرق الأوسط يمثل مصلحة إقليمية وعالمية، بالنظر إلى ما تمثله هذه المنطقة من أهمية استراتيجية كبيرة بالنسبة إلى العالم كله، مشدداً على موقف الإمارات الداعي إلى الحكمة في التعامل مع كافة التطورات التي تشهدها المنطقة.</p>
<p>وأشار سموه إلى أن ألمانيا بما تمتلكه من تأثير كبير إقليمياً وعالمياً، وعلاقاتها القوية مع منطقة الشرق الأوسط، لها دور مهم في العمل من أجل استتباب السلام والاستقرار في المنطقة.</p>
<p>وأوضح سموه أن كلاً من الإمارات وألمانيا تلعبان دوراً مهماً في العمل من أجل الاستقرار الإقليمي والدولي.</p>
<p>وعبر سموه عن تقدير دولة الإمارات للدور الإيجابي والبنّاء الذي تقوم به جمهورية ألمانيا الاتحادية الصديقة في السعي لإيجاد تسوية سياسية للأزمة في ليبيا الشقيقة من خلال مؤتمر برلين ..مؤكداً دعم الإمارات الكامل لجهود ألمانيا ومبعوث الأمم المتحدة إلى ليبيا، من أجل تثبيت وقف إطلاق النار والتوصل إلى تسوية سياسية شاملة ومستقرة للأزمة الليبية، تعزز الأمن العربي وتحفظ الاستقرار في منطقة البحر المتوسط.</p>
<p>وأكد سموه أن الحل السياسي والسلمي هو الحل الأمثل لتحقيق الأمن والاستقرار في المنطقة وتحقيق طموحات الشعب الليبي.</p>
<p>كما أكد سموه أن دولة الإمارات مع أي جهد أو تحرك أو مبادرة من شأنها مساعدة الشعب الليبي الشقيق على الخروج من أزمته وتجاوز معاناته، ووقف التدخل في شؤونه الداخلية، وتعزيز أركان الدولة الوطنية ومؤسساتها في مواجهة الميليشيات الإرهابية المسلحة، ووضع حد لتدفق العناصر الإرهابية إلى ليبيا ..مضيفا أن الإمارات كانت على الدوام إلى جانب الشعب الليبي، وطموحاته المشروعة في السلم والوفاق والوحدة والتنمية.</p>
<p>وقال سموه : إن الإمارات تتطلع إلى أن تسهم قيادة المستشارة أنجيلا ميركل لمؤتمر برلين حول ليبيا - في ظل حضور دولي وإقليمي كبير ومؤثر - في تقديم دفعة قوية لهذا المؤتمر نحو تحقيق أهدافه في العودة إلى الحوار البنّاء، لتحقيق السلام والأمن على الساحة الليبية.</p>
<p>واتفق الجانبان على أهمية بناء المؤسسات الوطنية الليبية وإعادة النظام والقانون لمحاربة قوى التطرف والإرهاب والتصدي للتدخلات الأجنبية في ليبيا.</p>
<p>من جانبها شكرت المستشارة الألمانية إنجيلا ميركل، صاحب السمو الشيخ محمد بن زايد على زيارته لألمانيا، ودوره المهم في تعميق أواصر الصداقة الإماراتية - الألمانية خلال السنوات الماضية ..مؤكدة أنها حريصة على التشاور مع سموه بشكل مستمر حول قضايا الخليج العربي والشرق الأوسط وأن الإمارات وألمانيا تشتركان في العمل من أجل السلام والتنمية وتعزيز قيم التسامح والتعايش ومواجهة التطرف والإرهاب في العالم.</p>',
                'content' => '<p>Seine Hoheit Scheich Mohamed bin Zayed Al Nahyan, Kronprinz von Abu Dhabi und stellvertretender Oberbefehlshaber der Streitkr&auml;fte der Vereinigten Arabischen Emirate, traf mit Bundeskanzlerin Angela Merkel zu einem Gespr&auml;ch zusammen, bei dem die bilateralen Beziehungen und M&ouml;glichkeiten zu ihrer Entwicklung auf allen Ebenen er&ouml;rtert wurden. Die beiden Staatenlenker sprachen auch &uuml;ber die j&uuml;ngsten Entwicklungen am Golf und im Nahen Osten, insbesondere in Libyen, sowie &uuml;ber Themen von beiderseitigem Interesse.</p>
                            <p>An dem Treffen nahm auch der Minister f&uuml;r ausw&auml;rtige Angelegenheiten und internationale Zusammenarbeit, S.H. Scheikh Abdullah bin Zayed Al Nahyan, teil, der die emiratische Delegation bei der morgigen Berliner Konferenz anf&uuml;hren wird. Ebenfalls anwesend war der Staatsminister und Sondergesandte der VAE in der Bundesrepublik Deutschland, Dr. Sultan bin Ahmad Al Jaber.</p>
                            <p>Zu Beginn des Treffens bedankte sich der emiratische Gast bei der deutschen Bundeskanzlerin f&uuml;r den herzlichen Empfang sowie f&uuml;r ihre Bem&uuml;hungen um eine St&auml;rkung der bilateralen Beziehungen. Der deutschen Bev&ouml;lkerung w&uuml;nschte er fortw&auml;hrenden Fortschritt und Entwicklung.</p>
                            <p>Seine Hoheit Scheikh Mohamed sagte, die Vereinigten Arabischen Emirate und Deutschland seien wichtige strategische Partner, und dass ihre bilateralen Beziehungen dank des Wunsches der F&uuml;hrung beider L&auml;nder, die Beziehungen in Politik, Wirtschaft, Kultur und anderen Bereichen voranzutreiben, stetig st&auml;rker w&uuml;rden.</p>
                            <p><em>(Scheikh Mohamed bin Zayed)</em> Al Nahyan betonte, die Sicherheit und Stabilit&auml;t des Nahen Ostens l&auml;gen angesichts seiner globalen strategischen Bedeutung im Interesse der Region und der Welt. Er f&uuml;gte hinzu, dass die Vereinigten Arabischen Emirate im Umgang mit den dortigen Entwicklungen Umsicht und Klugheit f&uuml;r unabdingbar hielten.</p>
                            <p>Der Bundesrepublik Deutschland, die erheblichen regionalen und globalen Einfluss sowie gute Beziehungen zu den meisten L&auml;ndern genie&szlig;e, k&auml;me eine wichtige Rolle bei der Arbeit f&uuml;r Frieden und Stabilit&auml;t in der Region zu, f&uuml;hrte der Kronprinz weiter aus. Sowohl die VAE als auch Deutschland spielten eine entscheidende Rolle bei den Bem&uuml;hungen, regionale und internationale Stabilit&auml;t zu sichern.</p>
                            <p><em>(Scheikh Mohamed bin Zayed)</em> Al Nahyan brachte auch die Anerkennung der VAE f&uuml;r die positive und konstruktive Rolle Deutschlands bei der Suche nach einer politischen L&ouml;sung f&uuml;r die Krise in Libyen bei der am Folgetag stattfindenden Berliner Konferenz zum Ausdruck. Er bekr&auml;ftigte die volle Unterst&uuml;tzung der VAE hinsichtlich der Bem&uuml;hungen Deutschlands und des Gesandten der Vereinten Nationen in Libyen um einen Waffenstillstand und eine umfassende politische L&ouml;sung, die die arabische Sicherheit st&auml;rken und die Stabilit&auml;t im Mittelmeerraum aufrechterhalten w&uuml;rde.</p>
                            <p>"Eine politische und friedliche L&ouml;sung ist der beste Ansatz, um Sicherheit und Stabilit&auml;t in der Region zu erreichen und den Erwartungen des libyschen Volkes gerecht zu werden", sagte er.</p>
                            <p>Der Kronprinz verwies darauf, dass die VAE alle Initiativen unterst&uuml;tzten, die die Krise l&ouml;sen und Libyen helfen w&uuml;rden, seine Not zu &uuml;berwinden, indem sie der Einmischung in seine inneren Angelegenheiten ein Ende setzen, die Grundpfeiler des Staates st&auml;rken und den Zustrom von Terroristen nach Libyen stoppen w&uuml;rden. Er f&uuml;gte hinzu, dass die VAE stets die Seite des libyschen Volkes eingenommen h&auml;tten und dessen legitime Bestrebungen nach Frieden, Vers&ouml;hnung, Einheit und Entwicklung uneingeschr&auml;nkt unterst&uuml;tzten.</p>
                            <p>Scheikh Mohamed bin Zayed hob hervor: "Die Vereinigten Arabischen Emirate sind hoch erfreut, dass Bundeskanzlerin Angela Merkel die Berliner Libyen-Konferenz mit einer so gro&szlig;en und einflussreichen internationalen und regionalen Pr&auml;senz leiten wird, um damit einen starken Impuls f&uuml;r die R&uuml;ckkehr zu einem konstruktiven Dialog und der Erreichung von Frieden und Sicherheit in Libyen zu geben".</p>
                            <p>Beide Seiten waren sich einig, dass ein Aufbau nationaler libyscher Institutionen und die Wiederherstellung von Recht und Ordnung wichtig seien, um die Kr&auml;fte des Extremismus und des Terrorismus zu bek&auml;mpfen und der ausl&auml;ndischen Einmischung in Libyen entgegenzuwirken.</p>
                            <p>Merkel ihrerseits dankte Al Nahyan f&uuml;r seinen Besuch in Deutschland und f&uuml;r seine wichtige Rolle bei der St&auml;rkung der deutsch-arabischen Freundschaft. Sie, sie sei an beidseitigen Beratungen &uuml;ber Fragen im Zusammenhang mit der Golfregion und dem Nahen Osten weiterhin sehr interessiert und f&uuml;gte hinzu, dass die VAE und Deutschland gemeinsam f&uuml;r Frieden und Entwicklung, die F&ouml;rderung von Toleranz und friedlicher Koexistenz und f&uuml;r die Bek&auml;mpfung von Extremismus und Terrorismus arbeiteten.</p>
							',
                'publish_date' => '2020-01-19',
                'thumbnail' => 'img/home-news/bilateral.jpg',
                'featured_image' => 'img/home-news/bilateral-full.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => 'NASA congratulates UAE on an inspiring mission of hope',
                'title_ar' => 'ناسا" تهنىء الإمارات بإطلاق "مسبار الامل',
                'title' => 'Die NASA gratuliert den VAE zu einer inspirierenden Mission der Hoffnung',
                'content_en' => '<p>WASHINGTON, 20th July, 2020 (WAM) -- The National Aeronautics and Space Administration, NASA, has congratulated the UAE on the launch of the Emirates Mars Mission from the Tanegashima Space Centre in Japan, describing the mission as culmination of tremendous hard work and dedication.</p>
<p>The United Arab Emirate successfully launched its Mars-bound Hope Probe on Monday, marking the Arab world\'s first interplanetary mission -- and the first of three international missions to the Red Planet this summer.</p>
<p>\'\'On behalf of NASA, I congratulate our friends in the United Arab Emirates on the launch of the Emirates Mars Mission, Hope. Today marks the culmination of tremendous hard work, focus, and dedication, as well as the beginning of the UAE&rsquo;s journey to Mars with the ultimate goal of human habitation of the Red Planet. This mission is aptly named since it&rsquo;s a symbol of inspiration for the UAE, the region, and the world,\'\' said NASA Administrator Jim Bridenstine in his message of congratulations.</p>
<p>"Congrats to the team that worked on @HopeMarsMission. It&rsquo;s truly amazing what @UAESpaceAgency &amp; @MBRSpaceCentre have accomplished in such a short time. Hope is exactly what the world needs and thank you to the UAE &amp; @MHI_Group for inspiring all of us," he tweeted.</p>
<p>\'\'We are in awe of the speed and commitment the UAE, through both the Mohammed bin Rashid Space Centre and the UAE Space Agency, has demonstrated in developing its first interplanetary spacecraft. Moreover, your dedication to advancing the world&rsquo;s understanding of Mars by publicly sharing the science and data produced by Hope represents the values of unity, peace, and transparency, that will be so important as humanity moves ever farther into the solar system.\'\' \'\'We are pleased American universities, including the University of Colorado at Boulder, Arizona State University, and the University of California, Berkeley, were able to assist you in this mission. We are also happy to facilitate NASA&rsquo;s Deep Space Network to communicate with Hope.</p>
<p>\'\'All of us at NASA are excited about the prospects for ambitious future partnerships with the UAE in low-Earth orbit and, via the Artemis programme, on and around the Moon with the ultimate destination of Mars,\'\' he added.</p>
<p>\'\'Congratulations again, and Go Hope!,\'\' NASA Administrator concluded.</p>',
                'content_ar' => '<p dir="RTL">واشنطن في 20 يوليو /وام/ عبر جيمس بريدنستين مدير وكالة الفضاء الأمريكية "ناسا" في تغريدة له، عبر حسابه الرسمي بموقع "تويتر" عن تهانيه لدولة الإمارات العربية المتحدة لنجاح إطلاق "مسبار الامل".</p>
<p dir="RTL">قال بريدنستين : نيابة عن وكالة ناسا أهنئ أصدقائنا في دولة الإمارات العربية المتحدة على إطلاق مهمة الإمارات للمريخ "مسبار الأمل. هذا اليوم يمثل تتويجًا للعمل الجاد والتركيز والتفاني الهائل، بالإضافة إلى بداية رحلة الإمارات إلى المريخ بهدف نهائي هو سكن الإنسان على الكوكب الأحمر.لقد تمت تسمية هذه المهمة بشكل ملائم لأنها رمز إلهام للإمارات والمنطقة والعالم.</p>
<p dir="RTL">عبر عن تقديره للسرعة والالتزام الذي أظهرته دولة الإمارات العربية المتحدة، من خلال مركز محمد بن راشد للفضاء ووكالة الفضاء الإماراتية، في تطوير أول مركبة فضائية بين الكواكب بالاضافة الى تفانيهم في تعزيز فهم العالم للمريخ من خلال مشاركة العلم والبيانات التي ينتجها مسبار الأمل. </p>
<p dir="RTL">وأشار مدير وكالة الفضاء الأمريكية إلى أنهم متحمسون لاحتمال إقامة شراكات مستقبلية طموحة مع دولة الإمارات العربية المتحدة في مدار أرضي منخفض ، وعن طريق برنامج Artemis، على القمر وحوله مع الوجهة النهائية للمريخ.</p>',
                'content' => 'WASHINGTON, 20. Juli 2020 (WAM) -- Die National Aeronautics and Space Administration, NASA, hat den Vereinigten Arabischen Emiraten zum Start der Marsmission Emirates Mars Mission vom Raumfahrtzentrum Tanegashima in Japan gratuliert und die Mission als Höhepunkt enormer harter Arbeit und Hingabe beschrieben. 
Das Vereinigte Arabische Emirat startete am Montag erfolgreich seine Mars-Hoffnungs-Sonde und markierte damit die erste interplanetare Mission der arabischen Welt - und die erste von drei internationalen Missionen zum Roten Planeten in diesem Sommer. 
Im Namen der NASA gratuliere ich unseren Freunden in den Vereinigten Arabischen Emiraten zum Start der Marsmission Emirates Hope. Der heutige Tag markiert den Höhepunkt enormer harter Arbeit, Konzentration und Hingabe sowie den Beginn der Reise der Vereinigten Arabischen Emirate zum Mars mit dem letztendlichen Ziel der Besiedlung des Roten Planeten durch den Menschen. Der Name dieser Mission ist treffend gewählt, da sie ein Symbol der Inspiration für die VAE, die Region und die Welt ist", sagte NASA-Administrator Jim Bridenstine in seiner Glückwunschbotschaft. 
"Herzlichen Glückwunsch an das Team, das an der @HopeMarsMission gearbeitet hat. Es ist wirklich erstaunlich, was @UAESpaceAgency & @MBRSpaceCentre in so kurzer Zeit erreicht haben. Hoffnung ist genau das, was die Welt braucht, und vielen Dank an die VAE & @MHI_Gruppe, die uns alle inspiriert hat", twitterte er. 
Wir bewundern die Geschwindigkeit und das Engagement, das die VAE sowohl durch das Mohammed bin Rashid Space Centre als auch durch die VAE Space Agency bei der Entwicklung ihres ersten interplanetaren Raumfahrzeugs bewiesen haben. Darüber hinaus repräsentiert Ihr Engagement, das Verständnis der Welt für den Mars durch den öffentlichen Austausch der von Hope produzierten Wissenschaft und Daten voranzubringen, die Werte der Einheit, des Friedens und der Transparenz, die so wichtig sein werden, wenn die Menschheit immer weiter in das Sonnensystem vordringt\'\'. \'\'Wir freuen uns, dass amerikanische Universitäten, darunter die University of Colorado at Boulder, die Arizona State University und die University of California, Berkeley, Sie bei dieser Mission unterstützen konnten. Wir freuen uns auch, dem Deep Space Network der NASA die Kommunikation mit Hope zu erleichtern. 
\'\'Wir alle bei der NASA sind begeistert über die Aussichten für ehrgeizige zukünftige Partnerschaften mit den Vereinigten Arabischen Emiraten im erdnahen Orbit und, über das Artemis-Programm, auf und um den Mond mit dem Endziel Mars\'\', fügte er hinzu. 
\'\'Nochmals herzlichen Glückwunsch und Go Hope!\'\', schloss der NASA-Administrator. 

							',
                'publish_date' => '2020-07-20',
                'thumbnail' => 'img/home-news/nasa.jpg',
                'featured_image' => 'img/home-news/nasa-full.jpg',
                'external_link' => '',
                'article_type_id' => 1
            ],
            [
                'title_en' => 'Terms of Use',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Privacy Policy',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
            [
                'title_en' => 'Disclaimer',
                'title_ar' => '',
                'title' => '',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'publish_date' => '',
                'thumbnail' => '',
                'featured_image' => '',
                'external_link' => '',
                'article_type_id' => 2
            ],
        ];

        foreach($data as $item){
            $item['slug'] = $this->generateSlug($item['title_en']);
            \App\Models\Article::create($item);
        }

    }
}
