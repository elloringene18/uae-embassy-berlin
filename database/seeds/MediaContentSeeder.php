<?php

use Illuminate\Database\Seeder;

class MediaContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title_en','title_ar','title','page_id','parent_section_id','content_en','content_ar','content'
        $data = [
            [
                'title_en' => 'News and Press Releases',
                'title_ar' => '	الأخبار',
                'title' => 'Nachrichten',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Economic Bulletin',
                'title_ar' => 'النشرة الاقتصادية',
                'title' => 'Wirtschaftsbulletin',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Photography Permission in UAE',
                'title_ar' => 'تصاريح التصوير ',
                'title' => 'Fotografiegenehmigung',
                'content_en' => '
                <img src="//uae-embassy.de/public/img/pages/permits.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>National Media Council (NMC) Media Services </strong></p>
                            <br/>
                            <p>The National Media Council offers four service categories to media and communications customers</p>
                            <ol>
                            <li>Media Licensing: This includes issuance and renewal of licenses for a wide range of on-shore and off-shore media and communications activities.&nbsp;<em>(Application must be filled out in Arabic)</em></li>
                            <li>Monitoring Media Content: This includes permissions for the circulation and production of wide ranging media contents in print and audio-visual forms.</li>
                            <li>Journalists Accreditation: This includes permissions for foreign journalists and correspondents based in the UAE for specific periods of time.</li>
                            <li>News Services: This includes news material transmitted by the Emirates News Agency, WAM, to subscribing media organizations or individuals in addition to daily bulletins and SMS services, and the NMC presence on the web, via its own site or its Twitter and Facebook feeds.</li>
                            </ol>
                            <p>&nbsp;</p>
                            <p><a href="https://nmc.gov.ae/en-us/Services#" target="_blank">Please click on the following link for all services in the categories listed above</a>.</p>
                        ',
                'content_ar' => '
                <img src="//uae-embassy.de/public/img/pages/permits.jpg" width="100%">
							<br/>
							<br/><p>يقدم <strong>المجلس الوطني للإعلام</strong> أربع خدمات أساسية لعملائهم من مجال الإعلام والاتصال، وهي كالتالي:</p>
<ul>
<li><strong>التراخيص الإعلامية:</strong> تشمل الخدمة إصدار وتجديد تراخيص لمجموعة واسعة من أنشطة الإعلام والاتصال المحلية والدولية (يجب تعبئة الطلب باللغة العربية)</li>
<li><strong>مراقبة المحتوى الإعلامي</strong>: تشمل الخدمة منح تصاريح لإنتاج وتداول كافة المحتويات الإعلامية المطبوعة والمواد الصوتية والبصرية.</li>
<li><strong>اعتماد الصحفيين</strong>: تشمل الخدمة إصدار التصاريح اللازمة للصحفيين والمراسلين الأجانب لممارسة العمل الصحافي في دولة الإمارات العربية المتحدة لفترة زمنية محددة.</li>
<li><strong>خدمات إخبارية:</strong> تشمل الخدمة توفير المواد الإخبارية التي تبثها وكالة أنباء الإمارات (وام) للمؤسسات الإعلامية والأفراد المشتركين في هذه الخدمة، والنشرات اليومية والرسائل القصيرة، بالإضافة إلى تواجد المجلس الوطني للإعلام عبر صفحته الالكترونية، وحساباته الرسمية على تويتر وفيس بوك.</li>
</ul>
<p>&nbsp;</p>
<p><strong>يرجى الضغط هنا للحصول على جميع الخدمات الواردة أعلاه.</strong></p>
<p><strong>التصاريح الخاصة بالتصوير الأرضي والجوي داخل أي منطقة في دولة الإمارات العربية المتحدة </strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong><u>المتقدمون للحصول على هذه الخدمات</u></strong></p>
<p>يتم توفير الخدمات لرجال الأعمال المستقلين (G2C)، وقطاع الأعمال (G2B)، والجهات الحكومية (G2G).</p>
<p>&nbsp;</p>
<p><strong><u>الإجراءات:</u></strong></p>
<p>للحصول على تصريح، على مقدمي الطلبات القيام بما يلي:</p>
<ul>
<li>تعبئة الطلب الالكتروني</li>
<li>دفع رسوم الخدمة</li>
</ul>
<p>&nbsp;</p>
<p><strong><u>المستندات المطلوبة:</u></strong></p>
<ul>
<li>صورة عن جوزات السفر والإقامة الخاصة بجميع أفراد طاقم التصوير، وصورة عن خلاصة القيد العائلي إذا كان أحد طاقم التصوير من مواطني الدولة.</li>
<li>في حال وجود نص، يجب الحصول على تصريح خاص للنص من قبل إدارة المحتوى الإعلامي وإرفاقه مع الطلب</li>
</ul>
<p>&nbsp;</p>
<p><strong><u>رسوم الخدمة:</u></strong></p>
<ul>
<li>التصريح بتصوير أرضي &ndash; 50 درهم إماراتي</li>
</ul>
<p>&nbsp;</p>
<p>يمكنكم الحصول على نموذج الطلب الخاص بتصاريح التصوير والمعدات عبر هذا الرابط الالكتروني.</p>',
                'content' => '
                <img src="//uae-embassy.de/public/img/pages/permits.jpg" width="100%">
							<br/>
							<br/><p><strong>National Media Council (NMC) Pressedienstleistungen</strong></p>
<br/>
<p>Der National Media Council bietet Medienvertretern und Kommunikationsdienstleistern vier Servicekategorien</p>
<ul>
<li>Medienlizenzierung: Hierzu geh&ouml;ren die Erteilung und Erneuerung von Lizenzen f&uuml;r ein breites Spektrum von On- und Offshore-Medien- und Kommunikationsaktivit&auml;ten. (Der Antrag muss in arabischer Sprache ausgef&uuml;llt werden)</li>
</ul>
<ul>
<li>Monitoring von Medieninhalten: Dazu geh&ouml;ren Genehmigungen f&uuml;r die Verbreitung und Produktion verschiedenster Medieninhalte in gedruckter und audiovisueller Form.</li>
</ul>
<ul>
<li>Journalisten-Akkreditierung: Dazu geh&ouml;ren Genehmigungen f&uuml;r ausl&auml;ndische Journalisten und Korrespondenten mit Sitz in den VAE f&uuml;r bestimmte Zeitr&auml;ume.</li>
</ul>
<ul>
<li>Nachrichtendienste: Dazu geh&ouml;rt Nachrichtenmaterial, das von der Emirates News Agency, WAM, an Medienorganisationen oder Einzelpersonen mit Abonnement &uuml;bermittelt wird, zus&auml;tzlich zu den t&auml;glichen Bekanntmachungen und SMS-Diensten sowie den NMC-Dienstleistungen im Internet &uuml;ber deren Website oder ihre Twitter- und Facebook-Feeds.</li>
</ul>
<br/>
<p>Bitte klicken Sie auf den folgenden Link f&uuml;r s&auml;mtliche Dienstleistungen in den oben beschriebenen Kategorien (in englischer Sprache)</p>
<p><a href="https://nmc.gov.ae/en-us/Services">https://nmc.gov.ae/en-us/Services#</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<br/>
<p><strong>Genehmigungen f&uuml;r Boden &ndash;und Luftbildaufnahmen / Drehgenehmigungen auf dem Territorium der VAE</strong></p>
<p><u>M&ouml;gliche Antragsteller:</u></p>
<p>Genehmigungen werden an Einzelpersonen (G2C), Unternehmen (G2B) und Regierungsbeh&ouml;rden (G2G) erteilt.</p>
<p><u>Antragsverfahren:</u></p>
<p>F&uuml;r die Ausstellung einer Genehmigung m&uuml;ssen</p>
<ul>
<li>der Online-Antrag ausgef&uuml;llt und</li>
<li>eine Servicegeb&uuml;hr entrichtet werden</li>
</ul>
<p><u>Ben&ouml;tigte Dokumente:</u></p>
<ul>
<li>Reisepass und Visum (individuell f&uuml;r jedes Mitglied der Filmcrew)</li>
<li>Sollten Texte vorliegen, muss eine vom Media Content Department ausgestellte Textgenehmigung mit dem Antrag eingereicht werden</li>
</ul>
<p><u>Geb&uuml;hren: </u></p>
<ul>
<li>Genehmigung f&uuml;r die Fotografie am Boden: AED 500</li>
</ul>
<br/>
<p>Zum Antrag f&uuml;r die Drehgenehmigungen gelangen Sie &uuml;ber den folgenden Link (in englischer Sprache, Anmeldung erforderlich)</p>
<p><a href="https://nmc.gov.ae/en-us/Services/Pages/Filmphotography-Permit.aspx">https://nmc.gov.ae/en-us/Services/Pages/Filmphotography-Permit.aspx</a></p>
',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Traveller’s Guide',
                'title_ar' => 'دليل المسافر',
                'title' => 'Leitfaden für Besucher',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 3,
                'parent_section_id' => '',
                'children' => [
                    [
                        'title_en' => "Traveller's Guide UAE",
                        'title_ar' => 'دليل المسافر إلى الإمارات',
                        'title' => 'Leitfaden VAE',
                        'content_en' => '<img src="//uae-embassy.de/public/img/pages/dubai.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>About the UAE</strong></p>
							<p>The United Arab Emirates is one of the world\'s fastest growing tourist and business destinations. Traditional Arab hospitality and comfortable winter temperatures are complemented by sophisticated infrastructure and world-class amenities.</p>
							<p>Its unique biodiversity is characterized by the beauty of deserts, oases, mountains, valleys and plains, mangroves, beaches, and diversity of flora, wildlife and marine life.</p>
							<p>Additionally, the UAE embraces 200 nationalities from different religious and cultural backgrounds, which live on its territory in harmony and peace.</p>
							<p>The UAE also has become a world-class venue for conferences, regional and international exhibitions and major global sports events such as the Dubai World Cup for horse-racing, the Abu Dhabi Formula One Grand Prix, the Dubai Desert Classic Golf Tournament, the FIFA Club World Cup, world class film festivals in Dubai as well as Abu Dhabi, and in conjunction with the White House, the Global Entrepreneurship Summit. <a target="blank" href="https://www.expo2020dubai.com">The UAE has won the bid to host the 2020 World Expo</a>.</p>
							<p>The travel book publisher <em>Lonely Planet </em>selected Dubai as one of the world\'s top 10 travel destinations in 2020. Abu Dhabi and Ras Al Khaimah won the awards for the Middle East&rsquo;s Leading Business Travel Destination and Middle East&rsquo;s Leading Adventure Tourism Destination, respectively, in the 2019 World Travel Awards.</p>
							<p>There is much to do in the UAE beyond Dubai and Abu Dhabi. First settled during the Bronze Age, Sharjah is the cultural capital of the emirates. The Heritage Area of Sharjah City includes a Maritime Museum, an Islamic Museum and museums for traditional and contemporary Arabic art, among many others.</p>
							<p>Ajman attracts international visitors with beautiful beaches, as does Fujairah which also offers snorkeling and diving and excursions to the Musandam Peninsula, renowned for the unspoiled nature of its sheer cliffs, rocky coves and coral reefs.</p>
							<p>Ras Al Khaima, on the border with Oman, is probably best known among local adventure travelers for the rugged beauty of the Hajjar Mountains.</p>
							<br/>
							<p><u>Important UAE contact details:</u></p>
							<p><u>Emergency services</u></p>
							<p>Police emergency 999</p>
							<p>Police non-emergency 901</p>
							<p>Ambulance 998 or 999</p>
							<p>Fire brigade 997 or 999</p>
							<br/>
							<p>Tourism:</p>
							<p><a target="_blank" href="https://visitabudhabi.ae/uk-en/default.aspx"><strong>Visit Abu Dhabi</strong></a></p>
							<p>Official visitor website for Abu Dhabi travel and tourism, offering information on hotels, restaurants, things to do, culture &amp; heritage, events, museums and more.</p>
							
							<p><a target="_blank" href="https://www.visitdubai.com/en"><strong>Visit Dubai</strong></a></p>
							<p>Dubai tourism&rsquo;s official website provides visitors with useful tips for sightseeing, information on events, hotels and travel deals, city tours, and much more.</p>
							
							<p><a target="_blank" href="https://www.visitsharjah.com/"><strong>Visit Sharjah</strong></a></p>
							<p>Sharjah is the third largest emirate in the UAE, its largest city and its most family-friendly destination. Experience a Sharjah museum, a desert safari and a Sharjah beach &mdash; the emirate offers things to do for everyone.</p>
							
							<p><a target="_blank" href="https://visitrasalkhaimah.com/"><strong>Ras Al Khaimah</strong></a></p>
							<p>An oasis of nature and tranquility, this magical emirate offers a stunning array of landscapes. It has 64km of mesmerizing coastline &ndash; from white sandy beaches to lush green mangroves, creeks and lagoons.</p>
							
							<p><a target="_blank" href="https://ajman.travel/"><strong>Ajman</strong></a></p>
							<p>As a tourist destination, the emirate Ajman offers its visitors a sunny vacation and an enriching historical experience.</p>
							
							<p><a target="_blank" href="https://www.szgmc.gov.ae/en/"><strong>Sheikh Zayed Grand Mosque Center</strong></a></p>
							<p>The Sheikh Zayed Grand Mosque stands out as one of the world&rsquo;s largest mosques, and the only one that captures unique interactions between Islam and world cultures.</p>
							
							<p><a target="_blank" href="https://www.louvreabudhabi.ae/"><strong>Louvre Abu Dhabi</strong></a></p>
							<p>Explore the museum galleries, what\'s on, exhibitions and events to prepare your visit.</p>
							
							<p><a target="_blank" href="http://www.sharjahmuseums.ae/"><strong>Sharjah Museums</strong></a></p>
							<p>The Sharjah Museums Authority ــ SMA was established in 2006 as an autonomous government authority. SMA links and oversees 16 museums across Sharjah that cover widely varied fields, including Islamic art and culture, archaeology, heritage, science, marine life, as well as the history of Sharjah and the region.</p>
							
							<br/>
							<p><strong>Airports</strong></p>
							<p>Find arrival and departure information, news, and travel information for the main airports in the UAE:</p>
							<p><a target="_blank" href="https://www.abudhabiairport.ae/en"><u>Abu Dhabi International Airport (AUH)</u></a></p>
							
							<p><a target="_blank" href="https://www.dubaiairports.ae/"><u>Dubai Airports</u></a></p>
							
							<p><a target="_blank" href="https://www.fujairah-airport.ae/"><u>Fujairah International Airport (FIA)</u></a></p>
							
							<p><a target="_blank" href="http://www.rakairport.com/"><u>Ras Al Khaimah International Airport (RAK)</u></a></p>
							
							<p><a target="_blank" href="https://www.sharjahairport.ae/en/"><u>Sharjah Airport</u></a></p>
							
							<br/>
							<p>Other useful information:</p>
							<p><strong>News</strong></p>
							<p>English language UAE media outlets.</p>
							<p><a target="_blank" href="https://www.thenational.ae/international">The National UAE</a></p>
							<p><a target="_blank" href="https://www.khaleejtimes.com/">Khaleej Times</a></p>
							<p><a target="_blank" href="https://gulfnews.com/">Gulf News</a></p>
							',
                        'content_ar' => '<img src="//uae-embassy.de/public/img/pages/dubai.jpg" width="100%">
							<br/>
							<br/><p><strong>عن دولة الإمارات العربية المتحدة</strong></p>
<p>تعد دولة الإمارات العربية المتحدة من أسرع الوجهات السياحية والتجارية تطوراً في العالم. وتتميز الدولة بتقاليد الضيافة العربية ودرجات الحرارة المعتدلة في فصل الشتاء، بالإضافة إلى بنية تحتية متطورة وخدمات عالمية المستوى.</p>
<p>كما تنفرد دولة الإمارات العربية المتحدة بتنوعها الجغرافي والذي يتميز بجمال الصحراء، الواحات، الجبال، الوديان، السهول، أشجار المانغروف، الشواطئ، النباتات والحيوانات البرية والبحرية.</p>
<p>بالإضافة إلى ذلك، تحتضن دولة الإمارات العربية المتحدة ٢٠٠ جنسية من مختلف الخلفيات الدينية والثقافية، يعيشون على أرضها بتناغم وسلام.</p>
<p>وقد أصبحت دولة الإمارات العربية المتحدة المكان الأمثل عالمياً لعقد المؤتمرات والمعارض الإقليمية والعالمية والفعاليات الرياضية العالمية الهامة، مثل كأس دبي العالمي لسباق الخيل، سباق جائزة أبو ظبي الكبرى - فورمولا ١، مباريات الغولف في صحراء دبي، وكأس العالم لكرة القدم فيفا، بالإضافة إلى المهرجانات السينمائية العالمية في دبي وأبو ظبي، وقمة ريادة الأعمال العالمية بالتعاون مع البيت الأبيض. وقد فازت دولة الإمارات باستضافة معرض إكسبو الدولي ٢٠٢٠.</p>
<p>كما قام ناشر كتاب دليل السفر &nbsp;Lonely Planetبإدراج دبي ضمن أفضل عشر وجهات سفر سياحية في العالم لعام ٢٠٢٠. وفازت كلاً من أبو ظبي ورأس الخيمة على جائزة أفضل وجهة سفر للأعمال والوجهة السياحية الرائدة في الشرق الأوسط على التوالي ضمن جوائز السفر العالمية لعام ٢٠١٩.</p>
<br/>
<p>وهناك الكثير للقيام به في دولة الإمارات العربية المتحدة خارج نطاق دبي وأبو ظبي. حيث تعد الشارقة التي تعود إلى العصر البرونزي، العاصمة الثقافية لدولة الإمارات العربية المتحدة. وتضم المنطقة التراثية في الشارقة متحف الشارقة البحري، ومتحف الحضارة الإسلامية، ومتحف الفنون العربية التقليدية والمعاصرة وغيرها من المزارات التراثية.</p>
<br/>
<p>وتجذب عجمان الزوار العالميين بشواطئها الجميلة، وتوفر الفجيرة فرصاً مميزة لزوارها لممارسة رياضات الغطس والغوص والرحلات إلى شبه جزيرة مسندم المعروفة بطبيعتها البكر التي تتميز بالمنحدرات الحادة، والخلجان الصخرية، والشعاب المرجانية.</p>
<p>وتشتهر رأس الخيمة التي تقع على حدود عُمان، بجمال ووعورة جبال الحَجَر المتواجدة بها والتي هي منطقة جذب لسياح المغامرة المحليين.</p>
<br/>
<p><strong><u>أرقام الاتصال الهامة في دولة الإمارات العربية المتحدة:</u></strong></p>
<p><strong><u>خدمات الطوارئ</u></strong></p>
<p>الشرطة &ndash; الحالات الطارئة ٩٩٩</p>
<p>الشرطة &ndash; الحالات غير الطارئة ٩٠١</p>
<p>الإسعاف ٩٩٨ أو ٩٩٩</p>
<p>المطافئ ٩٩٧ أو ٩٩٩</p>
<br/>
<p><strong>السياحة:</strong></p>
<p><strong>أبو ظبي</strong></p>
<p>الموقع الإلكتروني الرسمي لهيئة أبوظبي للسفر والسياحة، والذي يوفر معلومات عن الفنادق، والمطاعم، والأنشطة التي يمكن ممارستها، والثقافة والتراث، والفعاليات، والمتاحف، وغيرها.</p>
<p><strong>دبي</strong></p>
<p>الموقع الإلكتروني الرسمي للسياحة في دبي والذي يوفر نصائح مفيدة للزوار حول استكشاف معالم المدينة، والمعلومات عن الفعاليات، والفنادق، والعروض الخاصة للسفر، والجولات السياحية في المدينة، وغيرها.</p>
<p><strong>الشارقة</strong></p>
<p>تُعد الشارقة ثالث أكبر إمارة في دولة الإمارات العربية المتحدة، وأكبر مدينة ومن الوجهات المفضلة للعائلات. استمتع بزيارة إلى متحف الشارقة، ورحلة سفاري في الصحراء، وشاطئ الشارقة. تتيح هذه الإمارة أنشطة ممتعة للجميع.</p>
<p><strong>رأس الخيمة</strong></p>
<p>واحة الطبيعة والهدوء &ndash; تحتوي هذه الإمارة السحارة على مجموعة مدهشة من المناظر الطبيعية. تمتد سواحلها الخلابة على ٦٤ كلم من الشواطئ الرملية البيضاء وأشجار المانغروف الخضراء والجداول والبحيرات.</p>
<p><strong>عجمان</strong></p>
<p>تقدم إمارة عجمان كوجهة سياحية لزوارها، عطلة مشمسة وتجربة تاريخية غنية.</p>
<p><strong>مسجد الشيخ زايد الكبير</strong></p>
<p>يعد مسجد الشيخ زايد الكبير أحد أكبر المساجد في العالم، وينفرد بخلق مزيج بين التصاميم الإسلامية والحديثة.</p>
<p><strong>متحف اللوفر أبو ظبي</strong></p>
<p>استكشف صالات المتحف والمعارض والفعاليات للتحضير لزيارتك.</p>
<br/>
<p><strong>متاحف الشارقة</strong></p>
<p>تم تأسيس هيئة الشارقة للمتاحف في العام ٢٠٠٦ كدائرة حكومية مستقلة، تتولى الإشراف على والتنسيق ما بين ١٦ متحف في الشارقة والتي تغطي مجموعة من المجالات المتنوعة، بما في ذلك الفنون والثقافة الإسلامية، وعلم الآثار، والتراث، والعلوم، والحياة البحرية، وتاريخ الشارقة والمنطقة.</p>
<br/>
<p><strong>المطارات</strong></p>
<p>اطلع على معلومات رحلات الوصول والمغادرة، والأخبار، ومعلومات السفر الخاصة بالمطارات الرئيسية في دولة الإمارات العربية المتحدة.</p>
<p><a target="_blank" href="https://www.abudhabiairport.ae/en"><u>مطار أبو ظبي الدولي</u></a></p>
<p><a target="_blank" href="https://www.dubaiairports.ae/"><u>مطارات دبي</u></a></p>
<p><a target="_blank" href="https://www.fujairah-airport.ae/"><u>مطار الفجيرة الدولي</u></a></p>
<p><a target="_blank" href="http://www.rakairport.com/"><u>مطار رأس الخيمة الدولي</u></a></p>
<p><a target="_blank" href="https://www.sharjahairport.ae/en/"><u>مطار الشارقة الدولي</u></a></p>
<br/>
<p>معلومات أخرى مفيدة:</p>
<p><strong>الأخبار</strong></p>
<p>وسائل الإعلام الإماراتية باللغة الإنجليزية:</p>
                            <p><a href="https://www.thenational.ae/international" target="_blank"><strong>ذا ناشونال</strong></a>&nbsp;</p>
                            <p><a href="https://www.khaleejtimes.com/" target="_blank"><strong>خليج تايمز</strong></a></p>
                            <p><a href="https://gulfnews.com/" target="_blank"><strong>جولف نيوز</strong></a></p>
',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/dubai.jpg" width="100%">
							<br/>
                            <br/><p><strong>Informationen f&uuml;r Reisende</strong></p>
                            <p><strong>&Uuml;ber die VAE</strong></p>
                            <br/>
                            <p>Die Vereinigten Arabischen Emirate sind eines der am schnellsten wachsenden Tourismus- und Gesch&auml;ftsziele der Welt. Traditionelle arabische Gastfreundschaft und angenehme Wintertemperaturen werden durch eine hochentwickelte Infrastruktur und Annehmlichkeiten von Weltklasse erg&auml;nzt.</p>
                            <br/>
                            <p>Ihre einzigartige Artenvielfalt zeichnet sich durch die Sch&ouml;nheit der W&uuml;sten, Oasen, Berge, T&auml;ler und Ebenen, Mangroven, Str&auml;nde und die Vielfalt der Flora, Fauna und des Meereslebens aus.</p>
                            <br/>
                            <p>Dar&uuml;ber hinaus leben auf dem Territorium der VAE 200 Nationalit&auml;ten mit unterschiedlichen religi&ouml;sen und kulturellen Hintergr&uuml;nden friedlich und harmonisch miteinander zusammen.</p>
                            <br/>
                            <p>Die Vereinigten Arabischen Emirate haben sich auch zu einem erstklassigen Veranstaltungsort f&uuml;r Konferenzen, regionale und internationale Ausstellungen und globale Sportgro&szlig;veranstaltungen wie die Pferderennweltmeisterschaft in Dubai, den Formel-1-Grand Prix von Abu Dhabi, das Dubai Desert Classic Golf Tournament, die FIFA Klub-Weltmeisterschaft, Weltklasse-Filmfestivals in Dubai und Abu Dhabi sowie in Zusammenarbeit mit dem Wei&szlig;en Haus zum Austragungsort f&uuml;r den Global Entrepreneurship Summit entwickelt. Die VAE haben zudem die Bewerbung um die Ausrichtung der Weltausstellung Expo 2020 gewonnen (Hyperlink).</p>
                            <br/>
                            <p>Der Reisebuchverlag Lonely Planet w&auml;hlte Dubai zu einem der 10 besten Reiseziele der Welt im Jahr 2020. Abu Dhabi und Ras Al Khaimah gewannen bei den World Travel Awards 2019 die Auszeichnungen als f&uuml;hrendes Gesch&auml;ftsreiseziel im Nahen Osten bzw. als f&uuml;hrendes Reiseziel f&uuml;r Abenteuertourismus im Nahen Osten.</p>
                            <br/>
                            <p>Das Land hat jedoch &uuml;ber die international popul&auml;ren Reiseziele Dubai und Abu Dhabi hinaus viel zu bieten. Sharjah, das erstmals in der Bronzezeit besiedelt wurde, ist die kulturelle Hauptstadt der Emirate. Die Heritage Area von Sharjah City umfasst unter anderem ein Schifffahrtsmuseum, ein Islamisches Museum und Museen f&uuml;r traditionelle und zeitgen&ouml;ssische arabische Kunst.</p>
                            <p>Ajman zieht internationale Besucher mit wundersch&ouml;nen Str&auml;nden an, ebenso wie Fujairah, das zu Aktivit&auml;ten wie Schnorcheln und Tauchen sowie Ausfl&uuml;gen zur Musandam-Halbinsel einl&auml;dt, die f&uuml;r die unber&uuml;hrte Natur ihrer steilen Klippen, Felsbuchten und Korallenriffe bekannt ist.</p>
                            <br/>
                            <p>Ras Al Khaima, an der Grenze zu Oman, ist unter den lokalen Abenteuerreisenden wohl am bekanntesten f&uuml;r die raue Sch&ouml;nheit der Hajjar-Gebirge.</p>
                            <br/>
                            <p>Die Einwohner von Umm Al Quwain, dem zweitkleinsten der sieben Emirate, leben haupts&auml;chlich von Landwirtschaft und Fischerei. Auf den zehn kleinen Inseln in seiner Bucht wachsen Mangroven und haben seltene V&ouml;gel, Riesenschildkr&ouml;ten und Seek&uuml;he ihr Zuhause.</p>
                            <br/>
                            <p>Wichtige Kontaktdaten in den VAE:</p>
                            <br/>
                            <p><u>Notfalldienste</u></p>
                            <p>Polizeilicher Notfall 999</p>
                            <p>Polizei ohne Notfall 901</p>
                            <p>Krankenwagen 998 oder 999</p>
                            <p>Feuerwehr 997 oder 999</p>
                            <br/>
                            <p>Tourismus:</p>
                            <p><a href="https://visitabudhabi.ae/uk-en/default.aspx" target="_blank"><strong>Visit Abu Dhabi</strong></a></p>
                            <p>Offizielle Besucherseite mit Informationen zu Reisen und Tourismus in Abu Dhabi: Hotels, Restaurants, Sehensw&uuml;rdigkeiten, Kultur &amp; Kulturerbe, Veranstaltungen, Museen u.v.m.:</p>
                            <p><a href="https://www.visitdubai.com/en" target="_blank"><strong>Visit Dubai</strong></a></p>
                            <p>Auf der offiziellen Website von Dubai Tourism finden Besucher n&uuml;tzliche Tipps zu Sehensw&uuml;rdigkeiten, Informationen zu Veranstaltungen, Hotels und Reiseangeboten, Stadtrundfahrten u. v. m.:</p>
                            <p><a href="https://www.visitsharjah.com/" target="_blank"><strong>Visit Sharjah</strong></a></p>
                            <p>Sharjah ist das drittgr&ouml;&szlig;te Emirat der VAE und sein familienfreundlichstes Reiseziel. Entdecken Sie ein Sharjah-Museum,&nbsp; machen Sie eine W&uuml;stensafari und erholen Sie sich auf einem Strand in Sharjah - das Emirat bietet etwas f&uuml;r alle Besucher.</p>
                            <p><a href="https://visitrasalkhaimah.com/" target="_blank"><strong>Ras Al Khaimah</strong></a></p>
                            <p>Dieses m&auml;rchenhafte Emirat ist eine Oase der Natur und der Ruhe und bietet eine atemberaubende landschaftliche Vielfalt. Es verf&uuml;gt &uuml;ber eine faszinierende K&uuml;stenlinie von 64 km L&auml;nge - von wei&szlig;en Sandstr&auml;nden bis hin zu &uuml;ppig gr&uuml;nen Mangroven, Buchten und Lagunen.</p>
                            <p><a href="https://ajman.travel/" target="_blank"><strong>Ajman</strong></a></p>
                            <p>Als Reiseziel bietet das Emirat Ajman seinen Besuchern sonnige Urlaubstage und bereichernde historische Entdeckungsreisen.</p>
                            <p><a href="https://fujairah.ae/en/pages/tourism.aspx" target="_blank"><strong>Fujairah</strong></a></p>
                            <p>Das Emirat lockt mit herrlichen Str&auml;nden und weiten W&uuml;sten. Hier finden Sie Informationen zu Hotels und Str&auml;nden, Parks und Freizeitangeboten, Sehensw&uuml;rdigkeiten und kulturellen Aktivit&auml;ten.</p>
                            <p><a href="https://www.szgmc.gov.ae/en/" target="_blank"><strong>Sheikh Zayed Grand Mosque Center</strong></a></p>
                            <p>Dieses architektonische Kunstwerk ist eine der gr&ouml;&szlig;ten Moscheen der Welt und bietet Platz f&uuml;r 40.000 Gl&auml;ubige.</p>
                            <p><a href="https://www.louvreabudhabi.ae/" target="_blank"><strong>Louvre Abu Dhabi</strong></a></p>
                            <p>Erkunden Sie die Museumsgalerien, Ausstellungen und Veranstaltungen in Vorbereitung auf Ihren Besuch:</p>
                            <p><a href="http://www.sharjahmuseums.ae/" target="_blank"><strong>Sharjah-Museen</strong></a></p>
                            <p>Die Sharjah Museums Authority ــ SMA wurde 2006 als autonome Regierungsbeh&ouml;rde gegr&uuml;ndet. Die SMA verbindet und verwaltet 16 Museen in ganz Sharjah, die teils sehr unterschiedliche Bereiche abdecken, darunter islamische Kunst und Kultur, Arch&auml;ologie, Kulturerbe, Wissenschaft, Meeresleben sowie die Geschichte Sharjahs und der Region.</p>
                            <br/>
                            <p><strong>Flugh&auml;fen</strong></p>
                            <p>Hier finden Sie Ankunfts- und Abfluginformationen, Nachrichten und Reiseinformationen zu den wichtigsten Flugh&auml;fen in den VAE:</p>
                            
                            <p><a href="https://www.abudhabiairport.ae/en" target="_blank"><u><strong>Internationaler Flughafen Abu Dhabi (AUH)</strong></u></a></p>
                            <p><a href="https://www.dubaiairports.ae/" target="_blank"><u><strong>Flugh&auml;fen in Dubai</strong></u></a></p>
                            <p><a href="https://www.fujairah-airport.ae/" target="_blank"><u><strong>Internationaler Flughafen Fujairah (FIA)</strong></u></a></p>
                            <p><a href="http://www.rakairport.com/" target="_blank"><u><strong>Internationaler Flughafen Ras Al Khaimah (RAK)</strong></u></a></p>
                            <p><a href="https://www.sharjahairport.ae/en/" target="_blank"><u><strong>Flughafen Sharjah</strong></u></a></p>
                            <br/>
                            <p>Weitere n&uuml;tzliche Informationen:</p>
                            <p><a href="http://www.uaeweather.ae/#!gn:292968" target="_blank"><strong>Wetter</strong></a></p>
                            <p>Wettervorhersage des Nationalen Zentrums f&uuml;r Meteorologie der VAE</p>
                            <p><strong>Nachrichten</strong></p>
                            <p>Englischsprachige Medien in den Vereinigten Arabischen Emiraten.</p>
                            <p><a href="https://www.thenational.ae/international" target="_blank"><strong>The National UAE</strong></a>&nbsp;</p>
                            <p><a href="https://www.khaleejtimes.com/" target="_blank"><strong>Khaleej Times</strong></a></p>
                            <p><a href="https://gulfnews.com/" target="_blank"><strong>Gulf News</strong></a></p>
                            <p>&nbsp;</p>',
                        'page_id' => '',
                        'parent_section_id' => ''
                    ],
                    [
                        'title_en' => "Traveller's Guide German",
                        'title_ar' => 'دليل المسافر إلى ألمانيا',
                        'title' => 'Leitfaden Deutschland',
                        'content_en' => '<img src="//uae-embassy.de/public/img/pages/munich.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>About Germany</strong></p>
							
							<p><strong>Useful addresses:</strong></p>
							<p>General emergency phone number: 112</p>
							<p>Emergency number Police: 110</p>
							<p>NINA: Emergency information and news app of the <a href="https://www.bbk.bund.de/DE/NINA/Warn-App_NINA.html" target="_blank">Federal Authority for Civil Protection and disaster relief.</a></p>
							<p><a href="https://www.zoll.de/EN/Private-individuals/private_individuals_node.html" target="_blank">Customs information for private individuals</a></p>
							<br/>
							<p><u>Tourism:</u></p>
							<p><a href="http://www.germany.travel/en/index.html" target="_blank"><strong>Tourism in Germany</strong></a></p>
							<p>The official platform Germany Travel offers useful information for anyone seeking touristic information on the travel destiny Germany:</p>
							
							<p><a href="https://www.visitberlin.de/en" target="_blank"><strong>Visit Berlin</strong></a></p>
							<p>The official website of the German capital provides visitors with useful tips for sightseeing, information on events, hotels and travel deals, city tours, and much more:</p>
							
							<p><a href="https://www.hamburg-travel.com/?_ga=2.62226085.849508666.1579691791-2082984821.1579691791" target="_blank"><strong>Hamburg Tourism</strong></a></p>
							<p>For all tourism relevant information on Hamburg, including tourist attractions, events, hotels, restaurants and shopping, visit:</p>
							
							<p><a href="https://www.munich.travel/en/?gclid=CjwKCAiAgqDxBRBTEiwA59eEN6x1JXSRXifxANTyeeHV11YuW6szBxQ_eOeBifxEelG_HXWb9RVIiBoCCYcQAvD_BwE" target="_blank"><strong>Simply Munich</strong></a></p>
							<p>Discover Munich&rsquo;s touristic and cultural highlights, restaurants, shopping opportunities, tours, and much more:</p>
							<br/>
							<p><strong>Airports</strong></p>
							<p>Find arrival and departure information, news, and travel information for the main airports in Germany:</p>
							<p><a href="https://www.frankfurt-airport.com/en.html" target="_blank"><u>Frankfurt Airport (FRA)</u></a></p>
							<p><a href="https://www.munich-airport.com/passengers-visitors-75328" target="_blank"><u>Munich Airport (MUC)</u></a></p>
							<p><a href="https://www.berlin-airport.de/en/index.php" target="_blank"><u>Berlin Airports (TXL, SXF)</u></a></p>
							<p><a href="https://www.dus.com/en" target="_blank"><u>D&uuml;sseldorf Airport (DUS):</u></a></p>
							<p><a href="https://www.hamburg-airport.de/en/index.php" target="_blank"><u>Hamburg Airport (HAM)</u></a></p>
							<br/>
							<p>Other useful information:</p>
							
							<p><strong>Germany Platform</strong></p>
							<p>One of the most important instruments of the Federal Foreign Office to inform the world about Germany and to convey a positive image of the country is the Germany platform. Readers can gain an impression of Germany in nine languages on the <a href="http://www.deutschland.de/" target="_blank">website</a> as well as on its Social Media channels.</p>
							
							<p><a href="http://www.tatsachen-ueber-deutschland.de/en" target="_blank"><strong>Facts about Germany</strong></a></p>
							<p><em>Facts about Germany</em> is a website for anyone, but especially for readers abroad, seeking reliable and up-to-date information on the Federal Republic of Germany. It provides facts and figures about the country and its people, its government system, social life, political groupings and trends, the different sectors of industry and business, and the multifaceted face of culture in Germany and its 16 federal states.</p>
							
							<p><a href="https://www.goethe.de/en/index.html" target="_blank"><strong>Goethe-Institut - Culture and language</strong></a></p>
							<p>The Goethe-Institut is the Federal Republic of Germany&rsquo;s cultural institute, active worldwide. It promotes the study of German abroad and in Germany and encourages international cultural exchange.</p>
							<br/>
							<p><strong>News</strong></p>
							<p>Deutsche Welle is Germany&rsquo;s international radio and TV broadcaster, offering news online in currently 30 languages and reports with regional foci: <a href="http://www.dw.com/" target="_blank">www.dw.com</a></p>
							
							<p><a href="https://www.destatis.de/EN/Homepage.html" target="_blank"><strong>Federal Statistics Office</strong></a></p>
							<p>The Federal Statistics Office provides statistical information about Germany</p>
							
							<p><a href="https://www.auswaertiges-amt.de/en/einreiseundaufenthalt/02-lernen-und-arbeiten" target="_blank"><strong>Studying and Working in Germany</strong></a></p>
							<p>The German Foreign Office informs and offers a collection of websites about access to the German labour market and finding work in Germany. Additionally, it provides information for those wishing to study in Germany</p>
							
							<p><a href="https://www.bpb.de/die-bpb/138852/federal-agency-for-civic-education"><strong>Federal Agency for Civic Education</strong></a></p>
							<p>The Federal Agency for Civic Education (Bundeszentrale f&uuml;r politische Bildung) is a federal public authority providing citizenship education and information on political issues for all people in Germany. Some of the bpb content is available in English <a href="https://www.bpb.de/die-bpb/138852/federal-agency-for-civic-education" target="_blank">here</a>.</p>
							',
                        'content_ar' => '<img src="//uae-embassy.de/public/img/pages/munich.jpg" width="100%">
							<br/>
							<br/><p><strong>عن جمهورية ألمانيا الاتحادية</strong></p>
<p><strong><u>أرقام الاتصال الهامة في جمهورية ألمانيا الإتحادية:</u></strong></p>
<p>الحالات الطارئة العامة: 112</p>
<p>الشرطة &ndash; الحالات الطارئة 110</p>
<p>: NINA تطبيق معلومات الطوارئ والأخبار الخاص بالهيئة الإتحادية للحماية المدنية والإغاثة في حالة الطوارئ</p>
<br/>
<p><u>المعلومات الجمركية الخاصة بالأفراد</u></p>
<p><strong>السياحة:</strong></p>
<p>السياحة في جمهورية ألمانيا الإتحادية</p>
<p>توفر الصفحة الإلكترونية الرسمية Germany Travel معلومات مفيدة لأي شخص يسعى للحصول على معلومات سياحية عن جمهورية ألمانيا الإتحادية.</p>

<p><strong>زوروا برلين</strong></p>
<p>توفر الصفحة الإلكترونية الرسمية للعاصمة الألمانية للزوار، معلومات مفيدة حول استكشاف معالم المدينة، والمعلومات عن الفعاليات، والفنادق، والعروض الخاصة للسفر، والجولات السياحية في المدينة، وغيرها.</p>

<p><strong>السياحة في هامبورج</strong></p>
<p>قوموا بزيارة هذه الصفحة الإلكترونية للحصول على كافة المعلومات السياحية الخاصة بهامبورج مثل المزارات السياحية، والفاعليات، والفنادق، والمطاعم والتسوق.</p>

<p><strong>ميونخ</strong></p>
<p>اكتشف معالم ميونخ السياحية والثقافية، والمطاعم، وفرص التسوق والجولات السياحية وغيرها.</p>
<br/>
<p><strong>المطارات</strong></p>
<p>اطلع على معلومات رحلات الوصول والمغادرة، والأخبار، ومعلومات السفر الخاصة بالمطارات الرئيسية في دولة جمهورية ألمانيا الإتحادية.</p>
<p><strong><u>مطار فرانكفورت الدولي </u></strong></p>
<p><strong><u>مطار ميونيخ الدولي </u></strong></p>
<p><strong><u>مطار برلين تيجل/ مطار برلين شونيفيلد</u></strong></p>
<p><strong><u>مطار دوسلدورف</u></strong></p>
<p><strong><u>مطار هامبورج</u></strong></p>
<br/>
<p>معلومات أخرى مفيدة:</p>

<p><strong>الصفحة الإلكترونية الخاصة بجمهورية ألمانيا الإتحادية</strong></p>
<p>تعد الصفحة الإلكترونية الخاصة بجمهورية ألمانيا الإتحادية أحد أهم أدوات وزارة الخارجية لتقديم ألمانيا للعالم ونقل صورة إيجابية عن البلاد.&nbsp; تتوفر المعلومات ب٩ لغات مختلفة على الموقع الإلكتروني وعلى قنوات التواصل الاجتماعي الخاصة بالصفحة.</p>

<p><strong>حقائق عن جمهورية ألمانيا الإتحادية</strong></p>
<p>الصفحة الإلكترونية "حقائق عن جمهورية ألمانيا الإتحادية" هي صفحة موجهة للجميع، وخاصةً للقراء المتواجدين خارج الحدود الألمانية، الذين يسعون للحصول على معلومات موثوقة ومحدثة عن جمهورية ألمانيا الإتحادية. توفر الصفحة الإلكترونية حقائق وأرقام عن البلد والشعب الألماني، والنظام الحكومي، والإتجاهات السياسية، والولايات الاتحادية الـست عشرة، والحياة الاجتماعية، والقطاعات المختلفة للصناعة والأعمال، والأوجه المتعددة للثقافة الألمانية.</p>

<p><strong>معهد جوته &ndash; الثقافة واللغة</strong></p>
<p>هو المعهد الثقافي الرسمي لجمهورية ألمانيا الاتحادية. من أهداف المعهد تشجيع المتواجدين خارج وداخل الحدود الألمانية على دراسة اللغة الألمانية، وتعزيز التبادل الثقافي الدولي.</p>
<br/>
<p><strong>الأخبار</strong></p>
<p>&nbsp;تعتبر Deutsche Welle القناة التليفزيونية والإذاعية الدولة لجمهورية ألمانيا الإتحادية، وتقدم برامجها بثلاثين لغة بينها اللغة العربية.</p>

<p><strong>المكتب الاتحادي للإحصاء</strong></p>
<p>يوفر المكتب الاتحادي للإحصاء الدراسات والأعداد الرسمية عن كل ما يتعلق بالأمور الإحصائية بمؤسسات جمهورية ألمانيا الاتحادية.</p>

<p><strong>العمل والدراسة في جمهورية ألمانيا الإتحادية</strong></p>
<p>توفر وزارة الخارجية الألمانية مجموعة من الصفحات الإلكترونية الخاصة بكيفية الوصول إلى سوق العمل الألماني والحصول على وظيفة به، بالإضافة إلى معلومات للراغبين في الدراسة في جمهورية ألمانيا الإتحادية.</p>

<p><strong>الوكالة الإتحادية لتعليم المدنية والمواطنة</strong></p>
<p>توفر وزارة الخارجية الألمانية مجموعة من الصفحات الإلكترونية الخاصة بكيفية الوصول إلى سوق العمل الألماني والحصول على وظيفة به، بالإضافة إلى معلومات للراغبين في الدراسة في جمهورية ألمانيا الإتحادية.</p>',
                        'content' => '<img src="//uae-embassy.de/public/img/pages/munich.jpg" width="100%">
							<br/>
							<br/>
							<p><strong>Informationen f&uuml;r Reisende&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
                            <p>&nbsp;</p>
                            <p>Deutschland</p>
                            <p>Besucherinformationen</p>
                            <p>N&uuml;tzliche Adressen</p>
                            <p>Allgemeine Notruf-Nummer: 112</p>
                            <p>Notruf Polizei: 110</p>
                            <p>NINA: <a href="https://www.bbk.bund.de/DE/NINA/Warn-App_NINA.html" target="_blank">Notfall-Informations- und Nachrichten-App des Bundesamtes f&uuml;r Bev&ouml;lkerungsschutz und Katastrophenhilfe</a></p>
                            <p>Zoll: <a href="https://www.zoll.de/EN/Private-individuals/private_individuals_node.html" target="_blank">Informationen f&uuml;r Privatpersonen</a></p>
                            <p>&nbsp;</p>
                            <p>Tourismus:</p>
                            <p><strong>Tourismus in Deutschland</strong></p>
                            <p><a href="http://www.germany.travel/en/index.html" target="_blank">Die offizielle Plattform</a></p>
                            <p>Die offizielle Plattform Germany Travel bietet n&uuml;tzliche Informationen f&uuml;r alle, die touristische Informationen zum Reiseziel Deutschland suchen:</p>
                            
                            <p><a href="https://www.visitberlin.de/en" target="_blank"><strong>Visit Berlin</strong></a></p>
                            <p>Die offizielle Website der deutschen Hauptstadt bietet Besuchern n&uuml;tzliche Tipps rund um Besichtigungen, Informationen zu Veranstaltungen, Hotels und Reiseangeboten, Stadtrundfahrten und vieles mehr.</p>
                            <p><a href="https://www.hamburg-travel.com/?_ga=2.62226085.849508666.1579691791-2082984821.1579691791" target="_blank"><strong>Hamburg Tourismus</strong></a></p>
                            <p>Alle tourismusrelevanten Informationen zu Hamburg, einschlie&szlig;lich Sehensw&uuml;rdigkeiten, Veranstaltungen, Hotels, Restaurants und Einkaufsm&ouml;glichkeiten finden Sie unter.</p>
                            <p><a href="https://www.munich.travel/en/?gclid=CjwKCAiAgqDxBRBTEiwA59eEN6x1JXSRXifxANTyeeHV11YuW6szBxQ_eOeBifxEelG_HXWb9RVIiBoCCYcQAvD_BwE" target="_blank"><strong>Einfach M&uuml;nchen</strong></a></p>
                            <p>Entdecken Sie die touristischen und kulturellen Highlights M&uuml;nchens, Restaurants, Einkaufsm&ouml;glichkeiten, Touren und vieles mehr.</p>
                            <p><strong>&nbsp;</strong></p>
                            <p><strong><u>Flugh&auml;fen</u></strong></p>
                            <p>Hier finden Sie Informationen zu Ank&uuml;nften und Abf&uuml;gen, Neuigkeiten und Reiseinformationen zu den wichtigsten Flugh&auml;fen in Deutschland:</p>
                            <p><a href="https://www.frankfurt-airport.com/en.html" target="_blank">Frankfurt Airport (FRA)</a></p>
                            <p><a href="https://www.munich-airport.com/passengers-visitors-75328" target="_blank"><u>Munich Airport (MUC)</u>&nbsp;</a></p>
                            <p><a href="https://www.berlin-airport.de/en/index.php" target="_blank">Berlin Airports (TXL, SXF)</a></p>
                            <p><a href="https://www.dus.com/en&nbsp;" target="_blank"><u>D&uuml;sseldorf Airport (DUS)</u></a></p>
                            <p><a href="https://www.hamburg-airport.de/en/index.php" target="_blank"><u>Hamburg Airport (HAM)</u></a></p>
                            <p>&nbsp;</p>
                            <p>Weitere n&uuml;tzliche Informationen:</p>
                            <p><strong>Deutschland-Plattform</strong></p>
                            <p>Mit seiner Deutschland-Plattform informiert das Ausw&auml;rtige Amt &uuml;ber Deutschland und vermittelt dabei ein positives Bild vom Land. Unter <a href="http://www.detuschland.de/">www.detuschland.de</a> und den Social-Media-Kan&auml;len der Seite, kann sich die Welt auf neun Sprachen ein Bild von Deutschland machen.</p>
                            <p><a href="http://www.tatsachen-ueber-deutschland.de" target="_blank"><strong>Tatsachen &uuml;ber Deutschland</strong></a></p>
                            <p>Die Internetseite <em>Tatsachen &uuml;ber Deutschland</em> ist eine Plattform mit aktuellen und zuverl&auml;ssigen Informationen &uuml;ber die Bundesrepublik Deutschland. Sie bietet Fakten &uuml;ber das Land und die Leute, das Regierungssystem, das Sozialleben, politische Gruppierungen und Trends, die unterschiedlichen Industrie- und Gesch&auml;ftssektoren und die vielschichtige Kultur Deutschlands und seiner sechzehn Bundesl&auml;nder.</p>
                            
                            <p><a href="http://www.goethe.de" target="_blank"><strong>Goethe-Institut &ndash; Kultur und Sprache</strong></a></p>
                            <p>Das Goethe-Institut ist das weltweit t&auml;tige Kulturinstitut der Bundesrepublik Deutschland. Es f&ouml;rdert die Kenntnis der deutschen Sprache mit Deutschkursen im In- und Ausland und pflegt die internationale kulturelle Zusammenarbeit.</p>
                            
                            <p><a href="http://www.germany.travel/de/index.html" target="_blank"><strong>Tourismus in Deutschland</strong></a></p>
                            <p>Die offizielle Plattform Germany Travel stellt n&uuml;tzliche Informationen &uuml;ber das Reiseziel Deutschland bereit:</p>
                           
                            <p><a href="http://www.dw.com" target="_blank"><strong>Nachrichten</strong></a></p>
                            <p>Die Deutsche Welle ist Deutschlands internationale Radio- und Fernsehsendeanstalt und bietet in aktuell 30 Sprachen Online-Nachrichten sowie Reportagen mit regionalen Schwerpunkten.</p>
                            
                            <p><a href="https://www.destatis.de/DE/Startseite.html" target="_blank"><strong>Statistisches Bundesamt</strong></a></p>
                            <p>Das Statistische Bundesamt bietet statistische Informationen zu und &uuml;ber Deutschland:</p>
                            
                            <p><a href="https://www.auswaertiges-amt.de/de/einreiseundaufenthalt/02-lernen-und-arbeiten" target="_blank"><strong>Studieren und Arbeiten in Deutschland</strong></a></p>
                            <p>Das Ausw&auml;rtige Amt informiert &uuml;ber und bietet eine Sammlungen an Internetportalen zum Zugang zum deutschen Arbeitsmarkt und zur Arbeitssuche in Deutschland. Au&szlig;erdem stellt es Informationen zum Studium in Deutschland bereit:</p>
                            
                            <p><a href="https://www.bpb.de/die-bpb/" target="_blank"><strong>Bundeszentrale f&uuml;r politische Bildung</strong></a></p>
                            <p>Die Bundeszentrale f&uuml;r politische Bildung unterst&uuml;tzt die aktive Auseinandersetzung mit Politik und f&ouml;rdert das Verst&auml;ndnis f&uuml;r politische Sachverhalte durch eine Reihe von Publikationen zu allen politischen Themenbereichen.</p>
                            <p>&nbsp;</p>',
                        'page_id' => '',
                        'parent_section_id' => ''
                    ],
                ]
            ],
            [
                'title_en' => 'Gallery',
                'title_ar' => '	ألبوم الصور',
                'title' => 'Galerie',
                'content_en' => '',
                'content_ar' => '',
                'content' => '',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
            [
                'title_en' => 'Press Office',
                'title_ar' => 'المكتب الإعلامي',
                'title' => 'Pressestelle',
                'content_en' => '<img src="//uae-embassy.de/public/img/Media-Office.jpg" width="100%">
							<br/>
							<br/>
                            <p><strong>Press Office</strong></p>
<p>Address: Hiroshimastrasse 18-20, 10785 Berlin, Germany</p>
<p>Phone: +49 (0)30 51651-417</p>
<p>E-Mail: <a href="mailto:BerlinEMB.MA@mofaic.gov.ae">BerlinEMB.MA@mofaic.gov.ae</a></p>
<p>Social Media Accounts: (Username for all accounts: @UAEinBERLIN)</p>
<p><a href="https://www.facebook.com/UAEinBerlin/?modal=admin_todo_tour">Facebook</a> - <a href="https://www.instagram.com/uaeinberlin/">Instagram</a> - <a href="https://twitter.com/UAEinBerlin">Twitter</a></p>',
                'content_ar' => '<img src="//uae-embassy.de/public/img/Media-Office.jpg" width="100%">
							<br/>
							<br/>
<p><strong>المكتب الإعلامي</strong></p>
<p>العنوان: Hiroshimastrasse 18-20, 10785 Berlin, Germany</p>
<p>الهاتف: 417-51651 30(0) 49+  </p>
<p>إيميل: <a href="mailto:BerlinEMB.MA@mofaic.gov.ae">BerlinEMB.MA@mofaic.gov.ae</a></p>
<p>حسابات السفارة على مواقع التواصل الاجتماعي:</p>
<p>(اسم المستخدم لجميع الحسابات: UAEinBERLIN@)</p>
<p>فيسبوك &ndash; تويتر &ndash; إنستغرام</p>',
                'content' => '<img src="//uae-embassy.de/public/img/Media-Office.jpg" width="100%">
							<br/>
							<br/><p><strong>Pressestelle</strong></p>
<p>Address: Hiroshimastrasse 18-20, 10785 Berlin, Germany</p>
<p>Telefone: +49 (0) 30 51651 417</p>
<p>E-mail:&nbsp;<a href="mailto:BerlinEMB@mofa.gov.ae">BerlinEMB@mofa.gov.ae</a></p>
<p>Social Media Accounts: (Gemeinsamer Nutzername: @UAEinBERLIN)</p>
<p><a href="https://www.facebook.com/UAEinBerlin/?modal=admin_todo_tour">Facebook</a> - <a href="https://www.instagram.com/uaeinberlin/">Instagram</a> - <a href="https://twitter.com/UAEinBerlin">Twitter</a></p>',
                'page_id' => 3,
                'parent_section_id' => '',
            ],
        ];

        foreach($data as $item){
            $section['title_en'] = $item['title_en'];
            $section['title_ar'] = $item['title_ar'];
            $section['title'] = $item['title'];
            $section['content_en'] = $item['content_en'];
            $section['content_ar'] = $item['content_ar'];
            $section['content'] = $item['content'];
            $section['page_id'] = $item['page_id'];
            $section['parent_section_id'] = $item['parent_section_id'];
            $section['slug'] = \Illuminate\Support\Str::slug($item['title_en']);
            $new = \App\Models\PageSection::create($section);

            if($new && isset($item['children'])){
                foreach ($item['children'] as $child){
                    $section['title_en'] = $child['title_en'];
                    $section['title_ar'] = $child['title_ar'];
                    $section['title'] = $child['title'];
                    $section['content_en'] = $child['content_en'];
                    $section['content_ar'] = $child['content_ar'];
                    $section['content'] = $child['content'];
                    $section['page_id'] = $item['page_id'];
                    $section['slug'] = \Illuminate\Support\Str::slug($child['title_en']);
                    $section['parent_section_id'] = $new->id;

                    \App\Models\PageSection::create($section);
                }
            }
        }
    }
}
