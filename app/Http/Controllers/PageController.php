<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Article;
use Illuminate\Http\Request;
use GNAHotelSolutions\Weather\Weather;
use Illuminate\Support\Facades\Http;

class PageController extends Controller
{
    public function showPage($page=null){
        $lang = 'de';

        $responsetempberlin = Http::get("http://api.openweathermap.org/data/2.5/weather?q=berlin&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempberlin = $responsetempberlin->json();
        $berlintemp = round($tempberlin['main']['temp']);

        $responsetempdubai = Http::get("http://api.openweathermap.org/data/2.5/weather?q=dubai&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempdubai = $responsetempdubai->json();
        $dubaitemp = round($tempdubai['main']['temp']);

        $timeberlin = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=germany")->json();
        $time_berlin =$timeberlin['time_12'];

        $timedubai = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=dubai")->json();
        $time_dubai =$timedubai['time_12'];


        if(view()->exists($page)){
            return view($page,compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        } else if (!$page){
            return view('home',compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        }

    }
    public function showPageAR($page=null){
        $lang = 'ar';


        $responsetempberlin = Http::get("http://api.openweathermap.org/data/2.5/weather?q=berlin&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempberlin = $responsetempberlin->json();
        $berlintemp = round($tempberlin['main']['temp']);

        $responsetempdubai = Http::get("http://api.openweathermap.org/data/2.5/weather?q=dubai&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempdubai = $responsetempdubai->json();
        $dubaitemp = round($tempdubai['main']['temp']);

        $timeberlin = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=germany")->json();
        $time_berlin =$timeberlin['time_12'];

        $timedubai = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=dubai")->json();
        $time_dubai =$timedubai['time_12'];


        if(view()->exists($page)){
            return view($page,compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        } else if (!$page){
            return view('home',compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        }

    }
    public function showPageEn($page=null){
        $lang = 'en';

        $responsetempberlin = Http::get("http://api.openweathermap.org/data/2.5/weather?q=berlin&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempberlin = $responsetempberlin->json();
        $berlintemp = round($tempberlin['main']['temp']);

        $responsetempdubai = Http::get("http://api.openweathermap.org/data/2.5/weather?q=dubai&units=metric&appid=3e8d8eba8de904b0e4a537751dcf9955");
        $tempdubai = $responsetempdubai->json();
        $dubaitemp = round($tempdubai['main']['temp']);

        $timeberlin = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=germany");
        $timeberlins = $timeberlin->json();
        $time_berlin = $timeberlins['time_12'];

        $timedubai = Http::get("https://api.ipgeolocation.io/timezone?apiKey=05d43246b92b40a8bad5d562d8dfbcda&location=dubai")->json();
        $time_dubai =$timedubai['time_12'];




        if(view()->exists($page)){
            return view($page,compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        } else if (!$page){
            return view('home',compact('lang',['$berlintemp'=>'berlintemp'],['$dubaitemp'=>'dubaitemp'],['$time_berlin'=>'time_berlin'],['$time_dubai'=>'time_dubai']));
        }

    }



    public function showArticlePage($slug){
        $lang = 'de';
        $data = Article::where('slug',$slug)->first();
        return view('article',compact('data','lang'));
    }

    public function showArticlePageAR($slug){
        $lang = 'ar';
        $data = Article::where('slug',$slug)->first();

        $data['title'] = $data['title_ar'];
        $data['content'] = $data['content_ar'];

        return view('article',compact('data','lang'));
    }

    public function showArticlePageEN($slug){
        $lang = 'en';
        $data = Article::where('slug',$slug)->first();

        $data['title'] = $data['title_en'];
        $data['content'] = $data['content_en'];

        return view('article',compact('data','lang'));
    }

}
