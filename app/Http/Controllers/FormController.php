<?php

namespace App\Http\Controllers;

use App\Exports\FormExports;
use App\Models\FormEntry;
use App\Models\NewsletterEntry;
use App\Models\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class FormController extends Controller
{

    function newsletterStore(Request $request){

        $validated = $request->validate([
            'email'=>'required|email|unique:newsletter_entries,email',
            'captcha' => 'required|captcha'
        ]);

        Session::flash('success','Thank you for your registering.');

        $formEntry = NewsletterEntry::create($request->except('_token'));

        if(!$formEntry)
            Session::flash('error','There was an error while saving. Please try again later.');

        return redirect()->back();
    }


    public function export()
    {
        return Excel::download(new FormExports(), 'uae-embassy.de.xlsx');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}
