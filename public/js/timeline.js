var mainTimeline = $('#main-timeline').owlCarousel({
	loop:false,
	margin:100,
	nav:false,
	dots: false,
	startPosition: 1,
	center: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

var subTimeline = $('#sub-timeline').owlCarousel({
	loop:false,
	margin:100,
	startPosition: 1,
	nav:false,
	dots:false,
	center: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

var currentIndex = 1;
mainTimeline.on('changed.owl.carousel', function(event) {
	currentIndex = event.item.index;
	subTimeline.trigger('to.owl.carousel', event.item.index);
	
	$('.owl-item.highlight').removeClass('highlight');
	
	$(mainTimeline).find('.owl-item').eq(event.item.index).addClass('highlight');
	$(subTimeline).find('.owl-item').eq(event.item.index).addClass('highlight');
});

subTimeline.on('changed.owl.carousel', function(event) {
	mainTimeline.trigger('to.owl.carousel', event.item.index);
	
	$('.owl-carousel .owl-item.highlight').removeClass('highlight');
	$(mainTimeline).find('.owl-item').eq(event.item.index).addClass('highlight');
	$(subTimeline).find('.owl-item').eq(event.item.index).addClass('highlight');
});

$('.nav.next').on('click',function(e){
	e.preventDefault();
	
	if(currentIndex >= $('#main-timeline .owl-item').length - 2){
		currentIndex = 0;
		setTimeout(function(){
			mainTimeline.trigger('to.owl.carousel', 1);
		},100);
	}
	
	mainTimeline.trigger('next.owl.carousel');
});
$('.nav.prev').on('click',function(e){
	e.preventDefault();
	
	if(currentIndex == 1){
		currentIndex = $('#main-timeline .owl-item').length - 1;
		mainTimeline.trigger('to.owl.carousel', $('#main-timeline .owl-item').length - 2);
	}
	else
		mainTimeline.trigger('prev.owl.carousel');
});
