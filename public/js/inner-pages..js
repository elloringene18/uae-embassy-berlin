$('.main-nav').on('click',function(e){
	e.preventDefault();
	
    
    if($(this).closest('li').hasClass('active'))
        $(this).closest('li').removeClass('active');
    else {
        if($(this).attr('data-id')){		
            $('#inner-sidebar li').removeClass('active');
            $('#inner-sidebar a').removeClass('active');
            $('#section-content .section').hide();

            $(this).addClass('active');
            $(this).closest('li').addClass('active');

            $('#section-content .section[data-id="'+$(this).attr('data-id')+'"]').show();

            window.history.pushState("object or string", "Title", window.location.href.substring(0, window.location.href.indexOf('#')) + "#" +$(this).attr('data-id'));

        }
        else
            $(this).closest('li').find('ul li a').first().trigger('click');
    }
});

$('.sub-nav').on('click',function(e){
	e.preventDefault();
	$('#inner-sidebar li').removeClass('active');
	$('#inner-sidebar a').removeClass('active');
	
	$(this).closest('.main-li').addClass('active');
	$(this).closest('.main-li').find('a').first().addClass('active');
	$(this).addClass('active');
	
	$('#section-content .section').hide();
	$('#section-content .section[data-id="'+$(this).attr('data-id')+'"]').show();
	
    window.history.pushState("object or string", "Title", window.location.href.substring(0, window.location.href.indexOf('#')) + "#" +$(this).attr('data-id'));
});

// LINK History

cTarget = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
cTarget = cTarget.slice(cTarget.lastIndexOf('#') + 1);

if($("a[data-id='"+cTarget+"']").length){
	$('.inner-sidebar a').removeClass('active');
	$('.inner-sidebar li').removeClass('active');
	
	$('a[data-id='+cTarget+']').trigger('click');
} 