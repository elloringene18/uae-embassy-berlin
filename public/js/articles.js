var masonry = new Macy({
	container: '#macy-container',
	trueOrder: false,
	waitForImages: false,
	useOwnImageLoader: false,
	debug: true,
	mobileFirst: true,
	columns: 1,
	margin: {
		y: 46,
		x: '5%',
	},
	breakAt: {
		1200: 2,
		940: 2,
		520: 1,
		400: 1
	},
});